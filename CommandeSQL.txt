Table role :

INSERT INTO role VALUES (1, 'Administrateur', 'admin');
INSERT INTO role VALUES (2, 'Conseiller immobilier', 'conseiller');


Table type_offre :


INSERT INTO type_offre VALUES (1, 'Location');
INSERT INTO type_offre VALUES (2, 'Achat');



Table type_bien_immobilier : 

INSERT INTO type_bien_immobilier VALUES (1, 'Appartement');
INSERT INTO type_bien_immobilier VALUES (2, 'Maison');
INSERT INTO type_bien_immobilier VALUES (3, 'Studio');
INSERT INTO type_bien_immobilier VALUES (4, 'Entrepôt');
INSERT INTO type_bien_immobilier VALUES (5, 'Terrain');
