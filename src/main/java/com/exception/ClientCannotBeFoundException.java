package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClientCannotBeFoundException extends Exception{

	public ClientCannotBeFoundException(String message) {
		super(message);
	}
	
	

}
