package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class ProprietairesCannotBeDeletetException extends Exception {

	public ProprietairesCannotBeDeletetException(String message) {
		super(message);
	}
	

}
