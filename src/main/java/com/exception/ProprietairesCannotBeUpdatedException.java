package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class ProprietairesCannotBeUpdatedException extends Exception {

	public ProprietairesCannotBeUpdatedException(String message) {
		super(message);
	}

	
}
