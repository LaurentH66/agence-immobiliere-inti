package com.exception;

/**
 * 
 * @author Laurent Hurtado
 *
 */

public class ContratCanNotBeDeletedException extends Exception{

	public ContratCanNotBeDeletedException(String message) {
		super(message);
	}

	
	
}
