package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class ProprietairesCannotBeSavedException extends Exception{

	public ProprietairesCannotBeSavedException(String message) {
		super(message);
	}
	
	

}
