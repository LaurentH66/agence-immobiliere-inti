package com.exception;

/**
 * 
 * @author Charles
 *
 */

public class TypeBienImmobiliersCanNotBeFoundException extends Exception{

	public TypeBienImmobiliersCanNotBeFoundException(String message) {
		super(message);
	}

	
	
}
