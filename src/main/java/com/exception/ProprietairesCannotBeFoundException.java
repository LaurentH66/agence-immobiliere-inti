package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class ProprietairesCannotBeFoundException extends Exception{

	public ProprietairesCannotBeFoundException(String message) {
		super(message);
	}
	
	

}
