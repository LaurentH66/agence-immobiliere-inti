package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotFindAllConseillersImmobiliersException extends Exception {

	public CanNotFindAllConseillersImmobiliersException(String message) {
		super(message);
	}
	

}
