package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClientCannotBeUpdatedException extends Exception {

	public ClientCannotBeUpdatedException(String message) {
		super(message);
	}

	
}
