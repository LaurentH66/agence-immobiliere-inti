package com.exception;
/** 
 * @author Charles
 */
public class VisiteCanNotBeFoundException extends Exception {
	
	public VisiteCanNotBeFoundException(String message) {
		super(message);
	}

}
