package com.exception;

/**
 * 
 * @author Laurent Hurtado
 *
 */

public class ContratsCanNotBeFoundException extends Exception{

	public ContratsCanNotBeFoundException(String message) {
		super(message);
	}

	
	
}
