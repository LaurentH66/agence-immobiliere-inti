package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClasseStandardCannotBeFoundException extends Exception{

	public ClasseStandardCannotBeFoundException(String message) {
		super(message);
	}
	
	

}
