package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotFindConseillersImmobiliersException extends Exception {

	public CanNotFindConseillersImmobiliersException(String message) {
		super(message);
	}

}
