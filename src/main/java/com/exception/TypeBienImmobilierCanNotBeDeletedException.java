package com.exception;

/**
 * 
 * @author Charles
 *
 */

public class TypeBienImmobilierCanNotBeDeletedException extends Exception{

	public TypeBienImmobilierCanNotBeDeletedException(String message) {
		super(message);
	}

	
	
}
