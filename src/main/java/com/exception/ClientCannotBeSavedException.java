package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClientCannotBeSavedException extends Exception{

	public ClientCannotBeSavedException(String message) {
		super(message);
	}
	
	

}
