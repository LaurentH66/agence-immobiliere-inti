package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotFindAdresseByIdException extends Exception{

	public CanNotFindAdresseByIdException(String message) {
		super(message);
	}

}
