package com.exception;

/**
 * 
 * @author Laurent Hurtado
 *
 */

public class ContratCanNotBeAddedException extends Exception{

	public ContratCanNotBeAddedException(String message) {
		super(message);
	}

	
	
}
