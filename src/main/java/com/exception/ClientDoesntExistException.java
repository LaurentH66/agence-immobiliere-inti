package com.exception;

public class ClientDoesntExistException extends Exception{

	public ClientDoesntExistException (String message) {
		super(message);
	}
	
}
