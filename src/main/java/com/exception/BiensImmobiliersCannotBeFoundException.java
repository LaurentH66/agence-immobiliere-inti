package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class BiensImmobiliersCannotBeFoundException extends Exception{

	public BiensImmobiliersCannotBeFoundException(String message) {
		super(message);
	}
	
	

}
