package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClasseStandardCannotBeUpdatedException extends Exception {

	public ClasseStandardCannotBeUpdatedException(String message) {
		super(message);
	}

	
}
