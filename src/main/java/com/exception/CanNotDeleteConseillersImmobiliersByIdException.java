package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotDeleteConseillersImmobiliersByIdException extends Exception {

	public CanNotDeleteConseillersImmobiliersByIdException(String message) {
		super(message);
	}

}
