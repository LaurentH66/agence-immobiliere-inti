package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class BiensImmobiliersCannotBeSavedException extends Exception{

	public BiensImmobiliersCannotBeSavedException(String message) {
		super(message);
	}
	
	

}
