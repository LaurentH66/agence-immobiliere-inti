package com.exception;
/** 
 * @author Charles
 */
public class TypeBienImmobilierCanNotBeUpdatedException extends Exception {
	
	public TypeBienImmobilierCanNotBeUpdatedException(String message) {
		super(message);
	}

}
