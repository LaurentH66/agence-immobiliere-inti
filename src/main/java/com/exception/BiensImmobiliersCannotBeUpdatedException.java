package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class BiensImmobiliersCannotBeUpdatedException extends Exception {

	public BiensImmobiliersCannotBeUpdatedException(String message) {
		super(message);
	}

	
}
