package com.exception;

/**
 * 
 * @author Laurent Hurtado
 *
 */

public class ContratCanNotBeUpdatedException extends Exception{

	public ContratCanNotBeUpdatedException(String message) {
		super(message);
	}

	
	
}
