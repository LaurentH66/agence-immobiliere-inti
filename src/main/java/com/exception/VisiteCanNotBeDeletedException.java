package com.exception;

/**
 * 
 * @author Charles
 *
 */

public class VisiteCanNotBeDeletedException extends Exception{

	public VisiteCanNotBeDeletedException(String message) {
		super(message);
	}

	
	
}
