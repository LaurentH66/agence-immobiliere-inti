package com.exception;
/** 
 * @author Charles
 */
public class VisitesCanNotBeFoundException extends Exception {
	
	public VisitesCanNotBeFoundException(String message) {
		super(message);
	}

}
