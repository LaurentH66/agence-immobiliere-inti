package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class BiensImmobiliersCannotBeDeletetException extends Exception {

	public BiensImmobiliersCannotBeDeletetException(String message) {
		super(message);
	}
	

}
