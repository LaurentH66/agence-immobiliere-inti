package com.exception;

/**
 * 
 * @author Laurent Hurtado
 *
 */

public class ContratCanNotBeFoundException extends Exception{

	public ContratCanNotBeFoundException(String message) {
		super(message);
	}

	
	
}
