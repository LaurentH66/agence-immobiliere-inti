package com.exception;
/** 
 * @author Charles
 */
public class VisiteCanNotBeUpdatedException extends Exception {
	
	public VisiteCanNotBeUpdatedException(String message) {
		super(message);
	}

}
