package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotSaveConseillersImmobiliersException extends Exception{

	public CanNotSaveConseillersImmobiliersException(String message) {
		super(message);
	}

}
