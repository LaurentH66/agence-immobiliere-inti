package com.exception;
/** 
 * @author Charles
 */
public class VisiteCannotBeSavedException extends Exception{

	public VisiteCannotBeSavedException(String message) {
		super(message);
	}
	
	

}
