package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClientCannotBeDeletetException extends Exception {

	public ClientCannotBeDeletetException(String message) {
		super(message);
	}
	

}
