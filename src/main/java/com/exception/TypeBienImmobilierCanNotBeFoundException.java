package com.exception;

/**
 * 
 * @author Charles
 *
 */

public class TypeBienImmobilierCanNotBeFoundException extends Exception{

	public TypeBienImmobilierCanNotBeFoundException(String message) {
		super(message);
	}

	
	
}
