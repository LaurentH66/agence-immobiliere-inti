package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClasseStandardCannotBeSavedException extends Exception{

	public ClasseStandardCannotBeSavedException(String message) {
		super(message);
	}
	
	

}
