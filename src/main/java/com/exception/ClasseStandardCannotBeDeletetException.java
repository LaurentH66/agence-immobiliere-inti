package com.exception;
/**
 * 
 * @author Adeline Scherb
 *
 */
public class ClasseStandardCannotBeDeletetException extends Exception {
	
	public ClasseStandardCannotBeDeletetException(String message) {
		super(message);
	}
	

}
