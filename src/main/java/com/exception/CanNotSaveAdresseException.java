package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotSaveAdresseException extends Exception{

	public CanNotSaveAdresseException(String message) {
		super(message);
	}

}
