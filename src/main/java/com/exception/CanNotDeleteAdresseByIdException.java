package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotDeleteAdresseByIdException extends Exception{

	public CanNotDeleteAdresseByIdException(String message) {
		super(message);
	}

}
