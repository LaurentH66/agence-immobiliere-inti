package com.exception;
/** 
 * @author Charles
 */
public class TypeBienImmobilierCannotBeSavedException extends Exception{

	public TypeBienImmobilierCannotBeSavedException(String message) {
		super(message);
	}
	
	

}
