package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotUpdateAdresseException extends Exception {

	public CanNotUpdateAdresseException(String message) {
		super(message);
	}

}
