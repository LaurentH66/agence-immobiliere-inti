package com.exception;
/**
 * 
 * @author Damien
 *
 */
public class RoleCannotBeFoundException extends Exception {

	public RoleCannotBeFoundException(String message) {
		super(message);
	}
	

}
