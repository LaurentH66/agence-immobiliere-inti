package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotFindAllAdresseException extends Exception {

	public CanNotFindAllAdresseException(String message) {
		super(message);
	}

}
