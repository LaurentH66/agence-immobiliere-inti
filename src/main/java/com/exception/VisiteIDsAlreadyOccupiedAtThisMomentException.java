package com.exception;
/** 
 * @author Charles
 */
public class VisiteIDsAlreadyOccupiedAtThisMomentException extends Exception{

	public VisiteIDsAlreadyOccupiedAtThisMomentException(String message) {
		super(message);
	}
	
	

}
