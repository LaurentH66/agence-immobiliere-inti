package com.exception;

/**
 * 
 * @author Quentin PERINE
 *
 */
public class CanNotUpdateConseillersImmobiliersException extends Exception {

	public CanNotUpdateConseillersImmobiliersException(String message) {
		super(message);
	}
	

}
