package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.ClasseStandard;
import com.entity.TypeBienImmobilier;
import com.entity.TypeOffre;
/**
 * 
 * @author Adeline Scherb
 *
 */
@Repository
public interface ClasseStandardRepository extends JpaRepository<ClasseStandard, Long>{

	@Query("from ClasseStandard where prixMaximum = :x and superficieMaximale = :y and typeOffreId = :z and typeBienImmobilierId = :a")
	public ClasseStandard findClasseStandardBySuperficiePrixTypeOffre(@Param("y") int superficie,@Param("x") double prix,@Param("z") TypeOffre typeOffre,
			@Param("a") TypeBienImmobilier typeBien);
	
//	@Query("from ClasseStandard where prixMaximum = :x")
//	public ClasseStandard findClasseStandardByprixmax(@Param("x") double prix);
//	
//	@Query("from ClasseStandard where superficieMaximale = :y")
//	public ClasseStandard findClasseStandardBySuperficiemax(@Param("y") int superficie);
	
	@Query("from ClasseStandard where prixMaximum <= :x and superficieMaximale <= :y and typeOffreId = :z and typeBienImmobilierId = :a")
	public List<ClasseStandard> findClasseStandardBySuperficieMaxPrixMax(@Param("y") int superficie,@Param("x") double prix,@Param("z") TypeOffre typeOffre,
			@Param("a") TypeBienImmobilier typeBien);
}
