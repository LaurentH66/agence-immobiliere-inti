package com.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.BiensImmobiliers;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.BiensImmobiliersCannotBeUpdatedException;
import com.exception.CanNotFindAllAdresseException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ProprietairesCannotBeFoundException;
/**
 * 
 * @author Damien
 *
 */
@Repository
@Transactional
public interface BiensImmobiliersRepository extends JpaRepository<BiensImmobiliers, Long> {

	@Query("from BiensImmobiliers where cautionLocative < :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMax(@Param("x") double max);
	
	@Query("from BiensImmobiliers where cautionLocative > :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMin(@Param("x") double min);
	
	@Query("from BiensImmobiliers where cautionLocative between :x and :y ")
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMaxMin(@Param("x") double min, @Param("y") double max);
	
	
	
	@Query("from BiensImmobiliers where loyerMensuel  < :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMax(@Param("x") double max);
	
	@Query("from BiensImmobiliers where loyerMensuel  > :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMin(@Param("x") double min);
	
	@Query("from BiensImmobiliers where loyerMensuel  between :x and :y ")
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMaxMin(@Param("x") double max, @Param("y") double min);
	
	
	
	@Query("from BiensImmobiliers where prixLocation  < :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMax(@Param("x") double max);
	
	@Query("from BiensImmobiliers where prixLocation  > :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMin(@Param("x") double min);
	
	@Query("from BiensImmobiliers where prixLocation  between :x and :y ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMaxMin(@Param("x") double max, @Param("y") double min);
	
	
	
	@Query("from BiensImmobiliers where prixAchat < :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMax(@Param("x") double max);
	
	@Query("from BiensImmobiliers where prixAchat > :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMin(@Param("x") double min);
	
	@Query("from BiensImmobiliers where prixAchat  between :x and :y ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMaxMin(@Param("x") double max, @Param("y") double min);
	
	
	
	
	@Query("from BiensImmobiliers where prixAchatDemande < :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMax(@Param("x") double max);
	
	@Query("from BiensImmobiliers where prixAchatDemande > :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMin(@Param("x") double min);
	
	@Query("from BiensImmobiliers where prixAchatDemande  between :x and :y ")
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMaxMin(@Param("x") double max, @Param("y") double min);
	
	
	@Query("from BiensImmobiliers where revenuCadastral < :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMax(@Param("x") double max);
	
	@Query("from BiensImmobiliers where revenuCadastral > :x ")
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMin(@Param("x") double min);
	
	@Query("from BiensImmobiliers where revenuCadastral  between :x and :y ")
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMaxMin(@Param("x") double max, @Param("y") double min);
	
	
	
	@Query("from BiensImmobiliers where typeBail like %:x% ")
	public List<BiensImmobiliers> findBiensImmobiliersByTypeBail(@Param("x") String type);
	
	@Query("from BiensImmobiliers where garniture like %:x% ")
	public List<BiensImmobiliers> findBiensImmobiliersByGarniture(@Param("x") String garniture);
	
	@Query("from BiensImmobiliers where etat like %:x% ")
	public List<BiensImmobiliers> findBiensImmobiliersByEtat(@Param("x") String etat);
	
	@Query("from BiensImmobiliers where status like %:x% ")
	public List<BiensImmobiliers> findBiensImmobiliersByStatus(@Param("x") String status);
	
	@Query("from BiensImmobiliers where proprietaire.idProprietaire =:x ")
	public List<BiensImmobiliers> findBiensImmobiliersByProprietaireId(@Param("x") Long i);
	
	@Query("from BiensImmobiliers where contrat.idContrat =:x ")
	public List<BiensImmobiliers> findBiensImmobiliersByContratId(@Param("x") Long id);
	
	@Query("from BiensImmobiliers where adresse.idAdresse =:x ")
	public List<BiensImmobiliers> findBiensImmobiliersByAdresseId(@Param("x") Long id);
	
	@Query(nativeQuery = true, value = "SELECT * FROM biens_immobiliers "
			+ "WHERE FK_CLASSE_STANDARD IN (SELECT ID_CLASSE_STANDARD FROM classe_standard "
			+ "WHERE FK_TYPE_OFFRE = (SELECT ID_TYPE_OFFRE FROM type_offre WHERE MODE_OFFRE = :x))")
	public List<BiensImmobiliers> findBiensImmobiliersByTypeOffre(@Param("x") String modeOffre);
	
	@Modifying
	@Query("update BiensImmobiliers set contrat.idContrat = :x where idBienImmobilier = :y")
	public void assignContratToBienImmobilier(@Param("x") Long idContrat, @Param("y") Long idBienImmobilier) throws ContratCanNotBeFoundException, BiensImmobiliersCannotBeFoundException;
	
	@Modifying
	@Query("Update BiensImmobiliers b set status = 'Vendu', proprietaire.idProprietaire = (select idProprietaire from Proprietaires where numeroTelephonePrivee = :y)  where contrat.idContrat= :x")
	public void changementStatusBienImmobilier(@Param("x") Long idContrat, @Param("y") String numeroTelephonePrivee) throws BiensImmobiliersCannotBeUpdatedException, ProprietairesCannotBeFoundException;
	
	@Modifying
	@Query("Update BiensImmobiliers b set status = 'Loué' where contrat.idContrat= :x")
	public void changementStatusBienImmobilierLocation(@Param("x") Long idContrat) throws BiensImmobiliersCannotBeUpdatedException;
	
	@Query("from BiensImmobiliers where classeStandard.idClasseStandard =:x ")
	public List<BiensImmobiliers> findBiensImmobiliersByClasseStandardId(@Param("x") Long id);

	@Modifying
	@Query("update BiensImmobiliers set proprietaire.idProprietaire = :x where idBienImmobilier = :y")
	public void assignProprietaireToBienImmobilier(@Param("x") Long idProprietaire, @Param("y") Long idBienImmobilier) throws ProprietairesCannotBeFoundException, BiensImmobiliersCannotBeFoundException;


//	@Modifying
//	@Query("update BiensImmobiliers set adresse.idAdresse = :x where idBienImmobilier = :y")
//	public void assignAdresseToBienImmobilier(@Param("x") Long idAdresse, @Param("y") Long idBienImmobilier) throws CanNotFindAllAdresseException, BiensImmobiliersCannotBeFoundException;

	@Query(nativeQuery = true, value = "SELECT * FROM biens_immobiliers "
			+ "WHERE FK_CLASSE_STANDARD IN (SELECT ID_CLASSE_STANDARD FROM classe_standard "	
			+ "WHERE FK_TYPE_OFFRE = (SELECT ID_TYPE_OFFRE FROM type_offre WHERE MODE_OFFRE = :a) AND "
			+ "FK_TYPE_BIEN_IMMOBILIER = (SELECT ID_TYPE_BIEN_IMMOBILIER FROM type_bien_immobilier WHERE TYPE_BIEN = :b) AND "
			+ "PRIX_MAXIMUM > :e AND PRIX_MAXIMUM <= :c AND SUPERFICIE_MAXIMALE > :f AND SUPERFICIE_MAXIMALE <= :d) AND "
			+ "FK_ADRESSE IN (SELECT ID_ADRESSE FROM adresse WHERE VILLE = :g)")
	public List<BiensImmobiliers> findBiensImmobiliersByAllCriteriaOfClasseStandard(@Param("a") String typeOffre, @Param("b") String typeBien,
			@Param("c") int prixMax, @Param("d") int superficieMax, @Param("e") int prixMin,  @Param("f") int superficieMin,
			@Param("g") String ville);
	
	
	/** Italy service ********/
	@Query("from BiensImmobiliers where (adresse.pays = 'italie' or adresse.pays = 'italia' or adresse.pays = 'italy')")
	public List<BiensImmobiliers> findBiensImmobiliersItalie();

	
	@Query("from BiensImmobiliers where (adresse.pays = 'italie' or adresse.pays = 'italia' or adresse.pays = 'italy') and status like %:x% ")
	public List<BiensImmobiliers> findBiensImmobiliersItaliensByStatus(@Param("x") String status);
	
	@Query("from BiensImmobiliers where (adresse.pays = 'italie' or adresse.pays = 'italia' or adresse.pays = 'italy') and idBienImmobilier = :x ")
	public Optional<BiensImmobiliers> findBienImmobilierItalieById(@Param("x") Long idBienImmobilier);
	

}
































