package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.TypeBienImmobilier;

/** 
 * @author Charles
 */
@Repository
public interface TypeBienImmobilierRepository extends JpaRepository<TypeBienImmobilier, Long>{

	
	@Query("from TypeBienImmobilier where typeBien = :x")
	public TypeBienImmobilier findTypeBienImmobilierByName(@Param("x") String nom);
}
