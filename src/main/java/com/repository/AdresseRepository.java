package com.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Adresse;
import com.exception.CanNotFindAllAdresseException;

/**
 * 
 * @author Quentin PERINE
 *
 */

@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Long>{

	@Query("from Adresse where codePostal = :x")
	public List<Adresse> findAdresseByCodePostal(@Param("x") String codePostal) throws CanNotFindAllAdresseException;
	
	@Query("from Adresse where pays = :x")
	public List<Adresse> findAdresseByPays(@Param("x") String pays) throws CanNotFindAllAdresseException;
	
	@Query("from Adresse where rue = :x")
	public List<Adresse> findAdresseByRue(@Param("x") String rue) throws CanNotFindAllAdresseException;
	
	@Query("from Adresse where ville = :x")
	public List<Adresse> findAdresseByVille(@Param("x") String ville) throws CanNotFindAllAdresseException;
	
	@Query("from Adresse where numero = :a and rue = :b and ville = :c and codePostal = :e and pays = :d")
	public Optional<Adresse> findAdresse(@Param("a") int numero,@Param("b") String rue,@Param("c") String ville, @Param("e") String codePostal, @Param("d") String pays);
}
