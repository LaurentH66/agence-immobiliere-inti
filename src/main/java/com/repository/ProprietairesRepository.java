package com.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Client;
import com.entity.Proprietaires;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
/**
 * 
 * @author Damien
 *
 */
@Repository
@Transactional
public interface ProprietairesRepository extends JpaRepository<Proprietaires, Long> {

	@Query("from Proprietaires where nom like '%' || :x || '%'")
	public List<Proprietaires> findProprietaireByNom(@Param("x") String nom) throws ProprietairesCannotBeFoundException;
	
	@Query("from Proprietaires where numeroTelephonePrivee = :x")
	public Optional<Proprietaires> findProprietaireByNumeroTelephonePrivee(@Param("x") String numeroTelephonePrivee)  throws ProprietairesCannotBeFoundException;
	
	@Query("from Proprietaires where numeroTelephonePrivee = :x")
	public Optional<Proprietaires> searchProprietaireByNumeroTelephonePrivee(@Param("x") String numeroTelephonePrivee) ;
	
	
	@Query("from Proprietaires where numeroTelephoneTravail = :x")
	public Optional<Proprietaires> findProprietaireByNumeroTelephoneTravail(@Param("x") String numeroTelephoneTravail)  throws ProprietairesCannotBeFoundException;
	
	@Query("from Proprietaires where prenom like '%' || :x || '%'")
	public List<Proprietaires> findProprietaireByPrenom(@Param("x") String prenom)  throws ProprietairesCannotBeFoundException;

	@Modifying
	@Query(nativeQuery = true, value = "INSERT into proprietaires(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE) select ID_CLIENT, NOM, NUMERO_TELEPHONE from client where ID_CLIENT = :x")
	public void addProprietaireFromClient(@Param("x") Long idClient) throws ClientDoesntExistException, ProprietairesCannotBeSavedException, ClientCannotBeFoundException, ProprietairesCannotBeFoundException, ClientDoesntExistException ;
	

	/** Italie services **/

	@Query("select distinct b.proprietaire from BiensImmobiliers b where (b.adresse.pays = 'italie' or b.adresse.pays = 'italia' or b.adresse.pays = 'italy')")
	public List<Proprietaires> findProprietairesItalie();

	@Query("select  b.proprietaire from BiensImmobiliers b where (b.adresse.pays = 'italie' or b.adresse.pays = 'italia' or b.adresse.pays = 'italy') and b.proprietaire.idProprietaire = :x ")
	public Optional<Proprietaires> findProprietaireItalieById(@Param("x") Long idProprietaireItalie)throws ProprietairesCannotBeFoundException;
}