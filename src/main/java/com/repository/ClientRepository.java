package com.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.exception.ClientCannotBeFoundException;
/**
 * 
 * @author Adeline Scherb
 *
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, Long>{


	
	@Query("from Client where nom like '%' || :x || '%'")
	public List<Client> findClientByNom(@Param("x") String nom) throws ClientCannotBeFoundException;
	
	@Query("from Client where numeroTelephone = :x")
	public Optional<Client> findClientByNumeroTelephone(@Param("x") String numeroTelephone) throws ClientCannotBeFoundException;

	@Query("from Client where idClient = :x")
	public Client findClientById(@Param("x") Long id) throws ClientCannotBeFoundException;


	@Query(nativeQuery = true, value = "SELECT * FROM client WHERE ID_CLIENT = "
			+ "(SELECT FK_CLIENT FROM ligne_client_classe_standard WHERE FK_CLASSE_STANDARD = :x)")
	public List<Client> findClientByClasseStandard(@Param("x") Long idClasseStandard);
	
	
	@Query(nativeQuery = true, value = "SELECT * FROM client WHERE ID_CLIENT = "
			+ "(SELECT FK_CLIENT FROM ligne_client_classe_standard WHERE FK_CLASSE_STANDARD = "
			+ "(SELECT FK_CLASSE_STANDARD FROM biens_immobiliers WHERE ID_BIEN_IMMOBILIER = :x))")
	public List<Client> findClientByClasseStandardBienImmobilier(@Param("x") Long idBienImmobilier);
	
	@Query(nativeQuery = true, value = "SELECT * FROM client JOIN visite ON visite.ID_CLIENT = client.ID_CLIENT JOIN "
			+ "biens_immobiliers ON biens_immobiliers.ID_BIEN_IMMOBILIER = visite.ID_BIEN_IMMOBILIER "
			+ "WHERE visite.ID_BIEN_IMMOBILIER = :x")
	public List<Client> findClientByVisiteOnBienImmobilier(@Param("x") Long idBienImmobilier);
	
	/** Italie Services **/

	@Query(nativeQuery = true, value = "SELECT * FROM client JOIN contrat ON contrat.CLIENT_ID = client.ID_CLIENT JOIN "
			+ "biens_immobiliers ON contrat.ID_CONTRAT= biens_immobiliers.FK_CONTRAT "
			+ "JOIN adresse ON biens_immobiliers.FK_ADRESSE = adresse.ID_ADRESSE "
			+ "WHERE biens_immobiliers.STATUS = ('Loué' OR 'Louée') "
			+ "AND adresse.PAYS = ('Italie' OR 'Italy' OR 'Italia')")
	public List<Client> findLocataireInItaly();

	
}
