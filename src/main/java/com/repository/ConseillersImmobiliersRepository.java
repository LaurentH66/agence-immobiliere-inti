package com.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.ConseillersImmobiliers;
import com.exception.CanNotFindAllConseillersImmobiliersException;
import com.exception.CanNotFindConseillersImmobiliersException;

/**
 * 
 * @author Quentin PERINE
 *
 */

@Repository
public interface ConseillersImmobiliersRepository extends JpaRepository<ConseillersImmobiliers, Long> {

	
	
	@Query("from ConseillersImmobiliers where login = :x")
	public Optional<ConseillersImmobiliers> findConseillersImmobiliersByLogin(@Param("x") String login) throws CanNotFindConseillersImmobiliersException;
	
	@Query("from ConseillersImmobiliers where nom like '%' || :x || '%'")
	public List<ConseillersImmobiliers> findConseillersImmobiliersByNom(@Param("x") String nom) throws CanNotFindAllConseillersImmobiliersException;
	
	@Query("from ConseillersImmobiliers where prenom like '%' || :x || '%'")
	public List<ConseillersImmobiliers> findConseillersImmobiliersByPrenom(@Param("x") String prenom) throws CanNotFindAllConseillersImmobiliersException;
}
