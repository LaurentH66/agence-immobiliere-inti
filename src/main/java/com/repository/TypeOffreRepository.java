package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.TypeOffre;
/**
 * 
 * @author Laurent Hurtado
 *
 */
@Repository
public interface TypeOffreRepository extends JpaRepository<TypeOffre, Long> {

	@Query("from TypeOffre where modeOffre = :x")
	public TypeOffre findTypeOffreByNom(@Param("x") String nomOffre);
}
