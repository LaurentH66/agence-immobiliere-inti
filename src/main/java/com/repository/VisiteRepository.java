package com.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Visite;
import com.exception.VisitesCanNotBeFoundException;

/** 
 * @author Charles
 */
@Repository
public interface VisiteRepository extends JpaRepository<Visite, Long>{

	@Query("from Visite where dateVisite = :x")
	public List<Visite> findVisiteByDate(@Param("x") String dateVisite) throws VisitesCanNotBeFoundException;
	
	@Query("from Visite where heureVisite = :x")
	public List<Visite> findVisiteByHeure(@Param("x") String heureVisite) throws VisitesCanNotBeFoundException;
	
	/** Recherche toutes les visites d'un client/conseiller/bien précis**/
	
	@Query("from Visite where biensImmobiliers.idBienImmobilier = :x")
	public List<Visite> findVisiteIdBienImmobilier(@Param("x")Long idBienImmobilier) throws VisitesCanNotBeFoundException;
	
	@Query("from Visite where client.idClient = :x")
	public List<Visite> findVisiteIdClient(@Param("x")Long idClient) throws VisitesCanNotBeFoundException;
	
	@Query("from Visite where conseillerImmobilier.idConseillersImmobiliers = :x")
	public List<Visite> findVisiteIdConseiller(@Param("x")Long idConseillersImmobiliers) throws VisitesCanNotBeFoundException;

 
	/** Recherche toutes les visites par login conseiller et nom client
	 **/
	@Query("from Visite where conseillerImmobilier.login like %:x% ")
	public List<Visite> findVisiteByLoginConseiller(@Param("x")String loginConseillersImmobiliers) throws VisitesCanNotBeFoundException;
	
	@Query("from Visite where client.nom like %:x% ")
	public List<Visite> findVisiteByNomClient(@Param("x")String nomClient) throws VisitesCanNotBeFoundException;

	/** Recherche toutes les visites d'un client/conseiller précis dans un bien immobilier précis**/

	@Query("from Visite where biensImmobiliers.idBienImmobilier = :x and client.idClient = :y")
	public List<Visite> findVisiteIdBienImmobilierIdClient(@Param("x") Long idBienImmobilier,@Param("y") Long idClient ) throws VisitesCanNotBeFoundException;

	@Query("from Visite where biensImmobiliers.idBienImmobilier = :x and conseillerImmobilier.idConseillersImmobiliers = :y")
	public List<Visite> findVisiteIdBienImmobilierIdConseiller(@Param("x") Long idBienImmobilier,@Param("y") Long idConseillersImmobiliers ) throws VisitesCanNotBeFoundException;

	
	/** Recherche toutes les visites bien immobilier précis/client/conseiller à une date précise**/

	@Query("from Visite where biensImmobiliers.idBienImmobilier = :x and dateVisite = :y")
	public List<Visite> findVisiteIdBienImmobilierDate(@Param("x") Long idBienImmobilier,@Param("y") String dateVisite ) throws VisitesCanNotBeFoundException;

	@Query("from Visite where client.idClient = :x and dateVisite = :y")
	public List<Visite> findVisiteIdClientDate(@Param("x") Long idClient,@Param("y") String dateVisite ) throws VisitesCanNotBeFoundException;

	@Query("from Visite where conseillerImmobilier.idConseillersImmobiliers = :x and dateVisite = :y")
	public List<Visite> findVisiteIdConseillerDate(@Param("x") Long idConseillersImmobiliers,@Param("y") String dateVisite ) throws VisitesCanNotBeFoundException;

	
	/** Recherche visite precise**/
	
	@Query("from Visite where dateVisite = :x and heureVisite = :y and conseillerImmobilier.idConseillersImmobiliers = :z")
	public Optional<Visite> findVisiteByDateHeureIdConseiller(@Param("x") String dateVisite,@Param("y") String heureVisite,@Param("z") Long idConseillersImmobiliers) throws VisitesCanNotBeFoundException;

	@Query("from Visite where dateVisite = :x and heureVisite = :y and client.idClient = :z")
	public Optional<Visite> findVisiteByDateHeureIdClient(@Param("x") String dateVisite,@Param("y") String heureVisite,@Param("z") Long idClient) throws VisitesCanNotBeFoundException;

	@Query("from Visite where dateVisite = :x and heureVisite = :y and biensImmobiliers.idBienImmobilier = :z")
	public Optional<Visite> findVisiteByDateHeureIdBienImmobilier(@Param("x") String dateVisite,@Param("y") String heureVisite,@Param("z") Long idBienImmobilier) throws VisitesCanNotBeFoundException;

	/** Recherche visite precise**/

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Visite c WHERE c.dateVisite = :x and c.heureVisite = :y and c.conseillerImmobilier.idConseillersImmobiliers = :z")
    boolean existsByDateHeureIdConseiller(@Param("x") String dateVisite,@Param("y") String heureVisite,@Param("z") Long idConseillersImmobiliers);
	
    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Visite c WHERE c.dateVisite = :x and c.heureVisite = :y and c.client.idClient = :z")
    boolean existsByDateHeureIdClient(@Param("x") String dateVisite,@Param("y") String heureVisite,@Param("z") Long idClient);
	
    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Visite c WHERE c.dateVisite = :x and c.heureVisite = :y and c.biensImmobiliers.idBienImmobilier = :z")
    boolean existsByDateHeureIdBienImmobilier(@Param("x") String dateVisite,@Param("y") String heureVisite,@Param("z") Long idBienImmobilier);
	
   
    
}
