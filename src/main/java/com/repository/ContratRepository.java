package com.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entity.Contrat;
import com.exception.ClientCannotBeFoundException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ContratsCanNotBeFoundException;


/**
 * 
 * @author Laurent Hurtado
 *
 */

@Repository
@Transactional
public interface ContratRepository extends JpaRepository<Contrat, Long> {

	@Query("from Contrat where dateEffective = :x")
	public List<Contrat> findContratByDateEffective(@Param("x") String dateEffective) throws ContratsCanNotBeFoundException;
	
	@Query("from Contrat where prixEffectif = :x")
	public List<Contrat> findContratByPrixEffectif(@Param("x") double prixEffectif) throws ContratsCanNotBeFoundException;
	
	@Query("from Contrat where referenceContrat like '%' || :x || '%'")
	public List<Contrat> findContratByReferenceContrat(@Param("x") String referenceContrat) throws ContratCanNotBeFoundException;

	@Modifying
	@Query("update Contrat set client.idClient = :x where idContrat = :y")
	public void assignContratToClient(@Param("x") Long idClient, @Param("y") Long idContrat) throws ContratCanNotBeFoundException, ClientCannotBeFoundException;
	
	/** Italie Services **/
	
	@Query("select distinct b.contrat from BiensImmobiliers b where (b.adresse.pays = 'italie' or b.adresse.pays = 'italia' or b.adresse.pays = 'italy')")
	public List<Contrat> findContratsItalie();

	@Query("select  b.contrat from BiensImmobiliers b where (b.adresse.pays = 'italie' or b.adresse.pays = 'italia' or b.adresse.pays = 'italy') and b.contrat.idContrat = :x ")
	public Optional<Contrat> findContratsItalieById(@Param("x") Long idContrat) ;

}
