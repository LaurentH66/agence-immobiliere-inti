package com.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.entity.BiensImmobiliers;
import com.entity.ClasseStandard;
import com.entity.TypeBienImmobilier;
import com.entity.TypeOffre;
import com.exception.ClasseStandardCannotBeDeletetException;
import com.exception.ClasseStandardCannotBeFoundException;
import com.exception.ClasseStandardCannotBeSavedException;
import com.exception.ClasseStandardCannotBeUpdatedException;
import com.exception.ClientCannotBeSavedException;

/**
 * 
 * @author Adeline Scherb
 *
 */
public interface ClasseStandardService {

	/**
	 * @author Adeline Scherb
	 * @param classeStandard
	 * @return classeStandard
	 * @throws ClasseStandardCannotBeSavedException
	 */
	public ClasseStandard saveClasseStandard(ClasseStandard classeStandard) throws ClasseStandardCannotBeSavedException;

	/**
	 * @author Adeline Scherb
	 * @param classeStandard
	 * @return classeStandard
	 * @throws ClasseStandardCannotBeUpdatedException
	 */
	public ClasseStandard updateClasseStandard(ClasseStandard classeStandard)
			throws ClasseStandardCannotBeUpdatedException;

	/**
	 * @author Adeline Scherb
	 * @param id
	 * @throws ClasseStandardCannotBeDeletetException
	 */
	public void deleteClasseStandardById(Long id) throws ClasseStandardCannotBeDeletetException;

	/**
	 * @author Adeline Scherb
	 * @param id
	 * @return classeStandard
	 * @throws ClasseStandardCannotBeFoundException
	 */
	public ClasseStandard findClasseStandardById(Long id) throws ClasseStandardCannotBeFoundException;

	/**
	 * @author Adeline Scherb
	 * @return List<ClasseStandard>
	 * @throws ClasseStandardCannotBeFoundException
	 */
	public List<ClasseStandard> findClasseStandards() throws ClasseStandardCannotBeFoundException;

	/**
	 * @author Adeline Scherb
	 * @param ienImmo
	 * @param superficie
	 * @param prix
	 * @param typeOffre
	 * @param typeBien
	 * 
	 */
	public void insererBienDansClasseStandard(BiensImmobiliers ienImmo, int superficie, Double prix,
			TypeOffre typeOffre, TypeBienImmobilier typeBien);

	/**
	 * 
	 * @param superficie
	 * @param prix
	 * @param typeOffre
	 * @param typeBien
	 * @return ClasseStandard
	 */
	public ClasseStandard findClasseStandardBySuperficiePrixTypeOffre(@Param("x") int superficie,
			@Param("y") double prix, @Param("z") TypeOffre typeOffre, TypeBienImmobilier typeBien);

	/**
	 * 
	 * @param superficie
	 * @param prix
	 * @param typeOffre
	 * @param typeBien
	 * @return List<ClasseStandard>
	 */
	public List<ClasseStandard> findClasseStandardBySuperficieMaxPrixMax(int superficie, double prix,
			TypeOffre typeOffre, TypeBienImmobilier typeBien);
}
