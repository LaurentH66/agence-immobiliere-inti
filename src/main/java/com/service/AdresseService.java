package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;
import com.entity.Adresse;
import com.exception.CanNotDeleteAdresseByIdException;
import com.exception.CanNotFindAdresseByIdException;
import com.exception.CanNotFindAllAdresseException;
import com.exception.CanNotSaveAdresseException;
import com.exception.CanNotUpdateAdresseException;

/**
 * 
 * @author Quentin PERINE
 *
 */

public interface AdresseService {

	/**
	 * 
	 * @param Adresse adresse
	 * @return adresse saved
	 */
	public Adresse saveAdresse(Adresse adresse) throws CanNotSaveAdresseException;

	/**
	 * 
	 * @param Adresse adresse
	 * @return adresse updated
	 */
	public Adresse updateAdresse(Adresse adresse) throws CanNotUpdateAdresseException;

	/**
	 * 
	 * @param id of adresse which is deleted
	 */
	public void deleteAdresseById(Long id) throws CanNotDeleteAdresseByIdException;

	/**
	 * 
	 * @param id of the adresse we are looking for
	 * @return Adresse
	 */
	public Adresse findAdresseById(Long id) throws CanNotFindAdresseByIdException;

	/**
	 * 
	 * @return all adresses
	 */
	public List<Adresse> findAllAdresses() throws CanNotFindAllAdresseException;

	/**
	 * @author Laurent Hurtado
	 * @param codePostal
	 * @return la liste des adresses utilisant ce code postal
	 * @throws CanNotFindAllAdresseException
	 */
	
	public List<Adresse> findAdresseByCodePostal(@Param("x") String codePostal) throws CanNotFindAllAdresseException;

	/**
	 * @author Laurent Hurtado
	 * @param pays
	 * @return la liste des adresses dans ce pays
	 * @throws CanNotFindAllAdresseException
	 */
	
	public List<Adresse> findAdresseByPays(@Param("x") String pays) throws CanNotFindAllAdresseException;

	/**
	 * @author Laurent Hurtado
	 * @param rue
	 * @return la liste des adresses dans cette rue
	 * @throws CanNotFindAllAdresseException
	 */
	
	public List<Adresse> findAdresseByRue(@Param("x") String rue) throws CanNotFindAllAdresseException;

	/**
	 * @author Laurent Hurtado
	 * @param ville
	 * @return la liste des adresses dans cette ville
	 * @throws CanNotFindAllAdresseException
	 */
	
	public List<Adresse> findAdresseByVille(@Param("x") String ville) throws CanNotFindAllAdresseException;
	
	
	
	/**
	 * @author Quentin
	 * @param numero
	 * @param rue
	 * @param ville
	 * @param pays
	 * @return Adresse
	 */
	public Adresse findAdresse(int numero, String rue, String ville, String codePostal, String pays) throws CanNotFindAllAdresseException;
	
	
}
