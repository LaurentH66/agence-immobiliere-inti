package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.exception.ClientCannotBeDeletetException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;
import com.exception.ClientCannotBeUpdatedException;
/**
 * 
 * @author Adeline Scherb
 *
 */
public interface ClientService  {
	/**
	 * @author Adeline Scherb
	 * @param Client
	 * @return Client
	 * @exception ClientCannotBeSavedException
	 * 
	 */
	public Client saveClient(Client client)throws ClientCannotBeSavedException;
	
	/**
	 * @author Adeline Scherb
	 * @param Client
	 * @return Client
	 * @exception ClientCannotBeUpdatedException
	 * 
	 */
	public Client updateClient(Client client) throws ClientCannotBeUpdatedException;
	
	/**
	 * @author Adeline Scherb
	 * @param Long idClient
	 * @exception ClientCannotBeDeletetException
	 * 
	 */
	public void deleteClientById(Long id) throws ClientCannotBeDeletetException;
	
	/**
	 * @author Adeline Scherb
	 * @param Long idClient
	 * @return Client
	 * @exception ClientCannotBeFoundException
	 * 
	 */
	public Client findClientById(Long id) throws ClientCannotBeFoundException;
	
	/**
	 * @author Adeline Scherb
	 * @return List of Client
	 * @exception ClientCannotBeFoundException
	 * 
	 */
	public List<Client> findClients() throws ClientCannotBeFoundException;
	
	/**
	 * @author Laurent Hurtado
	 * @param nom du client
	 * @return liste des clients avec ce nom
	 */
	
	public List<Client> findClientByNom(@Param("x") String nom) throws ClientCannotBeFoundException;

	/**
	 * @author Laurent Hurtado
	 * @param numero de telephone du client
	 * @return le client recherché
	 */
	
	public Optional<Client> findClientByNumeroTelephone(@Param("x") String numeroTelephone) throws ClientCannotBeFoundException;
	
	/* @author Quentin Perine
	 * @param idBienImmobilier
	 * @return liste des clients intéressés par une visite
	 */
	public List<Client> findClientByVisiteOnBienImmobilier(@Param("x") Long idBienImmobilier);

	/**
	 * @author Charles
	 * @param idClasseStandard
	 * @return List Client
	 */
	public List<Client> findClientByClasseStandard(@Param("x") Long idClasseStandard);
	
	/**
	 * @author Charles
	 * @param idBienImmo
	 * @return Liste des clients qui sont interessés par la classe standard de ce bien immobilier
	 */
	
	public List<Client> findClientByClasseStandardBienImmobilier(@Param("x") Long idBienImmobilier);
	
	/**
	 * @author Charles
	 * @return Liste des clients locataires d'un bien en Italie
	 */
	public List<Client> findLocataireInItaly();


}


