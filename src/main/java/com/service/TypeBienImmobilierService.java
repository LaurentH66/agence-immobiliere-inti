package com.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.entity.TypeBienImmobilier;
import com.exception.TypeBienImmobilierCanNotBeDeletedException;
import com.exception.TypeBienImmobilierCanNotBeFoundException;
import com.exception.TypeBienImmobilierCanNotBeUpdatedException;
import com.exception.TypeBienImmobilierCannotBeSavedException;
import com.exception.TypeBienImmobiliersCanNotBeFoundException;
/** 
 * @author Charles
 */
public interface TypeBienImmobilierService {

	public void saveOrUpdateTypeBienImmobilier(TypeBienImmobilier typeBien);
	
	/**
	 * @param TypeBienImmobilier typeBien
	 * @return typeBien saved
	 */
	public TypeBienImmobilier saveTypeBienImmobilier(TypeBienImmobilier typeBien) throws TypeBienImmobilierCannotBeSavedException;

	/**
	 * @param Object TypeBienImmobilier to delete
	 */
	public void deleteTypeBienImmobilier(TypeBienImmobilier typeBien)throws TypeBienImmobilierCanNotBeDeletedException;
	
	/**
	 * @param Id of Object TypeBienImmobilier to delete
	 */
	public void deleteTypeBienImmobilierById(Long id)throws TypeBienImmobilierCanNotBeDeletedException;
	
	/**
	 * @param id of Object TypeBienImmobilier we are looking for 
	 * @return TypeBienImmobilier
	 */
	public TypeBienImmobilier findTypeBienImmobilierById(Long id) throws TypeBienImmobilierCanNotBeFoundException;
	
	/**
	 * @return all Objects TypeBienImmobilier
	 */
	public List<TypeBienImmobilier> findTypeBienImmobiliers() throws TypeBienImmobiliersCanNotBeFoundException;
	
	/**
	 * @param TypeBienImmobilier typeBien
	 * @return typeBien updated
	 */
	public TypeBienImmobilier updateTypeBienImmobilier(TypeBienImmobilier typeBien) throws TypeBienImmobilierCanNotBeUpdatedException;

	/**
	 * 
	 * @param nom type
	 * @return TypeBienImmobilier
	 */
	public TypeBienImmobilier findTypeBienImmobilierByName(@Param("x") String nom);
	
}
