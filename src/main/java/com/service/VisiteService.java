package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.entity.Visite;
import com.exception.VisiteCanNotBeDeletedException;
import com.exception.VisiteCanNotBeFoundException;
import com.exception.VisiteCanNotBeUpdatedException;
import com.exception.VisiteCannotBeSavedException;
import com.exception.VisiteIDsAlreadyOccupiedAtThisMomentException;
import com.exception.VisitesCanNotBeFoundException;


/**
 * @author Charles
 */
public interface VisiteService {

	public void saveOrUpdateVisite(Visite visite);

	/**
	 * @param TypeBienImmobilier typeBien
	 * @return typeBien saved
	 */
	public Visite saveVisite(Visite visite) throws VisiteCannotBeSavedException, VisiteIDsAlreadyOccupiedAtThisMomentException;

	/**
	 * @param Object TypeBienImmobilier to delete
	 */
	public void deleteVisite(Visite visite) throws VisiteCanNotBeDeletedException;

	/**
	 * @param Id of Object TypeBienImmobilier to delete
	 */
	public void deleteVisiteById(Long id) throws VisiteCanNotBeDeletedException;

	/**
	 * @param id of Object TypeBienImmobilier we are looking for
	 * @return TypeBienImmobilier
	 */
	public Visite findVisiteById(Long id) throws VisiteCanNotBeFoundException;

	/**
	 * @return all Objects TypeBienImmobilier
	 */
	public List<Visite> findVisites() throws VisitesCanNotBeFoundException;

	/**
	 * @param TypeBienImmobilier typeBien
	 * @return typeBien updated
	 */
	public Visite updateVisite(Visite visite) throws VisiteCanNotBeUpdatedException, VisiteIDsAlreadyOccupiedAtThisMomentException;

	/**
	 * @author Laurent Hurtado
	 * @param dateVisite
	 * @return la liste des visites prévues à cette date 
	 * @throws VisitesCanNotBeFoundException
	 */
	
	public List<Visite> findVisiteByDate(String dateVisite) throws VisitesCanNotBeFoundException;

	/**
	 * @author Laurent Hurtado
	 * @param heureVisite
	 * @return la liste des visites prévues à cette heure
	 * @throws VisitesCanNotBeFoundException
	 */
	
	public List<Visite> findVisiteByHeure( String heureVisite) throws VisitesCanNotBeFoundException;

	/**
	 * @author Charles
	 */
	/** Recherche toutes les visites d'un client/conseiller/bien précis**/

	public List<Visite> findVisiteIdBienImmobilier(Long idBienImmobilier) throws VisitesCanNotBeFoundException;
	public List<Visite> findVisiteIdClient(Long idClient) throws VisitesCanNotBeFoundException;
	public List<Visite> findVisiteIdConseiller(Long idConseillersImmobiliers) throws VisitesCanNotBeFoundException;

	
	/** Recherche toutes les visites d'un client/conseiller précis dans un bien immobilier précis**/

	public List<Visite> findVisiteIdBienImmobilierIdClient(Long idBienImmobilier, Long idClient) throws VisitesCanNotBeFoundException;

	public List<Visite> findVisiteIdBienImmobilierIdConseiller(Long idBienImmobilier, Long idConseillersImmobiliers) throws VisitesCanNotBeFoundException;

	
	/** Recherche toutes les visites bien immobilier précis/client/conseiller à une date précise**/

	public List<Visite> findVisiteIdBienImmobilierDate(Long idBienImmobilier, String dateVisite) throws VisitesCanNotBeFoundException;

	public List<Visite> findVisiteIdClientDate(Long idClient, String dateVisite) throws VisitesCanNotBeFoundException;

	public List<Visite> findVisiteIdConseillerDate(Long idConseillersImmobiliers, String dateVisite) throws VisitesCanNotBeFoundException;

	
	/** Recherche visite precise**/
	
	public Optional<Visite> findVisiteByDateHeureIdConseiller(String dateVisite, String heureVisite,
			Long idConseillersImmobiliers) throws VisitesCanNotBeFoundException;

	public Optional<Visite> findVisiteByDateHeureIdClient(String dateVisite, String heureVisite, Long idClient) throws VisitesCanNotBeFoundException;

	public Optional<Visite> findVisiteByDateHeureIdBienImmobilier(String dateVisite, String heureVisite,
			Long idBienImmobilier) throws VisitesCanNotBeFoundException;
	

	/** Recherche toutes les visites par login conseiller et nom client
	 **/
	
     /**
      * @author Damien
      * @param loginConseillersImmobiliers
      * @return List<Visite>
      * @throws VisitesCanNotBeFoundException
      */
	public List<Visite> findVisiteByLoginConseiller(String loginConseillersImmobiliers) throws VisitesCanNotBeFoundException;
	
	/**
	 * @author Damien
	 * @param nomClient
	 * @return List<Visite>
	 * @throws VisitesCanNotBeFoundException
	 */
	public List<Visite> findVisiteByNomClient(String nomClient) throws VisitesCanNotBeFoundException;
	
}
