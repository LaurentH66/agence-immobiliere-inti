package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.entity.Contrat;
import com.exception.ClientCannotBeFoundException;
import com.exception.ContratCanNotBeAddedException;
import com.exception.ContratCanNotBeDeletedException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ContratCanNotBeUpdatedException;
import com.exception.ContratsCanNotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;

/**
 * 
 * @author Laurent Hurtado
 *
 */

public interface ContratService {

	/**
	 * @author Laurent Hurtado
	 * @param Contrat 
	 * @return contrat saved
	 * @throws ContratCanNotBeAddedException
	 */
	
	public Contrat saveContrat(Contrat contrat) throws ContratCanNotBeAddedException;

	/**
	 * @author Laurent Hurtado
	 * @param Contrat 
	 * @return contrat updated
	 * @throws ContratCanNotBeUpdatedException
	 */
	
	public Contrat updateContrat(Contrat contrat) throws ContratCanNotBeUpdatedException;

	/**
	 * @author Laurent Hurtado
	 * @param id du contrat que l'on veut supprimer
	 * @throws ContratCanNotBeDeletedException
	 */
	
	public void deleteContratById(Long id) throws ContratCanNotBeDeletedException;

	/**
	 * @author Laurent Hurtado
	 * @param id du contrat que l'on veut trouver
	 * @return le contrat correspondant à l'id
	 * @throws ContratCanNotBeFoundException
	 */
	
	public Contrat findContratById(Long id) throws ContratCanNotBeFoundException;

	/**
	 * @author Laurent Hurtado
	 * @return la liste des contrats
	 * @throws ContratsCanNotBeFoundException
	 */
	
	public List<Contrat> findContrats() throws ContratsCanNotBeFoundException;

	/**
	 * @author Laurent Hurtado
	 * @param dateEffective
	 * @return la liste des contrats effectifs à cette date
	 * @throws ContratCanNotBeFoundException
	 */
	
	public List<Contrat> findContratByDateEffective(@Param("x") String dateEffective) throws ContratsCanNotBeFoundException;

	/**
	 * @author Laurent Hurtado
	 * @param prixEffectif
	 * @return la liste des contrats effectifs à ce prix
	 * @throws ContratCanNotBeFoundException
	 */
	
	public List<Contrat> findContratByPrixEffectif(@Param("x") double prixEffectif) throws ContratsCanNotBeFoundException;

	/**
	 * @author Laurent Hurtado
	 * @param referenceContrat
	 * @return le contrat ayant cette reference
	 * @throws ContratCanNotBeFoundException
	 */
	public List<Contrat> findContratByReferenceContrat(@Param("x") String referenceContrat) throws ContratCanNotBeFoundException;

	/**
	 * @author Laurent Hurtado
	 * @param idClient
	 * @param idContrat
	 * @throws ContratCanNotBeFoundException
	 * @throws ProprietairesCannotBeSavedException
	 * @throws ClientCannotBeFoundException
	 */
	
	public void assignContratToClient(@Param("x") Long idClient, @Param("y") Long idContrat) throws ContratCanNotBeFoundException , ProprietairesCannotBeSavedException, ClientCannotBeFoundException;


	/**
	 * @author Charles
	 * @return les contrats avec un bien immobilier en Italie
	 */
	public List<Contrat> findContratsItalie();

	/**
	 * @author Charles
	 * @param idContrat
	 * @return le contrat avec un bien immobilier en Italie
	 */
	public Optional<Contrat> findContratsItalieById(@Param("x") Long idContrat) ;
}
