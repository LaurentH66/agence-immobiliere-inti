package com.service;

import java.util.List;

import javax.management.relation.RoleNotFoundException;

import com.entity.Role;
import com.exception.RoleCannotBeFoundException;
/**
 * 
 * @author Quentin Perine
 *
 */
public interface RoleService {

	public Role saveRole(Role role);
	
	public Role updateRole(Role role);

	public void deleteRoleById(Long id);

	public Role findRoleById(Long idRole) throws RoleCannotBeFoundException;

	public List<Role> findRoles();
}
