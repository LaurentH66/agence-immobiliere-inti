package com.service;

import com.entity.Client;
import com.entity.Proprietaires;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;
import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeDeletetException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
import com.exception.ProprietairesCannotBeUpdatedException;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;


public interface ProprietairesService {
	    /**
	     * @author Damien
	     * @param proprietaire
	     * @return proprietaire
	     * @throws ProprietairesCannotBeSavedException
	     */
		public Proprietaires saveProprietairesOld(Proprietaires proprietaire) throws ProprietairesCannotBeSavedException;
		
		/**
		 * @author Damien
		 * @param proprietaire
		 * @return proprietaire
		 * @throws ProprietairesCannotBeUpdatedException
		 */
	    public Proprietaires updateProprietaires(Proprietaires proprietaire) throws ProprietairesCannotBeUpdatedException;
	
	    /**
	     * @author Damien
	     * @param id
	     * @throws ProprietairesCannotBeDeletetException
	     */
		public void deleteProprietairesById(Long id) throws ProprietairesCannotBeDeletetException;
		
		/**
		 * @author Damien
		 * @param id
		 * @return proprietaire
		 * @throws ProprietairesCannotBeFoundException
		 */
		public Proprietaires findProprietairesById(Long id) throws ProprietairesCannotBeFoundException;
		
		/**
		 * @author Damien
		 * @return List<Proprietaires>
		 * @throws ProprietairesCannotBeFoundException
		 */
		public List<Proprietaires> findProprietaires()  throws ProprietairesCannotBeFoundException;
	
		/**
		 * @author Laurent Hurtado
		 * @param nom
		 * @return la liste des propriétaires ayant ce nom
		 * @throws ProprietairesCannotBeFoundException
		 */
		
		public List<Proprietaires> findProprietaireByNom(@Param("x") String nom) throws ProprietairesCannotBeFoundException;
		
		/**
		 * @author Laurent Hurtado
		 * @param numeroTelephonePrivee
		 * @return le proprietaire ayant ce numero de telephone prive
		 * @throws ProprietairesCannotBeFoundException
		 */
		
		public Optional<Proprietaires> findProprietaireByNumeroTelephonePrivee(@Param("x") String numeroTelephonePrivee)  throws ProprietairesCannotBeFoundException;
		/** idem mais renvoie null si il trouve rien **/
		public Optional<Proprietaires> searchProprietaireByNumeroTelephonePrivee(@Param("x") String numeroTelephonePrivee) ;

		/**
		 * @author Laurent Hurtado
		 * @param numeroTelephoneTravail
		 * @return le proprietaire ayant ce numero de telephone de travail
		 * @throws ProprietairesCannotBeFoundException
		 */
		
		public Optional<Proprietaires> findProprietaireByNumeroTelephoneTravail(@Param("x") String numeroTelephoneTravail)  throws ProprietairesCannotBeFoundException;

		/**
		 * @author Laurent Hurtado
		 * @param prenom
		 * @return la liste des propriétaires ayant ce prenom
		 * @throws ProprietairesCannotBeFoundException
		 */
		
		public List<Proprietaires> findProprietaireByPrenom(@Param("x") String prenom)  throws ProprietairesCannotBeFoundException;
	
		public void addProprietaireFromClient(@Param("x") Long idClient) throws ClientDoesntExistException, ProprietairesCannotBeSavedException;
		
		public Proprietaires saveProprietaires(Proprietaires proprietaires) throws ProprietairesCannotBeSavedException, ClientCannotBeSavedException, ClientCannotBeFoundException, ProprietairesCannotBeFoundException, ClientDoesntExistException;

		
		/**
		 * @author Charles
		 * @return les propriotaires d'un bien en Italie
		 */
		public List<Proprietaires> findProprietairesItalie();
		
		/**
		 * @author Charles
		 * @param idProprietaireItalie
		 * @return Proprietaire
		 */
		public Optional<Proprietaires> findProprietaireItalieById(@Param("x") Long idProprietaireItalie)throws ProprietairesCannotBeFoundException;;

}
