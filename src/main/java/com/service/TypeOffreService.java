package com.service;

import com.entity.TypeOffre;
/**
 * 
 * @author Laurent Hurtado
 *
 */
public interface TypeOffreService {

	public TypeOffre findTypeOffreByNom(String nomOffre);
	
}
