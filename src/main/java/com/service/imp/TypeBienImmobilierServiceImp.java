package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.TypeBienImmobilier;
import com.entity.Visite;
import com.exception.ContratCanNotBeDeletedException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.TypeBienImmobilierCanNotBeDeletedException;
import com.exception.TypeBienImmobilierCanNotBeFoundException;
import com.exception.TypeBienImmobilierCanNotBeUpdatedException;
import com.exception.TypeBienImmobilierCannotBeSavedException;
import com.exception.TypeBienImmobiliersCanNotBeFoundException;
import com.exception.VisiteCanNotBeFoundException;
import com.exception.VisiteCannotBeSavedException;
import com.exception.VisitesCanNotBeFoundException;
import com.repository.TypeBienImmobilierRepository;
import com.service.TypeBienImmobilierService;

import lombok.extern.slf4j.Slf4j;
/** 
 * @author Charles
 */
@Service
@Slf4j
public class TypeBienImmobilierServiceImp implements TypeBienImmobilierService {


	@Autowired
	TypeBienImmobilierRepository dao;

	@Override
	public void saveOrUpdateTypeBienImmobilier(TypeBienImmobilier typeBien) {
		dao.save(typeBien);
		
	}
	
	@Override
	public void deleteTypeBienImmobilier(TypeBienImmobilier typeBien) throws TypeBienImmobilierCanNotBeDeletedException {
		try {
		dao.delete(typeBien);
		log.info("TypeBienImmobilier deleted by id with success");
		} catch (Exception e) {
			throw new TypeBienImmobilierCanNotBeDeletedException("TypeBienImmobilier " + typeBien +"can not be ");
		}
	}
	
	public void deleteTypeBienImmobilierById(Long id) throws TypeBienImmobilierCanNotBeDeletedException {
		try {
			dao.deleteById(id);
			log.info("TypeBienImmobilier deleted by id with success");
		} catch (Exception e) {
			throw new TypeBienImmobilierCanNotBeDeletedException("TypeBienImmobilier can not be delted with id = "+ id);
		}
	}

	
	@Override
	public TypeBienImmobilier findTypeBienImmobilierById(Long id) throws TypeBienImmobilierCanNotBeFoundException {
		Optional<TypeBienImmobilier> typeBien;
		try {
			typeBien = dao.findById(id);
			log.info("TypeBienImmobilier found with success");
		} catch (Exception e) {
			throw new TypeBienImmobilierCanNotBeFoundException("TypeBienImmobilier can not be found with id = "+id);
		}

		if(typeBien.isPresent()) {
			return typeBien.get();
			}
		else {
			throw new TypeBienImmobilierCanNotBeFoundException("TypeBienImmobilier can not be found with id = "+id);
		}
	}


	@Override
	public List<TypeBienImmobilier> findTypeBienImmobiliers() throws TypeBienImmobiliersCanNotBeFoundException {
		
		List<TypeBienImmobilier> typeBiens = null;
		
		try {
			typeBiens = dao.findAll();
			log.info("Find all visite with success");
		} catch (Exception e) {
			throw new TypeBienImmobiliersCanNotBeFoundException("TypeBienImmobiliers can not be found");
		}
		
		return typeBiens;
	}


	public TypeBienImmobilier updateTypeBienImmobilier(TypeBienImmobilier typeBien)
			throws TypeBienImmobilierCanNotBeUpdatedException {
		if (typeBien.getIdTypeBienImmobilier() == null)
			throw new TypeBienImmobilierCanNotBeUpdatedException("typeBien can not be updated : " + typeBien);
		TypeBienImmobilier typeBienUpdate = dao.save(typeBien);
		try {
			typeBienUpdate = dao.save(typeBien);
		} catch (Exception e) {
			throw new TypeBienImmobilierCanNotBeUpdatedException("typeBien can not be updated : " + typeBien);
		}
		return typeBienUpdate;
	}

	@Override
	public TypeBienImmobilier saveTypeBienImmobilier(TypeBienImmobilier typeBien)
			throws TypeBienImmobilierCannotBeSavedException {
		// TODO Auto-generated method stub
		
		TypeBienImmobilier typeBien2= null;
		try {
			typeBien2= dao.save(typeBien);
		log.info("Save typeBienImmobilier with success");
		} catch (Exception e){
			throw new TypeBienImmobilierCannotBeSavedException("Can not save typeBienImmobilier:" + typeBien);
			}
		return typeBien2;	

	}

	@Override
	public TypeBienImmobilier findTypeBienImmobilierByName(String nom) {
		// TODO Auto-generated method stub
		return dao.findTypeBienImmobilierByName(nom);
	}

	
}
