package com.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.TypeOffre;
import com.repository.TypeOffreRepository;
import com.service.TypeOffreService;

@Service
public class TypeOffreServiceImp implements TypeOffreService{

	@Autowired
	TypeOffreRepository repo;
	
	@Override
	public TypeOffre findTypeOffreByNom(String nomOffre) {
		// TODO Auto-generated method stub
		return repo.findTypeOffreByNom(nomOffre);
	}

}
