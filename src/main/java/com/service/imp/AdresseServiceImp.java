package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Adresse;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.CanNotDeleteAdresseByIdException;
import com.exception.CanNotFindAdresseByIdException;
import com.exception.CanNotFindAllAdresseException;
import com.exception.CanNotSaveAdresseException;
import com.exception.CanNotUpdateAdresseException;
import com.repository.AdresseRepository;
import com.service.AdresseService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Quentin PERINE
 *
 */

@Slf4j
@Service
public class AdresseServiceImp implements AdresseService {

	@Autowired
	AdresseRepository arepo;
	
	@Override
	public Adresse saveAdresse(Adresse adresse) throws CanNotSaveAdresseException {
		// TODO Auto-generated method stub
		Adresse adresseR = null;
		
		try {
			adresseR = arepo.save(adresse);
			log.info("Save adresse with success");
		} catch (Exception e) {
			throw new CanNotSaveAdresseException("Can not save adresse : "+adresse);
		}
		
		return adresseR;
	}

	@Override
	public Adresse updateAdresse(Adresse adresse) throws CanNotUpdateAdresseException {
		// TODO Auto-generated method stub
		if (adresse.getIdAdresse() == null) throw new CanNotUpdateAdresseException("Adresse can not be updated");
		Adresse adresseR = null;
		try {
			adresseR = arepo.save(adresse);
			log.info("Update adresse with success");
		} catch (Exception e) {
			throw new CanNotUpdateAdresseException("Adresse can not be updated");
		}
		
		return adresseR;
	}

	@Override
	public void deleteAdresseById(Long id) throws CanNotDeleteAdresseByIdException {
		// TODO Auto-generated method stub
		try {
			arepo.deleteById(id);
			log.info("Delete adresse by id with success");
		} catch (Exception e) {
			throw new CanNotDeleteAdresseByIdException("Adresse can not be delted with id = "+id);
		} 
	}

	@Override
	public Adresse findAdresseById(Long id) throws CanNotFindAdresseByIdException {
		// TODO Auto-generated method stub
		Optional<Adresse> adresse;
		try {
			adresse = arepo.findById(id);
			log.info("Find adresse with success");
		} catch (Exception e) {
			throw new CanNotFindAdresseByIdException("Adresse can not be found with id = "+id);
		}
		
		if(adresse.isPresent()) {
			return adresse.get();
			}
		else {
			throw new CanNotFindAdresseByIdException("Adresse can not be found with id = "+id);
		}	}

	@Override
	public List<Adresse> findAllAdresses() throws CanNotFindAllAdresseException {
		// TODO Auto-generated method stub
		List<Adresse> adresses = null;
		try {
			adresses = arepo.findAll();
			log.info("Find all adresses with success");
		} catch (Exception e) {
			throw new CanNotFindAllAdresseException("Adresses can not be found");
		}
		
		return adresses;
	}

	@Override
	public List<Adresse> findAdresseByCodePostal(String codePostal) throws CanNotFindAllAdresseException {
		// TODO Auto-generated method stub
		List<Adresse> adresses = null;
		try {
			adresses = arepo.findAdresseByCodePostal(codePostal);
			log.info("Find all adresses with success");
		} catch (Exception e) {
			throw new CanNotFindAllAdresseException("Adresses can not be found");
		}
		
		return adresses;
	}

	@Override
	public List<Adresse> findAdresseByPays(String pays) throws CanNotFindAllAdresseException {
		// TODO Auto-generated method stub
		List<Adresse> adresses = null;
		try {
			adresses = arepo.findAdresseByPays(pays);
			log.info("Find all adresses with success");
		} catch (Exception e) {
			throw new CanNotFindAllAdresseException("Adresses can not be found");
		}
		
		return adresses;
	}

	@Override
	public List<Adresse> findAdresseByRue(String rue) throws CanNotFindAllAdresseException {
		// TODO Auto-generated method stub
		List<Adresse> adresses = null;
		try {
			adresses = arepo.findAdresseByRue(rue);
			log.info("Find all adresses with success");
		} catch (Exception e) {
			throw new CanNotFindAllAdresseException("Adresses can not be found");
		}
		
		return adresses;
	}

	@Override
	public List<Adresse> findAdresseByVille(String ville) throws CanNotFindAllAdresseException {
		// TODO Auto-generated method stub
		List<Adresse> adresses = null;
		try {
			adresses = arepo.findAdresseByVille(ville);
			log.info("Find all adresses with success");
		} catch (Exception e) {
			throw new CanNotFindAllAdresseException("Adresses can not be found");
		}
		
		return adresses;
	}

	@Override
	public Adresse findAdresse(int numero, String rue, String ville, String codePostal, String pays) throws CanNotFindAllAdresseException {
		// TODO Auto-generated method stub
	Optional<Adresse> adresse;
	try {
		adresse = arepo.findAdresse(numero, rue, ville, codePostal, pays);
		log.info("Find adresse with success");
	} catch (Exception e) {
		throw new CanNotFindAllAdresseException("Adresse can not be found");
	}
	
	if(adresse.isPresent()) {
		return adresse.get();
		}
	else {
		return null;
	}	}
	


}
