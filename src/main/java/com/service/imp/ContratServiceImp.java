package com.service.imp;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Client;
import com.entity.Contrat;
import com.exception.CanNotFindConseillersImmobiliersException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ContratCanNotBeAddedException;
import com.exception.ContratCanNotBeDeletedException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ContratCanNotBeUpdatedException;
import com.exception.ContratsCanNotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
import com.repository.ClientRepository;
import com.repository.ContratRepository;
import com.repository.ProprietairesRepository;
import com.service.ContratService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Laurent Hurtado
 *
 */

@Slf4j
@Service
public class ContratServiceImp implements ContratService {

	@Autowired
	ContratRepository repo;

	@Autowired
	ClientRepository crepo;

	@Autowired
	ProprietairesRepository prepo;

	@Override
	public Contrat saveContrat(Contrat contrat) throws ContratCanNotBeAddedException {
		// TODO Auto-generated method stub
		Contrat contratR = null;

		try {
			contratR = repo.save(contrat);
			log.info("Save contrat with success");
		} catch (Exception e) {
			throw new ContratCanNotBeAddedException("Can not save contrat : " + contrat);
		}

		return contratR;
	}

	@Override
	public Contrat updateContrat(Contrat contrat) throws ContratCanNotBeUpdatedException {
		// TODO Auto-generated method stub
		if (contrat.getIdContrat() == null)
			throw new ContratCanNotBeUpdatedException("Contrat can not be updated");
		Contrat contratR = null;
		try {
			contratR = repo.save(contrat);
			log.info("Contrat updated with success");
		} catch (Exception e) {
			throw new ContratCanNotBeUpdatedException("Contrat can not be updated");
		}

		return contratR;
	}

	@Override
	public void deleteContratById(Long id) throws ContratCanNotBeDeletedException {
		// TODO Auto-generated method stub
		try {
			repo.deleteById(id);
			log.info("Contrat deleted by id with success");
		} catch (Exception e) {
			throw new ContratCanNotBeDeletedException("Contrat can not be delted with id = " + id);
		}
	}

	@Override
	public Contrat findContratById(Long id) throws ContratCanNotBeFoundException {
		// TODO Auto-generated method stub
		Optional<Contrat> contrat;
		try {
			contrat = repo.findById(id);
			log.info("Contrat found with success");
		} catch (Exception e) {
			throw new ContratCanNotBeFoundException("Contrat can not be found with id = " + id);
		}

		if (contrat.isPresent()) {
			return contrat.get();
		} else {
			throw new ContratCanNotBeFoundException("Contrat can not be found with id = " + id);
		}
	}

	@Override
	public List<Contrat> findContrats() throws ContratsCanNotBeFoundException {
		// TODO Auto-generated method stub
		List<Contrat> contrats = null;
		try {
			contrats = repo.findAll();
			log.info("Find all contrats with success");
		} catch (Exception e) {
			throw new ContratsCanNotBeFoundException("Contrats can not be found");
		}

		return contrats;
	}

	@Override
	public List<Contrat> findContratByDateEffective(String dateEffective) throws ContratsCanNotBeFoundException {
		// TODO Auto-generated method stub
		List<Contrat> contrats = null;
		try {
			contrats = repo.findContratByDateEffective(dateEffective);
			log.info("Find all contrats with success");
		} catch (Exception e) {
			throw new ContratsCanNotBeFoundException("Contrats can not be found");
		}

		return contrats;
	}

	@Override
	public List<Contrat> findContratByPrixEffectif(double prixEffectif) throws ContratsCanNotBeFoundException {
		// TODO Auto-generated method stub
		List<Contrat> contrats = null;
		try {
			contrats = repo.findContratByPrixEffectif(prixEffectif);
			log.info("Find all contrats with success");
		} catch (Exception e) {
			throw new ContratsCanNotBeFoundException("Contrats can not be found");
		}

		return contrats;
	}

	@Override
	public List<Contrat> findContratByReferenceContrat(String referenceContrat)
			throws ContratCanNotBeFoundException {
		List<Contrat> contrat = null;
		try {
			contrat = repo.findContratByReferenceContrat(referenceContrat);
			log.info("Contrat found with success");
		} catch (Exception e) {
			log.warn("Contrat can not be found");
			throw new ContratCanNotBeFoundException(
					"Contrat can not be found with his reference : " + referenceContrat);
		}

		return contrat;
	}

	@Transactional
	@Override
	public void assignContratToClient(Long idClient, Long idContrat)
			throws ContratCanNotBeFoundException, ClientCannotBeFoundException, ProprietairesCannotBeSavedException {

		try {
			crepo.findById(idClient);
		} catch (Exception e) {
			throw new ClientCannotBeFoundException("Client can not be found with his id : " + idClient);
		}

		try {
			repo.assignContratToClient(idClient, idContrat);
			log.info("Contrat found with success");
		} catch (Exception e) {
			throw new ContratCanNotBeFoundException("Contrat can not be found with his id : " + idContrat);
		}
		if (!prepo.findById(idClient).isPresent()) {

			try {
				prepo.addProprietaireFromClient(idClient);
			} catch (Exception e) {
				throw new ProprietairesCannotBeSavedException(null);
			}
		}

	}

	@Override
	public List<Contrat> findContratsItalie() {
		return repo.findContratsItalie();
	}

	@Override
	public Optional<Contrat> findContratsItalieById(Long idContrat) {
		return repo.findContratsItalieById(idContrat);
	}
}
