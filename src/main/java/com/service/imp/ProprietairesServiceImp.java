package com.service.imp;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.entity.Adresse;
import com.entity.Client;
import com.entity.Proprietaires;

import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;
import com.exception.ClientCannotBeUpdatedException;
import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeDeletetException;
import com.exception.ProprietairesCannotBeSavedException;
import com.exception.ProprietairesCannotBeUpdatedException;
import com.repository.AdresseRepository;
import com.repository.ClientRepository;
import com.repository.ProprietairesRepository;
import com.service.ClientService;
import com.service.ProprietairesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProprietairesServiceImp implements ProprietairesService {

	@Autowired
	ProprietairesRepository dao;

	@Autowired
	ClientRepository daoc;

	@Autowired
	ClientService sClient;
	
	@Autowired
	AdresseRepository adao;

	@Override
	public Proprietaires saveProprietairesOld(Proprietaires proprietaire) throws ProprietairesCannotBeSavedException {

		Proprietaires proprietaire2 = null;
		try {
			proprietaire2 = dao.save(proprietaire);
			log.info("Save proprietaire with success");
		} catch (Exception e) {
			throw new ProprietairesCannotBeSavedException("Can not save proprietaire:" + proprietaire);
		}
		return proprietaire2;
	}

	@Override
	public Proprietaires updateProprietaires(Proprietaires proprietaire) throws ProprietairesCannotBeUpdatedException {
		if (proprietaire.getIdProprietaire() == null)
			throw new ProprietairesCannotBeUpdatedException("Proprietaire can not be updated");
		Proprietaires proprietaire2 = null;
		try {
			proprietaire2 = dao.save(proprietaire);
			log.info("Update proprietaire with success");
		} catch (Exception e) {
			throw new ProprietairesCannotBeUpdatedException("Can not update proprietaire:" + proprietaire);
		}
		return proprietaire2;
	}

	@Override
	public void deleteProprietairesById(Long id) throws ProprietairesCannotBeDeletetException {
		try {
			dao.deleteById(id);
			log.info("Delete proprietaire with success");
		} catch (Exception e) {
			throw new ProprietairesCannotBeDeletetException("Can not delete proprietaire with id " + id);
		}

	}

	@Override
	public Proprietaires findProprietairesById(Long id) throws ProprietairesCannotBeFoundException {
		// TODO Auto-generated method stub
		Optional<Proprietaires> proprietaire;
		try {
			proprietaire = dao.findById(id);
			log.info("Proprietaire found with success");
		} catch (Exception e) {
			throw new ProprietairesCannotBeFoundException("Proprietaire can not be found with id = " + id);
		}
		if (proprietaire.isPresent()) {
			return proprietaire.get();
		} else {
			throw new ProprietairesCannotBeFoundException("Proprietaire can not be found with id = " + id);
		}
	}

	@Override
	public List<Proprietaires> findProprietaires() throws ProprietairesCannotBeFoundException {
		// TODO Auto-generated method stub
		List<Proprietaires> proprietaires = null;
		try {
			proprietaires = dao.findAll();
			log.info("Find all Proprietairess with success");
		} catch (Exception e) {
			throw new ProprietairesCannotBeFoundException("Proprietairess can not be found");
		}

		return proprietaires;
	}

	@Override
	public List<Proprietaires> findProprietaireByNom(String nom) throws ProprietairesCannotBeFoundException {
		// TODO Auto-generated method stub
		List<Proprietaires> proprietaires = null;
		try {
			proprietaires = dao.findProprietaireByNom(nom);
			log.info("All proprietaires have been found");
		} catch (Exception e) {
			// TODO: handle exception
			throw new ProprietairesCannotBeFoundException("Proprietaires can not be found");
		}
		return proprietaires;
	}

	@Override
	public Optional<Proprietaires> findProprietaireByNumeroTelephonePrivee(String numeroTelephonePrivee)
			throws ProprietairesCannotBeFoundException {
		// TODO Auto-generated method stub
		Optional<Proprietaires> proprietaire;
		try {
			proprietaire = dao.findProprietaireByNumeroTelephonePrivee(numeroTelephonePrivee);
			log.info("Proprietaire has been found");
		} catch (Exception e) {
			log.warn("Proprietaire can not be found");
			throw new ProprietairesCannotBeFoundException(
					"Proprietaire can not be found with his numero : " + numeroTelephonePrivee);
		}
		if (!proprietaire.isPresent()) {
			throw new ProprietairesCannotBeFoundException(
					"Proprietaire can not be found with his numero : " + numeroTelephonePrivee);
		}
		return proprietaire;
	}
	
	public Optional<Proprietaires> searchProprietaireByNumeroTelephonePrivee(@Param("x") String numeroTelephonePrivee) {
		return dao.searchProprietaireByNumeroTelephonePrivee(numeroTelephonePrivee);
	}

	@Override
	public Optional<Proprietaires> findProprietaireByNumeroTelephoneTravail(String numeroTelephoneTravail)
			throws ProprietairesCannotBeFoundException {
		// TODO Auto-generated method stub
		Optional<Proprietaires> proprietaire;
		try {
			proprietaire = dao.findProprietaireByNumeroTelephoneTravail(numeroTelephoneTravail);
			log.info("Proprietaire has been found");
		} catch (Exception e) {
			log.warn("Proprietaire can not be found");
			throw new ProprietairesCannotBeFoundException(
					"Proprietaire can not be found with his numero : " + numeroTelephoneTravail);
		}
		if (!proprietaire.isPresent()) {
			throw new ProprietairesCannotBeFoundException(
					"Proprietaire can not be found with his numero : " + numeroTelephoneTravail);
		}
		return proprietaire;
	}

	@Override
	public List<Proprietaires> findProprietaireByPrenom(String prenom) throws ProprietairesCannotBeFoundException {
		// TODO Auto-generated method stub
		List<Proprietaires> proprietaires = null;
		try {
			proprietaires = dao.findProprietaireByPrenom(prenom);
			log.info("All proprietaires have been found");
		} catch (Exception e) {
			// TODO: handle exception
			throw new ProprietairesCannotBeFoundException("Proprietaires can not be found");
		}
		return proprietaires;
	}

	@Transactional
	@Override
	public void addProprietaireFromClient(Long idClient)
			throws ClientDoesntExistException, ProprietairesCannotBeSavedException {
		// TODO Auto-generated method stub

		if (dao.findById(idClient).isPresent()) {
			throw new ProprietairesCannotBeSavedException("Le propriétaire existe déjà");
		}
		try {
			dao.addProprietaireFromClient(idClient);
		} catch (Exception e) {
			throw new ClientDoesntExistException(
					"Proprietaire can not be created because Client doesn't exist with this id : " + idClient);
		}

	}

	public Proprietaires saveProprietaires(Proprietaires proprietaire)
			throws ProprietairesCannotBeSavedException, ClientCannotBeSavedException, ClientCannotBeFoundException,
			ProprietairesCannotBeFoundException, ClientDoesntExistException {

		boolean booleana = (!findProprietaireByNom(proprietaire.getNom()).isEmpty() && searchProprietaireByNumeroTelephonePrivee(proprietaire.getNumeroTelephonePrivee()).isPresent());
		
		if (!findProprietaireByNom(proprietaire.getNom()).isEmpty() && searchProprietaireByNumeroTelephonePrivee(proprietaire.getNumeroTelephonePrivee()).isPresent()){
			
			
			Client clientiold = sClient.findClientByNumeroTelephone(proprietaire.getNumeroTelephonePrivee()).orElse(null);
			clientiold.setAdresse(proprietaire.getAdresse());

			try {
				sClient.updateClient(clientiold);

			} catch (ClientCannotBeUpdatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Proprietaires propriold = findProprietaireByNumeroTelephonePrivee(proprietaire.getNumeroTelephonePrivee()).orElse(proprietaire);
			propriold.setAdresse(proprietaire.getAdresse());
			
			try {
				updateProprietaires(propriold);
				
			} catch (ProprietairesCannotBeUpdatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return propriold;		
		}
		
		else if (!sClient.findClientByNom(proprietaire.getNom()).isEmpty() && sClient.findClientByNumeroTelephone(proprietaire.getNumeroTelephonePrivee()).isPresent() && !booleana){
			
			Client clientiold = sClient.findClientByNumeroTelephone(proprietaire.getNumeroTelephonePrivee()).orElse(null);

			clientiold.setAdresse(proprietaire.getAdresse());

			try {
				sClient.updateClient(clientiold);

			} catch (ClientCannotBeUpdatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			addProprietaireFromClient(sClient.findClientByNumeroTelephone(proprietaire.getNumeroTelephonePrivee()).get().getIdClient());
			
			Proprietaires prop = findProprietaireByNumeroTelephonePrivee(proprietaire.getNumeroTelephonePrivee()).orElse(proprietaire);
			prop.setAdresse(proprietaire.getAdresse());
			prop.setNumeroTelephoneTravail(proprietaire.getNumeroTelephoneTravail());
			prop.setPrenom(proprietaire.getPrenom());

			
			return prop;		
		}
		
		else {
		
		Client client = Client.builder().nom(proprietaire.getNom())
				.numeroTelephone(proprietaire.getNumeroTelephonePrivee()).adresse(proprietaire.getAdresse()).build();
		
		Client client2 = sClient.saveClient(client);

		Proprietaires proprietaire2 = Proprietaires.builder().idProprietaire(client2.getIdClient())
				.nom(proprietaire.getNom()).numeroTelephonePrivee(proprietaire.getNumeroTelephonePrivee())
				.numeroTelephoneTravail(proprietaire.getNumeroTelephoneTravail()).prenom(proprietaire.getPrenom())
				.adresse(proprietaire.getAdresse()).build();

		try {
			dao.save(proprietaire2);
			log.info("Save proprietaire with success");
		} catch (Exception e) {
			throw new ProprietairesCannotBeSavedException("Can not save proprietaire:" + proprietaire2);
		}
		
		return proprietaire2;
		}
		
	}

	@Override
	public List<Proprietaires> findProprietairesItalie() {
		return dao.findProprietairesItalie();
	}

	@Override
	public Optional<Proprietaires> findProprietaireItalieById(Long idProprietaireItalie)
			throws ProprietairesCannotBeFoundException {
		return dao.findProprietaireItalieById(idProprietaireItalie);
	}
	
	
}


