package com.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Client;
import com.exception.ClientCannotBeDeletetException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;
import com.exception.ClientCannotBeUpdatedException;
import com.exception.VisiteCanNotBeFoundException;
import com.repository.ClientRepository;
import com.service.ClientService;

import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @author Adeline Scherb
 *
 */
@Service
@Slf4j 
public class ClientServiceImp implements ClientService{
	
	@Autowired
	ClientRepository repo;
	
	

	public Client saveClient(Client client)throws ClientCannotBeSavedException{
		Client clientTuple = null;
		if (client.getIdClient() != null) {
			throw new ClientCannotBeSavedException("Cannot be created because id!=null");
		}
		try {
			log.info("Client will be create");
			clientTuple = repo.save(client);
			log.info("Client created with success");
		}
		catch (Exception e) {
			log.warn("Client cannot be created");
			throw new ClientCannotBeSavedException(client +" cannot be created");
		}
		return clientTuple; 
	}

	public Client updateClient(Client client) throws ClientCannotBeUpdatedException{
		Client clientTuple = null;
		if (client.getIdClient() == null) {
			throw new ClientCannotBeUpdatedException("Cannot be updated because id=null");
		}
		try {
			log.info("Client will be update");
			clientTuple = repo.save(client);
			log.info("Client updated with success");
		}
		catch (Exception e) {
			log.warn("Client cannot be updated");
			throw new ClientCannotBeUpdatedException(client +" cannot be updated");
		}
		return clientTuple; 
	}

	@Override
	public void deleteClientById(Long id) throws ClientCannotBeDeletetException{
		try {
			log.info("Client will be delete");
			repo.deleteById(id);
			log.info("Client deleted with success");
		} catch (Exception e) {
			log.warn("Client cannot be deleted");
			throw new ClientCannotBeDeletetException("Client Cannot be deleted with id : " + id);
		}
	}

	@Override

	public Client findClientById(Long id) throws ClientCannotBeFoundException{
		Optional<Client> client;
		try {
			client = repo.findById(id);
			log.info("Client found");
		} catch (Exception e) {
			log.warn("Client cannot be found");
			throw new ClientCannotBeFoundException("Client cannot be found with id : " + id);
		}
		if(client.isPresent()) {
			return client.get();
			}
		else {
			throw new ClientCannotBeFoundException("Client cannot be found with id : " + id);
		}	
	}

	@Override

	public List<Client> findClients() throws ClientCannotBeFoundException{
		List<Client> clients = new ArrayList<Client>();
		try {
			log.info("Clients will be find");
			clients=repo.findAll();
			log.info("Clients found with success");
		} catch (Exception e) {
			log.warn("Clients cannot be found");
			throw new ClientCannotBeFoundException("Clients cannot be found");
		}
		return clients;
	}

	
	@Override
	public List<Client> findClientByNom(String nom) throws ClientCannotBeFoundException {
		// TODO Auto-generated method stub
		List<Client> clients = null;
		try {
			clients=repo.findClientByNom(nom);
			log.info("Clients found with success");
		} catch (Exception e) {
			log.warn("Clients cannot be found");
			throw new ClientCannotBeFoundException("Clients cannot be found");
		}
		return clients;
	}


	@Override
	public Optional<Client> findClientByNumeroTelephone(String numeroTelephone) throws ClientCannotBeFoundException {
		// TODO Auto-generated method stub
		Optional<Client> client = null;
		try {
			client = repo.findClientByNumeroTelephone(numeroTelephone);
			log.info("Client found");
		} catch (Exception e) {
			log.warn("Client cannot be found");
			throw new ClientCannotBeFoundException("Client cannot be found with his phone number : " + numeroTelephone);
		}
		return client;
	}

	@Override
	public List<Client> findClientByClasseStandard(Long idClasseStandard) {
		return repo.findClientByClasseStandard(idClasseStandard);
	}

	@Override
	public List<Client> findClientByClasseStandardBienImmobilier(Long idBienImmobilier) {
		return repo.findClientByClasseStandardBienImmobilier(idBienImmobilier);
	}
	
	@Override
	public List<Client> findClientByVisiteOnBienImmobilier(Long idBienImmobilier) {
		// TODO Auto-generated method stub
		return repo.findClientByVisiteOnBienImmobilier(idBienImmobilier);
	}

	@Override
	public List<Client> findLocataireInItaly() {
		// TODO Auto-generated method stub
		return repo.findLocataireInItaly();
	}


	
}
