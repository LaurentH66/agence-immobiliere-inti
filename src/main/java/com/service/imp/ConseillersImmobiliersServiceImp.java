package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.ConseillersImmobiliers;
import com.exception.CanNotDeleteConseillersImmobiliersByIdException;
import com.exception.CanNotFindConseillersImmobiliersException;
import com.exception.CanNotFindAllConseillersImmobiliersException;
import com.exception.CanNotSaveConseillersImmobiliersException;
import com.exception.CanNotUpdateConseillersImmobiliersException;
import com.exception.ClientCannotBeFoundException;
import com.repository.ConseillersImmobiliersRepository;
import com.service.ConseillersImmobiliersService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Quentin PERINE
 *
 */

@Slf4j
@Service
public class ConseillersImmobiliersServiceImp implements ConseillersImmobiliersService{

	@Autowired
	ConseillersImmobiliersRepository cirepo;
	
	@Override
	public ConseillersImmobiliers saveConseillersImmobiliers(ConseillersImmobiliers conseillersImmobiliers) throws CanNotSaveConseillersImmobiliersException {
		// TODO Auto-generated method stub
		ConseillersImmobiliers conseillersImmobiliersR = null;
		
		try {
			conseillersImmobiliersR = cirepo.save(conseillersImmobiliers);
			log.info("Save conseillersImmobiliers with success");
		} catch (Exception e) {
			throw new CanNotSaveConseillersImmobiliersException("Can not save conseillersImmobiliers : "+conseillersImmobiliers);
		}
		
		return conseillersImmobiliersR;
	}

	@Override
	public ConseillersImmobiliers updateConseillersImmobiliers(ConseillersImmobiliers conseillersImmobiliers) throws CanNotUpdateConseillersImmobiliersException {
		// TODO Auto-generated method stub
		if (conseillersImmobiliers.getIdConseillersImmobiliers() == null) throw new CanNotUpdateConseillersImmobiliersException("ConseillersImmobiliers can not be updated");
		ConseillersImmobiliers conseillersImmobiliersR = null;
		try {
			conseillersImmobiliersR = cirepo.save(conseillersImmobiliers);
			log.info("Update conseillersImmobiliers with success");
		} catch (Exception e) {
			throw new CanNotUpdateConseillersImmobiliersException("ConseillersImmobiliers can not be updated");
		}
		
		return conseillersImmobiliersR;
	}

	@Override
	public void deleteConseillersImmobiliersById(Long id) throws CanNotDeleteConseillersImmobiliersByIdException {
		// TODO Auto-generated method stub
		try {
			cirepo.deleteById(id);
			log.info("Delete conseillersImmobiliers by id with success");
		} catch (Exception e) {
			throw new CanNotDeleteConseillersImmobiliersByIdException("ConseillersImmobiliers can not be delted with id = "+id);
		} 
	}

	@Override
	public ConseillersImmobiliers findConseillersImmobiliersById(Long id) throws CanNotFindConseillersImmobiliersException {
		// TODO Auto-generated method stub
		Optional<ConseillersImmobiliers> conseillersImmobilier;
		try {
			conseillersImmobilier = cirepo.findById(id);
			
			log.info("Find conseillersImmobiliers with success");
		} catch (Exception e) {
			throw new CanNotFindConseillersImmobiliersException("ConseillersImmobiliers can not be found with id = "+id);
		}
		if(conseillersImmobilier.isPresent()) {
			return conseillersImmobilier.get();
			}
		else {
			throw new CanNotFindConseillersImmobiliersException("ConseillersImmobiliers can not be found with id = "+id);
		}
	}

	@Override
	public List<ConseillersImmobiliers> findAllConseillersImmobilierss() throws CanNotFindAllConseillersImmobiliersException {
		// TODO Auto-generated method stub
		List<ConseillersImmobiliers> conseillersImmobilierss = null;
		try {
			conseillersImmobilierss = cirepo.findAll();
			log.info("Find all conseillersImmobilierss with success");
		} catch (Exception e) {
			throw new CanNotFindAllConseillersImmobiliersException("ConseillersImmobilierss can not be found");
		}
		
		return conseillersImmobilierss;
	}

	@Override
	public Optional<ConseillersImmobiliers> findConseillersImmobiliersByLogin(String login)
			throws CanNotFindConseillersImmobiliersException {
		// TODO Auto-generated method stub
		Optional<ConseillersImmobiliers> conseillerImmobilier;
		try {
			conseillerImmobilier = cirepo.findConseillersImmobiliersByLogin(login);
			
			log.info("Conseiller immobilier found");
		} catch (Exception e) {
			log.warn("Conseiller immobilier cannot be found");
			throw new CanNotFindConseillersImmobiliersException("Conseiller immobilier cannot be found with login : " + login);
		}
		if (!conseillerImmobilier.isPresent()) {
			throw new CanNotFindConseillersImmobiliersException("Conseiller immobilier cannot be found with login : " + login);
		}
		return conseillerImmobilier;
	}


	@Override
	public List<ConseillersImmobiliers> findConseillersImmobiliersByNom(String nom)
			throws CanNotFindAllConseillersImmobiliersException {
		// TODO Auto-generated method stub
		List<ConseillersImmobiliers> conseillersImmobilierss = null;
		try {
			conseillersImmobilierss = cirepo.findConseillersImmobiliersByNom(nom);
			log.info("Find all conseillersImmobilierss with success");
		} catch (Exception e) {
			throw new CanNotFindAllConseillersImmobiliersException("ConseillersImmobilierss can not be found");
		}
		
		return conseillersImmobilierss;
	
	}

	@Override
	public List<ConseillersImmobiliers> findConseillersImmobiliersByPrenom(String prenom)
			throws CanNotFindAllConseillersImmobiliersException {
		// TODO Auto-generated method stub
		List<ConseillersImmobiliers> conseillersImmobilierss = null;
		try {
			conseillersImmobilierss = cirepo.findConseillersImmobiliersByPrenom(prenom);
			log.info("Find all conseillersImmobilierss with success");
		} catch (Exception e) {
			throw new CanNotFindAllConseillersImmobiliersException("ConseillersImmobilierss can not be found");
		}
		
		return conseillersImmobilierss;
	
	}


}
