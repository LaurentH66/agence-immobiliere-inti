package com.service.imp;

import java.util.List;
import java.util.Optional;

import javax.management.relation.RoleNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Role;
import com.entity.Visite;
import com.exception.RoleCannotBeFoundException;
import com.repository.RoleRepository;
import com.service.RoleService;

@Service
public class RoleServiceImp implements RoleService {
	
	@Autowired
	RoleRepository dao;
	
	@Override
	public Role saveRole(Role role) {
		// TODO Auto-generated method stub
		return dao.save(role);
	}

	@Override
	public void deleteRoleById(Long id) {
		// TODO Auto-generated method stub
		dao.deleteById(id);
	}

	@Override
	public Role findRoleById(Long idRole) throws RoleCannotBeFoundException {
		Optional<Role> role;
		
		try {
			role = dao.findById(idRole);
		} catch (Exception e) {
			throw new RoleCannotBeFoundException("Can not found Role with Id : "+idRole);
		}

		if(role.isPresent()) {
			return role.get();
		}
		else {
			throw new RoleCannotBeFoundException("Can not found Role with Id : "+idRole);
		}
	}

	@Override
	public List<Role> findRoles() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

	@Override
	public Role updateRole(Role role) {
		// TODO Auto-generated method stub
		return dao.save(role);
	}

}
