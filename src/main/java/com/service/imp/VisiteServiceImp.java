package com.service.imp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Visite;
import com.exception.VisiteCanNotBeDeletedException;
import com.exception.VisiteCanNotBeFoundException;
import com.exception.VisiteCanNotBeUpdatedException;
import com.exception.VisiteCannotBeSavedException;
import com.exception.VisiteIDsAlreadyOccupiedAtThisMomentException;
import com.exception.VisitesCanNotBeFoundException;
import com.repository.VisiteRepository;
import com.service.VisiteService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Charles
 */
@Service
@Slf4j
public class VisiteServiceImp implements VisiteService {

	@Autowired
	VisiteRepository dao;

	@Override
	public void saveOrUpdateVisite(Visite visite) {
		dao.save(visite);

	}

	
	public Visite saveVisite(Visite visite)
			throws VisiteCannotBeSavedException, VisiteIDsAlreadyOccupiedAtThisMomentException {
		Visite visite2 = null;

			if(dao.existsByDateHeureIdClient(visite.getDateVisite(), visite.getHeureVisite(), visite.getClient().getIdClient())==true)
				throw new VisiteIDsAlreadyOccupiedAtThisMomentException(visite.getDateVisite()+" "+visite.getHeureVisite()+" "+ visite.getClient().getIdClient());
			
			if(dao.existsByDateHeureIdBienImmobilier(visite.getDateVisite(), visite.getHeureVisite(), visite.getBiensImmobiliers().getIdBienImmobilier())==true)
				throw new VisiteIDsAlreadyOccupiedAtThisMomentException(visite.getDateVisite()+" "+visite.getHeureVisite()+" "+ visite.getBiensImmobiliers().getIdBienImmobilier());
			
			if(dao.existsByDateHeureIdConseiller(visite.getDateVisite(), visite.getHeureVisite(), visite.getConseillerImmobilier().getIdConseillersImmobiliers())==true)
				throw new VisiteIDsAlreadyOccupiedAtThisMomentException(visite.getDateVisite()+" "+visite.getHeureVisite()+" "+ visite.getConseillerImmobilier().getIdConseillersImmobiliers());
		
			
			try {
					visite2 = dao.save(visite);
					log.info("Save visite with success");
				} catch (Exception e) {
					e.printStackTrace();
					throw new VisiteCannotBeSavedException("Can not save visite:" + visite);
					
				}
		return visite2;
	}

	@Override
	public void deleteVisite(Visite visite) throws VisiteCanNotBeDeletedException {
		try {
			dao.delete(visite);
			log.info("Visite deleted with success");
		} catch (Exception e) {
			throw new VisiteCanNotBeDeletedException("Visite " + visite + " can not be deleted");
		}

	}

	public void deleteVisiteById(Long id) throws VisiteCanNotBeDeletedException {
		try {
			dao.deleteById(id);
			log.info("Visite deleted by id with success");
		} catch (Exception e) {
			throw new VisiteCanNotBeDeletedException("Visite can not be deleted with id = " + id);
		}
	}

	@Override
	public Visite findVisiteById(Long id) throws VisiteCanNotBeFoundException {
		Optional<Visite> visite;

		try {
			visite = dao.findById(id);
			log.info("Visite found with success");
		} catch (Exception e) {
			throw new VisiteCanNotBeFoundException("Visite can not be found with id = " + id);
		}

		if (visite.isPresent()) {
			return visite.get();
		} else {
			throw new VisiteCanNotBeFoundException("Visite can not be found with id = " + id);
		}
	}

	@Override
	public List<Visite> findVisites() throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;

		try {
			visites = dao.findAll();
			log.info("Find all visite with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}

		return visites;

	}

	public Visite updateVisite(Visite visite)
			throws VisiteCanNotBeUpdatedException, VisiteIDsAlreadyOccupiedAtThisMomentException {
		if (visite.getIdVisite() == null) {
			throw new VisiteCanNotBeUpdatedException("visite can not be updated : " + visite);
		}
		Visite visiteUpdate = null;
		
			if(dao.existsByDateHeureIdClient(visite.getDateVisite(), visite.getHeureVisite(), visite.getClient().getIdClient())==true)
				throw new VisiteIDsAlreadyOccupiedAtThisMomentException(visite.getDateVisite()+" "+visite.getHeureVisite()+" "+ visite.getClient().getIdClient());
			
			if(dao.existsByDateHeureIdBienImmobilier(visite.getDateVisite(), visite.getHeureVisite(), visite.getBiensImmobiliers().getIdBienImmobilier())==true)
				throw new VisiteIDsAlreadyOccupiedAtThisMomentException(visite.getDateVisite()+" "+visite.getHeureVisite()+" "+ visite.getBiensImmobiliers().getIdBienImmobilier());
			
			if(dao.existsByDateHeureIdConseiller(visite.getDateVisite(), visite.getHeureVisite(), visite.getConseillerImmobilier().getIdConseillersImmobiliers())==true)
				throw new VisiteIDsAlreadyOccupiedAtThisMomentException(visite.getDateVisite()+" "+visite.getHeureVisite()+" "+ visite.getConseillerImmobilier().getIdConseillersImmobiliers());		
		
			try {
					visiteUpdate = dao.save(visite);
					log.info("Update visite with success");

				} catch (Exception e) {
					throw new VisiteCanNotBeUpdatedException("visite can not be updated : " + visite);
				}
		return visiteUpdate;
	}

	@Override
	public List<Visite> findVisiteByDate(String dateVisite) throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;

		try {
			visites = dao.findVisiteByDate(dateVisite);
			log.info("Find all visites with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}

		return visites;

	}

	@Override
	public List<Visite> findVisiteByHeure(String heureVisite) throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;

		try {
			visites = dao.findVisiteByHeure(heureVisite);
			log.info("Find all visites with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}

		return visites;

	}

	@Override
	public List<Visite> findVisiteIdBienImmobilierIdClient(Long idBienImmobilier, Long idClient)
			throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdBienImmobilierIdClient(idBienImmobilier, idClient);
			log.info("Find visites with success");

		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;
	}

	public List<Visite> findVisiteIdBienImmobilierIdConseiller(Long idBienImmobilier, Long idConseillersImmobiliers)
			throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdBienImmobilierIdConseiller(idBienImmobilier, idConseillersImmobiliers);
			log.info("Find visites with success");

		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;
	}

	@Override
	public List<Visite> findVisiteIdBienImmobilierDate(Long idBienImmobilier, String dateVisite)
			throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdBienImmobilierDate(idBienImmobilier, dateVisite);
			log.info("Find visites with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;
	}

	@Override
	public List<Visite> findVisiteIdClientDate(Long idClient, String dateVisite) throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdClientDate(idClient, dateVisite);
			log.info("Find visites with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;
	}

	@Override
	public List<Visite> findVisiteIdConseillerDate(Long idConseillersImmobiliers, String dateVisite)
			throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdConseillerDate(idConseillersImmobiliers, dateVisite);
			log.info("Find visites with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;
	}

	@Override
	public Optional<Visite> findVisiteByDateHeureIdConseiller(String dateVisite, String heureVisite,
			Long idConseillersImmobiliers) throws VisitesCanNotBeFoundException {
		Optional<Visite> visite;
		try {
			visite = dao.findVisiteByDateHeureIdConseiller(dateVisite, heureVisite, idConseillersImmobiliers);
			log.info("Find visite with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException(
					"Visite can not be found at date : " + dateVisite + " " + heureVisite + " with this conseiller");
		}
		if (!visite.isPresent()) {
			throw new VisitesCanNotBeFoundException(
					"Visite can not be found at date : " + dateVisite + " " + heureVisite + " with this conseiller");
		}
		return visite;
	}

	@Override
	public Optional<Visite> findVisiteByDateHeureIdClient(String dateVisite, String heureVisite, Long idClient)
			throws VisitesCanNotBeFoundException {
		Optional<Visite> visite;
		try {
			visite = dao.findVisiteByDateHeureIdClient(dateVisite, heureVisite, idClient);
			log.info("Find visite with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException(
					"Visite can not be found at date : " + dateVisite + " " + heureVisite + " with this client");
		}
		if (!visite.isPresent()) {
			throw new VisitesCanNotBeFoundException(
					"Visite can not be found at date : " + dateVisite + " " + heureVisite + " with this client");
		}
		return visite;
	}

	@Override
	public Optional<Visite> findVisiteByDateHeureIdBienImmobilier(String dateVisite, String heureVisite,
			Long id_bien_immobilier) throws VisitesCanNotBeFoundException {
		Optional<Visite> visite;
		try {
			visite = dao.findVisiteByDateHeureIdBienImmobilier(dateVisite, heureVisite, id_bien_immobilier);
			log.info("Find visite with success");
		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visite can not be found at date : " + dateVisite + " "
					+ heureVisite + " with this bien immobilier");
		}
		if (!visite.isPresent()) {
			throw new VisitesCanNotBeFoundException("Visite can not be found at date : " + dateVisite + " "
					+ heureVisite + " with this bien immobilier");
		}
		return visite;
	}

	@Override
	public List<Visite> findVisiteIdBienImmobilier(Long idBienImmobilier) throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdBienImmobilier(idBienImmobilier);
			log.info("Find visites with success");

		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;

	}

	@Override
	public List<Visite> findVisiteIdClient(Long idClient) throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdClient(idClient);
			log.info("Find visites with success");

		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;

	}

	@Override
	public List<Visite> findVisiteIdConseiller(Long idConseillersImmobiliers) throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteIdConseiller(idConseillersImmobiliers);
			log.info("Find visites with success");

		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;

	}


	@Override
	public List<Visite> findVisiteByLoginConseiller(String loginConseillersImmobiliers)
			throws VisitesCanNotBeFoundException {
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteByLoginConseiller(loginConseillersImmobiliers);
			log.info("Find visites with success");

		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;
	}


	@Override
	public List<Visite> findVisiteByNomClient(String nomClient) throws VisitesCanNotBeFoundException {
		// TODO Auto-generated method stub
		List<Visite> visites = null;
		try {
			visites = dao.findVisiteByNomClient(nomClient);
			log.info("Find visites with success");

		} catch (Exception e) {
			throw new VisitesCanNotBeFoundException("Visites can not be found");
		}
		return visites;
	}
	}



