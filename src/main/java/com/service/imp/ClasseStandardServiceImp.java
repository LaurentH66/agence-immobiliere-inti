package com.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.BiensImmobiliers;
import com.entity.ClasseStandard;
import com.entity.TypeBienImmobilier;
import com.entity.TypeOffre;
import com.exception.ClasseStandardCannotBeDeletetException;
import com.exception.ClasseStandardCannotBeFoundException;
import com.exception.ClasseStandardCannotBeSavedException;
import com.exception.ClasseStandardCannotBeUpdatedException;
import com.exception.ClientCannotBeFoundException;
import com.repository.ClasseStandardRepository;
import com.service.ClasseStandardService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Adeline Scherb
 *
 */
@Service
@Slf4j
public class ClasseStandardServiceImp implements ClasseStandardService {

	@Autowired
	ClasseStandardRepository repo;

	@Override
	public ClasseStandard saveClasseStandard(ClasseStandard classeStandard)
			throws ClasseStandardCannotBeSavedException {
		ClasseStandard classeStandardTuple = null;
		if (classeStandard.getIdClasseStandard() != null) {
			throw new ClasseStandardCannotBeSavedException("Cannot be created because id!=null");
		}
		try {
			log.info("ClasseStandard will be create");
			classeStandardTuple = repo.save(classeStandard);
			log.info("ClasseStandard created with success");
		} catch (Exception e) {
			log.warn("ClasseStandard cannot be created");
			throw new ClasseStandardCannotBeSavedException(classeStandard + " cannot be created");
		}
		return classeStandardTuple;
	}

	@Override
	public ClasseStandard updateClasseStandard(ClasseStandard classeStandard)
			throws ClasseStandardCannotBeUpdatedException {
		ClasseStandard classeStandardTuple = null;
		if (classeStandard.getIdClasseStandard() == null) {
			throw new ClasseStandardCannotBeUpdatedException("Cannot be updated because id=null");
		}
		try {
			log.info("ClasseStandard will be update");
			classeStandardTuple = repo.save(classeStandard);
			log.info("ClasseStandard updated with success");
		} catch (Exception e) {
			log.warn("ClasseStandard cannot be updated");
			throw new ClasseStandardCannotBeUpdatedException(classeStandard + " cannot be updated");
		}
		return classeStandardTuple;
	}

	@Override
	public void deleteClasseStandardById(Long id) throws ClasseStandardCannotBeDeletetException {
		try {
			log.info("ClasseStandard will be delete");
			repo.deleteById(id);
			log.info("ClasseStandard deleted with success");
		} catch (Exception e) {
			log.warn("ClasseStandard cannot be deleted");
			throw new ClasseStandardCannotBeDeletetException("ClasseStandard Cannot be deleted with id : " + id);
		}
	}

	@Override
	public ClasseStandard findClasseStandardById(Long id) throws ClasseStandardCannotBeFoundException {
		Optional<ClasseStandard> classeStandard;
		try {
			classeStandard = repo.findById(id);
			log.info("ClasseStandard found");
		} catch (Exception e) {
			log.warn("ClasseStandard cannot be found");
			throw new ClasseStandardCannotBeFoundException("ClasseStandard cannot be found with id : " + id);
		}

		if (classeStandard.isPresent()) {
			return classeStandard.get();
		} else {
			throw new ClasseStandardCannotBeFoundException("ClasseStandard cannot be found with id : " + id);
		}
	}

	@Override
	public List<ClasseStandard> findClasseStandards() throws ClasseStandardCannotBeFoundException {
		List<ClasseStandard> classesStandards = new ArrayList<ClasseStandard>();
		try {
			log.info("ClassesStandards will be find");
			classesStandards = repo.findAll();
			log.info("ClassesStandards found with success");
		} catch (Exception e) {
			log.warn("ClassesStandards cannot be found");
			throw new ClasseStandardCannotBeFoundException("ClassesStandards cannot be found");
		}
		return classesStandards;
	}

	@Override
	public void insererBienDansClasseStandard(BiensImmobiliers bienImmo, int superficie, Double prix,
			TypeOffre typeOffre, TypeBienImmobilier typeBien) {
		// TODO Auto-generated method stub
		int supMax = 0;
		int prixMaxF = 0;

		if (typeOffre.getModeOffre().equals("Achat")) {
			if (typeBien.getTypeBien().equals("Appartement") || typeBien.getTypeBien().equals("Studio")) {
				int nb3 = (superficie / 10);
				supMax = (nb3 + 1) * 10;

				double prixMax = Math.floor(prix);
				int nb = (int) prixMax / 50000;
				if ((int) prixMax % 50000 == 0 && prix < (nb) * 50000)
					prixMaxF = nb * 50000;
				else
					prixMaxF = (nb + 1) * 50000;
			}

			if (typeBien.getTypeBien().equals("Maison")) {

				int nb2 = superficie / 50;
				supMax = (nb2 + 1) * 50;

				double prixMax = Math.floor(prix);
				int nb = (int) prixMax / 100000;
				if ((int) prixMax % 100000 == 0 && prixMax < (nb) * 100000)
					prixMaxF = nb * 100000;
				else
					prixMaxF = (nb + 1) * 100000;
			}

			if (typeBien.getTypeBien().equals("Entrepôt") || typeBien.getTypeBien().equals("Terrain")) {
				int nb2 = superficie / 200;
				supMax = (nb2 + 1) * 200;

				double prixMax = Math.floor(prix);
				int nb = (int) prixMax / 200000;
				if ((int) prixMax % 200000 == 0 && prixMax < (nb + 1) * 200000)
					prixMaxF = nb * 200000;
				else
					prixMaxF = (nb + 1) * 200000;
			}
		}

		if (typeOffre.getModeOffre().equals("Location")) {
			if (typeBien.getTypeBien().equals("Appartement") || typeBien.getTypeBien().equals("Studio")) {
				int nb3 = (superficie / 10);
				supMax = (nb3 + 1) * 10;
				double prixMax = Math.floor(prix);
				int nb = (int) prixMax / 500;
				if ((int) prixMax % 500 == 0 && prix < (nb) * 500)
					prixMaxF = nb * 500;
				else
					prixMaxF = (nb + 1) * 500;
				
				System.out.println(supMax + "  " + prixMaxF);
			}

			if (typeBien.getTypeBien().equals("Maison")) {

				int nb2 = superficie / 50;
				supMax = (nb2 + 1) * 50;

				double prixMax = Math.floor(prix);
				int nb = (int) prixMax / 1000;
				if ((int) prixMax % 1000 == 0 && prixMax < (nb) * 1000)
					prixMaxF = nb * 1000;
				else
					prixMaxF = (nb + 1) * 1000;
			}

			if (typeBien.getTypeBien().equals("Entrepôt") || typeBien.getTypeBien().equals("Terrain")) {
				int nb2 = superficie / 200;
				supMax = (nb2 + 1) * 200;

				double prixMax = Math.floor(prix);
				int nb = (int) prixMax / 2000;
				if ((int) prixMax % 2000 == 0 && prixMax < (nb + 1) * 2000)
					prixMaxF = nb * 2000;
				else
					prixMaxF = (nb + 1) * 2000;
			}
		}

		System.out.println(supMax + "  " + prixMaxF);

		if (findClasseStandardBySuperficiePrixTypeOffre(supMax, (double) prixMaxF, typeOffre, typeBien) != null)
			bienImmo.setClasseStandard(
					findClasseStandardBySuperficiePrixTypeOffre(supMax, (double) prixMaxF, typeOffre, typeBien));

		else {
			ClasseStandard newClasseStandard = ClasseStandard.builder().superficieMaximale(supMax)
					.prixMaximum((double) prixMaxF).typeOffreId(typeOffre).typeBienImmobilierId(typeBien).build();
			System.out.println(newClasseStandard);
			repo.save(newClasseStandard);
			bienImmo.setClasseStandard(
					findClasseStandardBySuperficiePrixTypeOffre(supMax, (double) prixMaxF, typeOffre, typeBien));
		}

	}

	@Override
	public ClasseStandard findClasseStandardBySuperficiePrixTypeOffre(int superficie, double prix, TypeOffre typeOffre,
			TypeBienImmobilier typeBien) {
		// TODO Auto-generated method stub
		return repo.findClasseStandardBySuperficiePrixTypeOffre(superficie, prix, typeOffre, typeBien);
	}

	public List<ClasseStandard> findClasseStandardBySuperficieMaxPrixMax(int superficie, double prix,
			TypeOffre typeOffre, TypeBienImmobilier typeBien) {
		return repo.findClasseStandardBySuperficieMaxPrixMax(superficie, prix, typeOffre, typeBien);
	}

}
