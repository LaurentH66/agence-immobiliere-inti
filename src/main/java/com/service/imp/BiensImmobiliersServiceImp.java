package com.service.imp;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.entity.BiensImmobiliers;
import com.entity.Proprietaires;
import com.exception.BiensImmobiliersCannotBeDeletetException;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.BiensImmobiliersCannotBeSavedException;
import com.exception.BiensImmobiliersCannotBeUpdatedException;
import com.exception.CanNotFindAllAdresseException;
import com.exception.ClasseStandardCannotBeFoundException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ProprietairesCannotBeFoundException;
import com.repository.AdresseRepository;
import com.repository.BiensImmobiliersRepository;
import com.repository.ContratRepository;
import com.repository.ProprietairesRepository;
import com.service.BiensImmobiliersService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BiensImmobiliersServiceImp implements BiensImmobiliersService {

	@Autowired
	BiensImmobiliersRepository dao;

	@Autowired
	ProprietairesRepository daop;

	@Autowired
	ContratRepository daoc;

	@Autowired
	AdresseRepository adao;

	/**
	 * @author Damien
	 * @param bienImmobilier
	 * @return bienImmobilier
	 * @throws BiensImmobiliersCannotBeSavedException
	 */
	@Override
	public BiensImmobiliers saveBiensImmobiliers(BiensImmobiliers bienImmobilier)
			throws BiensImmobiliersCannotBeSavedException {
		BiensImmobiliers bienImmobilier2 = null;
		try {
			bienImmobilier2 = dao.save(bienImmobilier);
			log.info("Save BienImmobilier with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeSavedException("Can not save BienImmobilier:" + bienImmobilier);
		}
		return bienImmobilier2;
	}

	/**
	 * @author Damien
	 * @param bienImmobilier
	 * @return bienImmobilier
	 * @throws BiensImmobiliersCannotBeUpdatedException
	 */
	@Override
	public BiensImmobiliers updateBiensImmobiliers(BiensImmobiliers bienImmobilier)
			throws BiensImmobiliersCannotBeUpdatedException {
		if (bienImmobilier.getIdBienImmobilier() == null)
			throw new BiensImmobiliersCannotBeUpdatedException("BienImmobilier can not be updated");
		BiensImmobiliers bienImmobilier2 = null;
		try {
			bienImmobilier2 = dao.save(bienImmobilier);
			log.info("Update BienImmobilier with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeUpdatedException("Can not update BienImmobilier:" + bienImmobilier);
		}
		return bienImmobilier2;
	}

	/**
	 * 
	 * @param id
	 * @throws BiensImmobiliersCannotBeDeletetException
	 */
	@Override
	public void deleteBiensImmobiliersById(Long id) throws BiensImmobiliersCannotBeDeletetException {
		try {
			dao.deleteById(id);
			log.info("Delete BienImmobilier with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeDeletetException("Can not delete BienImmobilier with id " + id);
		}

	}

	/**
	 * @author Damien
	 * @param id
	 * @return bienImmobilier
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public BiensImmobiliers findBiensImmobiliersById(Long id) throws BiensImmobiliersCannotBeFoundException {
		// TODO Auto-generated method stub
		Optional<BiensImmobiliers> bienImmobilier;
		try {
			bienImmobilier = dao.findById(id);
			log.info("BienImmobilier found with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BienImmobilier can not be found with id = " + id);
		}

		if (bienImmobilier.isPresent()) {
			return bienImmobilier.get();
		} else {
			throw new BiensImmobiliersCannotBeFoundException("BienImmobilier can not be found with id = " + id);
		}
	}

	/**
	 * @author Damien
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliers() throws BiensImmobiliersCannotBeFoundException {
		// TODO Auto-generated method stub
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findAll();
			log.info("Find all BiensImmobiliers with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param max Cautionlocative
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMax(double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByCautionlocativeMax(max);
			log.info(" CautionlocativeMax BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("CautionlocativeMax BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Cautionlocative
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMin(double min)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByCautionlocativeMin(min);
			log.info(" CautionlocativeMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("CautionlocativeMin BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Cautionlocative
	 * @param2 max Cautionlocative
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMaxMin(double min, double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByCautionlocativeMaxMin(min, max);
			log.info(" CautionlocativeMaxMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException(
					"CautionlocativeMaxMin  BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param max Loyermensuel
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMax(double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByLoyermensuelMax(max);
			log.info(" LoyermensuelMax BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("LoyermensuelMax BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Loyermensuel
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMin(double min)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByLoyermensuelMin(min);
			log.info(" LoyermensuelMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("LoyermensuelMin BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Loyermensuel
	 * @param2 max Loyermensuel
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMaxMin(double min, double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByLoyermensuelMaxMin(min, max);
			log.info(" LoyermensuelMaxMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("LoyermensuelMaxMin  BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param max Prixlocation
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMax(double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixlocationMax(max);
			log.info(" PrixlocationMax BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixlocationMax BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Prixlocation
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMin(double min)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixlocationMin(min);
			log.info(" PrixlocationMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixlocationMin BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Prixlocation
	 * @param2 max Prixlocation
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMaxMin(double min, double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixlocationMaxMin(min, max);
			log.info(" PrixlocationMaxMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixlocationMaxMin  BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param max Prixachat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMax(double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixachatMax(max);
			log.info(" PrixachatMax BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixachatMax BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Prixachat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMin(double min)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixachatMin(min);
			log.info(" PrixachatMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixachatMin BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Prixachat
	 * @param2 max Prixachat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMaxMin(double min, double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixachatMaxMin(min, max);
			log.info(" PrixachatMaxMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixachatMaxMin  BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param max Prixachatdemande
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMax(double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixachatdemandeMax(max);
			log.info(" PrixachatdemandeMax BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixachatdemandeMax BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Prixachatdemande
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMin(double min)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixachatdemandeMin(min);
			log.info(" PrixachatdemandeMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("PrixachatdemandeMin BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Prixachatdemande
	 * @param2 max Prixachatdemande
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMaxMin(double min, double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByPrixachatdemandeMaxMin(min, max);
			log.info(" PrixachatdemandeMaxMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException(
					"PrixachatdemandeMaxMin  BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param max Revenucadastral
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMax(double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByRevenucadastralMax(max);
			log.info(" RevenucadastralMax BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("RevenucadastralMax BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Revenucadastral
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMin(double min)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByRevenucadastralMin(min);
			log.info(" RevenucadastralMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("RevenucadastralMin BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param min Revenucadastral
	 * @param2 max Revenucadastral
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMaxMin(double min, double max)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByRevenucadastralMaxMin(min, max);
			log.info(" RevenucadastralMaxMin BiensImmobiliers  with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException(
					"RevenucadastralMaxMin  BiensImmobiliers can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param typeBAil
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByTypeBail(String type)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByTypeBail(type);
			log.info(" Find BiensImmobiliers By TypeBail with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By TypeBail can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param garniture
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByGarniture(String type)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByGarniture(type);
			log.info(" Find BiensImmobiliers By Garniture with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By Garniture can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param Etat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByEtat(String type)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByEtat(type);
			log.info(" Find BiensImmobiliers By Etat with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By Etat can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param Status
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByStatus(String type)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByStatus(type);
			log.info(" Find BiensImmobiliers By Status with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By Status can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param ProprietaireId
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByProprietaireId(Long id)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByProprietaireId(id);
			log.info(" Find BiensImmobiliers By ProprietaireId with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By ProprietaireId can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param ContratId
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByContratId(Long id)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByContratId(id);
			log.info(" Find BiensImmobiliers By ContratId with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By ContratId can not be found");
		}

		return biensImmobiliers;
	}

	/**
	 * @author Damien
	 * @param AdresseId
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByAdresseId(Long id)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByAdresseId(id);
			log.info(" Find BiensImmobiliers By AdresseId with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By AdresseId can not be found");
		}

		return biensImmobiliers;
	}

	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByTypeOffre(String modeOffre) {
		// TODO Auto-generated method stub
		List<BiensImmobiliers> biensImmobiliers = null;
		return dao.findBiensImmobiliersByTypeOffre(modeOffre);
	}

	@Transactional
	@Override
	public void assignContratToBienImmobilier(Long idContrat, Long idBienImmobilier)
			throws ContratCanNotBeFoundException, BiensImmobiliersCannotBeFoundException {

		try {
			daoc.findById(idContrat);
		} catch (Exception e) {
			// TODO: handle exception
			throw new ContratCanNotBeFoundException("Can't find the contrat with idContrat : " + idContrat);
		}

		try {
			dao.assignContratToBienImmobilier(idContrat, idBienImmobilier);
		} catch (Exception e) {
			// TODO: handle exception
			throw new BiensImmobiliersCannotBeFoundException(
					"Can't find the bien immobilier with idBienImmobilier : " + idBienImmobilier);
		}
	}

	@Transactional
	@Override
	public void changementStatusBienImmobilier(Long idContrat, String numeroTelephonePrivee)
			throws BiensImmobiliersCannotBeUpdatedException, ProprietairesCannotBeFoundException {

		try {
			daop.findProprietaireByNumeroTelephonePrivee(numeroTelephonePrivee);
		} catch (Exception e) {
			// TODO: handle exception
			throw new ProprietairesCannotBeFoundException(
					"Proprietaire can not be found with this numero de telephone privee : " + numeroTelephonePrivee);
		}

		try {
			dao.changementStatusBienImmobilier(idContrat, numeroTelephonePrivee);
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeUpdatedException(
					"Bien immobilier can not be updated with the ID of the contrat : " + idContrat);
		}

	}
	
	public void changementStatusBienImmobilierLocation(Long idContrat) 
			throws BiensImmobiliersCannotBeUpdatedException{
	

		try {
			dao.changementStatusBienImmobilierLocation(idContrat);
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeUpdatedException(
					"Bien immobilier can not be updated with the ID of the contrat : " + idContrat);
		}

	}
	

	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByClasseStandardId(Long id)
			throws BiensImmobiliersCannotBeFoundException {
		List<BiensImmobiliers> biensImmobiliers = null;
		try {
			biensImmobiliers = dao.findBiensImmobiliersByClasseStandardId(id);
			log.info(" Find BiensImmobiliers By ClassStdId with success");
		} catch (Exception e) {
			throw new BiensImmobiliersCannotBeFoundException("BiensImmobiliers By AdresseId can not be found");
		}

		return biensImmobiliers;
	}

	public BiensImmobiliers assignContratToBienImmobilierWithReturn(Long idContrat, Long idBienImmobilier)
			throws ContratCanNotBeFoundException, BiensImmobiliersCannotBeFoundException {

		assignContratToBienImmobilier(idContrat, idBienImmobilier);

		return findBiensImmobiliersById(idBienImmobilier);

	}

	@Override
	public List<BiensImmobiliers> findBiensImmobiliersByAllCriteriaOfClasseStandard(String typeOffre, String typeBien,
			int prixMax, int superficieMax, int prixMin, int superficieMin, String ville) {
		// TODO Auto-generated method stub
		return dao.findBiensImmobiliersByAllCriteriaOfClasseStandard(typeOffre, typeBien, prixMax, superficieMax, prixMin, superficieMin, ville);
	}
	
	public List<BiensImmobiliers> findBiensImmobiliersItaliensByStatus(@Param("x") String status){
		return dao.findBiensImmobiliersItaliensByStatus(status);
		
	}
	
	public List<BiensImmobiliers> findBiensImmobiliersItalie(){
		return dao.findBiensImmobiliersItalie();
	}
	
	public Optional<BiensImmobiliers> findBienImmobilierItalieById(@Param("x") Long idBienImmobilier){
		return dao.findBienImmobilierItalieById(idBienImmobilier);
	}



	
	


	
	@Override
	public BiensImmobiliers assignProprietaireToBienImmobilierWithReturn(Long idProprietaire, Long idBienImmobilier)
			throws ProprietairesCannotBeFoundException, BiensImmobiliersCannotBeFoundException {
		// TODO Auto-generated method stub
		assignProprietaireToBienImmobilierWithReturn(idProprietaire, idBienImmobilier);
		
		return findBiensImmobiliersById(idBienImmobilier);
	}

//	@Override
//	public void assignAdresseToBienImmobilier(Long idAdresse, Long idBienImmobilier)
//			throws CanNotFindAllAdresseException, BiensImmobiliersCannotBeFoundException {
//		try {
//			adao.findById(idAdresse);
//		} catch (Exception e) {
//			throw new CanNotFindAllAdresseException("Can't find the adresse with idAdresse : " + idAdresse);
//		}
//		try {
//			dao.assignAdresseToBienImmobilier(idAdresse, idBienImmobilier);
//		} catch (Exception e) {
//			throw new BiensImmobiliersCannotBeFoundException(
//					"can't find the bien immobilier with idBienImmobilier : " + idBienImmobilier);
//		}
//
//	}
//
//	@Override
//	public BiensImmobiliers assignAdresseToBienImmobilierWithReturn(Long idAdresse, Long idBienImmobilier)
//			throws CanNotFindAllAdresseException, BiensImmobiliersCannotBeFoundException {
//		// TODO Auto-generated method stub
//		assignAdresseToBienImmobilier(idAdresse, idBienImmobilier);
//
//		return findBiensImmobiliersById(idBienImmobilier);
//	}

}