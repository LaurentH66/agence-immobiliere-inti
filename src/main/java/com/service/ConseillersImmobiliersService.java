package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.query.Param;

import com.entity.ConseillersImmobiliers;
import com.exception.CanNotDeleteConseillersImmobiliersByIdException;
import com.exception.CanNotFindConseillersImmobiliersException;
import com.exception.CanNotFindAllConseillersImmobiliersException;
import com.exception.CanNotSaveConseillersImmobiliersException;
import com.exception.CanNotUpdateConseillersImmobiliersException;

/**
 * 
 * @author Quentin PERINE
 *
 */
public interface ConseillersImmobiliersService {

	/**
	 * 
	 * @param ConseillersImmobiliers conseillersImmobiliers
	 * @return conseillersImmobiliers saved
	 */
	public ConseillersImmobiliers saveConseillersImmobiliers(ConseillersImmobiliers conseillersImmobiliers) throws CanNotSaveConseillersImmobiliersException;
	
	/**
	 * 
	 * @param ConseillersImmobiliers conseillersImmobiliers
	 * @return conseillersImmobiliers updated
	 */
	public ConseillersImmobiliers updateConseillersImmobiliers (ConseillersImmobiliers conseillersImmobiliers) throws CanNotUpdateConseillersImmobiliersException;
	
	/**
	 * 
	 * @param id of conseillersImmobiliers which is deleted
	 */
	public void deleteConseillersImmobiliersById(Long id) throws CanNotDeleteConseillersImmobiliersByIdException;
	
	/**
	 * 
	 * @param id of the conseillersImmobiliers we are looking for 
	 * @return ConseillersImmobiliers
	 */
	public ConseillersImmobiliers findConseillersImmobiliersById(Long id) throws CanNotFindConseillersImmobiliersException;
	
	/**
	 * 
	 * @return all conseillersImmobilierss
	 */
	public List<ConseillersImmobiliers> findAllConseillersImmobilierss() throws CanNotFindAllConseillersImmobiliersException;

	/**
	 * @author Laurent Hurtado
	 * @param login
	 * @return le conseiller immobilier utilisant ce login
	 * @throws CanNotFindConseillersImmobiliersException
	 */
	
	public Optional<ConseillersImmobiliers> findConseillersImmobiliersByLogin(@Param("x") String login) throws CanNotFindConseillersImmobiliersException;

	/**
	 * @author Laurent Hurtado
	 * @param nom
	 * @return les conseillers ayant ce nom
	 * @throws CanNotFindConseillersImmobiliersException
	 */
	
	public List<ConseillersImmobiliers> findConseillersImmobiliersByNom(@Param("x") String nom) throws CanNotFindAllConseillersImmobiliersException;

	/**
	 * @author Laurent Hurtado
	 * @param prenom
	 * @return les conseillers ayant ce prenom
	 * @throws CanNotFindConseillersImmobiliersException
	 */
	
	public List<ConseillersImmobiliers> findConseillersImmobiliersByPrenom(@Param("x") String prenom) throws CanNotFindAllConseillersImmobiliersException;
	
}
