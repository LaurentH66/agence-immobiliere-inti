package com.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.entity.BiensImmobiliers;
import com.exception.BiensImmobiliersCannotBeDeletetException;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.BiensImmobiliersCannotBeSavedException;
import com.exception.BiensImmobiliersCannotBeUpdatedException;
import com.exception.CanNotFindAllAdresseException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ProprietairesCannotBeFoundException;

public interface BiensImmobiliersService {
    
	/**
	 * @author Damien
	 * @param bienImmobilier
	 * @return bienImmobilier
	 * @throws BiensImmobiliersCannotBeSavedException
	 */
	public BiensImmobiliers saveBiensImmobiliers(BiensImmobiliers bienImmobilier) throws BiensImmobiliersCannotBeSavedException;
	/**
	 * @author Damien
	 * @param bienImmobilier
	 * @return bienImmobilier
	 * @throws BiensImmobiliersCannotBeUpdatedException
	 */
	public BiensImmobiliers updateBiensImmobiliers(BiensImmobiliers bienImmobilier) throws BiensImmobiliersCannotBeUpdatedException;
	/**
	 * 
	 * @param id
	 * @throws BiensImmobiliersCannotBeDeletetException
	 */
	public void deleteBiensImmobiliersById(Long id) throws BiensImmobiliersCannotBeDeletetException;
	
	/**
	 * @author Damien
	 * @param id
	 * @return bienImmobilier
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public BiensImmobiliers findBiensImmobiliersById(Long id) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliers() throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param max Cautionlocative
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMax(double max) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Cautionlocative
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMin(double min) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Cautionlocative
	 * @param2 max Cautionlocative
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByCautionlocativeMaxMin(double min, double max) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param max Loyermensuel
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMax(double max) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Loyermensuel
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMin(double min) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Loyermensuel
	 * @param2 max Loyermensuel
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByLoyermensuelMaxMin(double max, double min) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param max Prixlocation
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMax( double max) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Prixlocation
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMin(double min) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Prixlocation
	 * @param2 max Prixlocation
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixlocationMaxMin(double max, double min) throws BiensImmobiliersCannotBeFoundException;
	
	
	
	/**
	 * @author Damien
	 * @param max Prixachat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMax(double max) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Prixachat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMin(double min) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Prixachat
	 * @param2 max Prixachat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatMaxMin(double max, double min) throws BiensImmobiliersCannotBeFoundException;
	
	
	/**
	 * @author Damien
	 * @param max Prixachatdemande
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMax(double max) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Prixachatdemande
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMin(double min) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Prixachatdemande
	 * @param2 max Prixachatdemande
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByPrixachatdemandeMaxMin(double max, double min) throws BiensImmobiliersCannotBeFoundException;
	
	
	
	/**
	 * @author Damien
	 * @param max Revenucadastral
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMax( double max) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Revenucadastral
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMin(double min) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param min Revenucadastral
	 * @param2 max Revenucadastral
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByRevenucadastralMaxMin(double max, double min) throws BiensImmobiliersCannotBeFoundException;
	
	
	
   /**
    * @author Damien
    * @param typeBAil
    * @return List<BiensImmobiliers>
    * @throws BiensImmobiliersCannotBeFoundException
    */
	public List<BiensImmobiliers> findBiensImmobiliersByTypeBail(String type) throws BiensImmobiliersCannotBeFoundException;
	
	
	/**
	 * @author Damien
	 * @param garniture
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByGarniture(String garniture) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param etat
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByEtat(String etat) throws BiensImmobiliersCannotBeFoundException;
	
	/**
	 * @author Damien
	 * @param status
	 * @return List<BiensImmobiliers>
	 * @throws BiensImmobiliersCannotBeFoundException
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByStatus(String status) throws BiensImmobiliersCannotBeFoundException;
	
	
	
    /**
     * @author Damien
     * @param ProprietaireId
     * @return List<BiensImmobiliers>
     * @throws BiensImmobiliersCannotBeFoundException
     */
	public List<BiensImmobiliers> findBiensImmobiliersByProprietaireId( Long id) throws BiensImmobiliersCannotBeFoundException;
	 /**
     * @author Damien
     * @param ContratId
     * @return List<BiensImmobiliers>
     * @throws BiensImmobiliersCannotBeFoundException
     */
	public List<BiensImmobiliers> findBiensImmobiliersByContratId(  Long id) throws BiensImmobiliersCannotBeFoundException;
	 /**
     * @author Damien
     * @param AdresseId
     * @return List<BiensImmobiliers>
     * @throws BiensImmobiliersCannotBeFoundException
     */
	public List<BiensImmobiliers> findBiensImmobiliersByAdresseId( Long id) throws BiensImmobiliersCannotBeFoundException;
	
	
	/**
	 * @author Quentin
	 * @param modeOffre
	 * @return  List<BiensImmobiliers>
	 */
	public List<BiensImmobiliers> findBiensImmobiliersByTypeOffre(@Param("x") String modeOffre);
	
	public void assignContratToBienImmobilier(@Param("x") Long idContrat, @Param("y") Long idBienImmobilier) throws ContratCanNotBeFoundException, BiensImmobiliersCannotBeFoundException;
	
	public void changementStatusBienImmobilier(@Param("x") Long idContrat, @Param("y") String numeroTelephonePrivee) throws BiensImmobiliersCannotBeUpdatedException, ProprietairesCannotBeFoundException;

	public void changementStatusBienImmobilierLocation(@Param("x") Long idContrat) throws BiensImmobiliersCannotBeUpdatedException;

	
    /**
     * @author Adeline Scherb
     * @param ClasseStandardId
     * @return List<BiensImmobiliers>
     * @throws BiensImmobiliersCannotBeFoundException
     */
	public List<BiensImmobiliers> findBiensImmobiliersByClasseStandardId( Long id) throws BiensImmobiliersCannotBeFoundException;
	
	public BiensImmobiliers assignContratToBienImmobilierWithReturn(Long idContrat, Long idBienImmobilier) throws ContratCanNotBeFoundException, BiensImmobiliersCannotBeFoundException;

//	public void assignAdresseToBienImmobilier(@Param("x") Long idAdresse, @Param("y") Long idBienImmobilier) throws CanNotFindAllAdresseException, BiensImmobiliersCannotBeFoundException;

//	public BiensImmobiliers assignAdresseToBienImmobilierWithReturn(@Param("x") Long idAdresse, @Param("y") Long idBienImmobilier) throws CanNotFindAllAdresseException, BiensImmobiliersCannotBeFoundException;

	public List<BiensImmobiliers> findBiensImmobiliersByAllCriteriaOfClasseStandard(@Param("a") String typeOffre, @Param("b") String typeBien,
			@Param("c") int prixMax, @Param("d") int superficieMax, @Param("e") int prixMin,  @Param("f") int superficieMin, @Param("g") String ville);
	
	public BiensImmobiliers assignProprietaireToBienImmobilierWithReturn(Long idProprietaire, Long idBienImmobilier)
			throws ProprietairesCannotBeFoundException, BiensImmobiliersCannotBeFoundException;

	
	/**
	 * @author Charles
	 * @param status
	 * @return List<BiensImmobiliers> from Italie with the specific status param
	 */
	public List<BiensImmobiliers> findBiensImmobiliersItaliensByStatus(@Param("x") String status);
	/**
	 * @author Charles
	 * @return List<BiensImmobiliers> from Italie 
	 */
	public List<BiensImmobiliers> findBiensImmobiliersItalie();
	/**
	 * @author Charles
	 * @param id bien
	 * @return List<BiensImmobiliers> from Italie with the specific id bien param
	 */
	public Optional<BiensImmobiliers> findBienImmobilierItalieById(@Param("x") Long idBienImmobilier);
	
	
}
