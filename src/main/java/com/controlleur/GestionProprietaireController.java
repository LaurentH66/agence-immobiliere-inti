package com.controlleur;

import java.util.Comparator;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Adresse;
import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.entity.Proprietaires;
import com.entity.Visite;
import com.exception.BiensImmobiliersCannotBeDeletetException;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.CanNotDeleteAdresseByIdException;
import com.exception.CanNotSaveAdresseException;

import com.exception.ClientCannotBeSavedException;

import com.exception.CanNotUpdateAdresseException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeUpdatedException;
import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeDeletetException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
import com.exception.ProprietairesCannotBeUpdatedException;
import com.service.AdresseService;
import com.service.BiensImmobiliersService;
import com.service.ProprietairesService;
/**
 * 
 * @author Adeline Scherb
 *
 */
@Controller
@RequestMapping(value = "/gestionProprietaireController")
public class GestionProprietaireController {

	@Autowired
	ProprietairesService propService;
	
	@Autowired
	AdresseService adresseService;
	
	@Autowired
	BiensImmobiliersService bienService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView findAllProprietaires() {
		ModelAndView mv = new ModelAndView();
				try {
					mv.addObject("proprietaires", propService.findProprietaires()
							.stream().sorted(Comparator.comparing(Proprietaires::getNom)).collect(Collectors.toList()));
				} catch (ProprietairesCannotBeFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		mv.setViewName("gestionProprietaire.html");
		return mv;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addProprietaire(@ModelAttribute Adresse adresse,@ModelAttribute Proprietaires prop) throws ClientCannotBeSavedException, ProprietairesCannotBeSavedException, ClientCannotBeFoundException, ProprietairesCannotBeFoundException, ClientDoesntExistException {
			try {
				adresseService.saveAdresse(adresse);
			} catch (CanNotSaveAdresseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			prop.setAdresse(adresse);
			try {
				propService.saveProprietaires(prop);
			} catch (ProprietairesCannotBeSavedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("proprietaires", propService.findProprietaires().stream().sorted(Comparator.comparing(Proprietaires::getNom)).collect(Collectors.toList()));
			} catch (ProprietairesCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mv.setViewName("gestionProprietaire.html");
		return mv;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateProprietaire(@ModelAttribute Adresse adresse, @ModelAttribute Proprietaires prop) {
		prop.setAdresse(adresse);
		adresse.setIdAdresse(prop.getAdresse().getIdAdresse());
		try {
			adresseService.updateAdresse(adresse);
		} catch (CanNotUpdateAdresseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

			try {
				propService.updateProprietaires(prop);
			} catch (ProprietairesCannotBeUpdatedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		ModelAndView mv = new ModelAndView();
			
		try {
			mv.addObject("proprietaires", propService.findProprietaires().stream().sorted(Comparator.comparing(Proprietaires::getNom)).collect(Collectors.toList()));
		} catch (ProprietairesCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mv.setViewName("gestionProprietaire.html");
		return mv;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteProprietaireById(@RequestParam("idProprietaire") Long id) {
		try {
			if (bienService.findBiensImmobiliersByProprietaireId(id)!= null) {
				for (BiensImmobiliers bien : bienService.findBiensImmobiliersByProprietaireId(id)) {
					bienService.deleteBiensImmobiliersById(bien.getIdBienImmobilier());
				}
			}
		} catch (BiensImmobiliersCannotBeFoundException | BiensImmobiliersCannotBeDeletetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			
			try {
				propService.deleteProprietairesById(id);
			} catch (ProprietairesCannotBeDeletetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		ModelAndView mv = new ModelAndView();

			try {
				mv.addObject("proprietaires", propService.findProprietaires().stream().sorted(Comparator.comparing(Proprietaires::getNom)).collect(Collectors.toList()));
			} catch (ProprietairesCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("gestionProprietaire.html");
		return mv;
	}
	
	@RequestMapping(value = "/getproprietaire", method = RequestMethod.GET)
	public ModelAndView findProprietaireById(@RequestParam("idProprietaire") Long id) {
		ModelAndView mv = new ModelAndView();

			try {
				mv.addObject("proprietaire", propService.findProprietairesById(id));
				mv.addObject("adresse", propService.findProprietairesById(id).getAdresse());
			} catch (ProprietairesCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("gestionProprietaireUpdate.html");
		return mv;
	}
	
	@RequestMapping(value = "/getproprietairebynom", method = RequestMethod.GET)
	public ModelAndView findProprietaireByNom(@RequestParam("nom") String nom) {
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("proprietaires", propService.findProprietaireByNom(nom).stream().sorted(Comparator.comparing(Proprietaires::getNom)).collect(Collectors.toList()));
			} catch (ProprietairesCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("gestionProprietaire.html");
		return mv;
	}
	
	@RequestMapping(value = "/afficherBiens", method = RequestMethod.GET)
	public ModelAndView findBiensWithIdProprietaire(@RequestParam("idProprietaire") Long idProprietaire) {
		ModelAndView mv = new ModelAndView();

				try {
					mv.addObject("biens", bienService.findBiensImmobiliersByProprietaireId(idProprietaire));
					mv.addObject("proprietaire", propService.findProprietairesById(idProprietaire));
				} catch (BiensImmobiliersCannotBeFoundException | ProprietairesCannotBeFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


		mv.setViewName("afficherBienDUnProprietaire.html");
		return mv;
	}
}
