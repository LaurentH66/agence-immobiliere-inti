package com.controlleur;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Adresse;
import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.entity.Contrat;
import com.entity.Proprietaires;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.BiensImmobiliersCannotBeSavedException;
import com.exception.CanNotSaveAdresseException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;
import com.exception.ClientDoesntExistException;
import com.exception.ContratCanNotBeDeletedException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ContratCanNotBeUpdatedException;
import com.exception.ContratsCanNotBeFoundException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
import com.repository.ContratRepository;
import com.service.AdresseService;
import com.service.BiensImmobiliersService;
import com.service.ClientService;
import com.service.ContratService;
import com.service.ProprietairesService;

/**
 * 
 * @author Laurent Hurtado et Charles Tupin
 *
 */
@Controller
@RequestMapping(value = "/gestionContratController")
public class GestionContratController {

	@Autowired
	ContratService contratService;

	@Autowired
	ClientService clientService;

	@Autowired
	ProprietairesService proprietaireService;

	@Autowired
	BiensImmobiliersService bienService;

	@Autowired
	AdresseService adresseService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView findAllContrats() throws BiensImmobiliersCannotBeFoundException {
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("contrats", contratService.findContrats());
			mv.addObject("biens", bienService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
		} catch (ContratsCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mv.setViewName("gestionContrat.html");
		return mv;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addContrat(@ModelAttribute Adresse adresse, @ModelAttribute Client client,
			@ModelAttribute Contrat contrat, @RequestParam Long savedBiens)
			throws ContratCanNotBeFoundException, ProprietairesCannotBeSavedException, ClientCannotBeFoundException,
			ClientDoesntExistException, ClientCannotBeSavedException, ProprietairesCannotBeFoundException, BiensImmobiliersCannotBeFoundException {

		BiensImmobiliers biens = bienService.findBiensImmobiliersById(savedBiens);
		
		
		
		try {
			adresseService.saveAdresse(adresse);
		} catch (CanNotSaveAdresseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		client.setAdresse(adresse);

		Proprietaires proprietaire = Proprietaires.builder().nom(client.getNom())
				.numeroTelephonePrivee(client.getNumeroTelephone()).adresse(client.getAdresse()).build();

		try {
		if (biens.getStatus().equalsIgnoreCase("A vendre")) {

		proprietaire = proprietaireService.saveProprietaires(proprietaire);
		}
		} catch (Exception e) {
			e.printStackTrace();
		}

		 if (!clientService.findClientByNumeroTelephone(client.getNumeroTelephone()).isPresent()){
			clientService.saveClient(client);
		}

		contrat.setClient(client);

		try {
			contrat =contratService.saveContrat(contrat);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		try {
//			bienService.assignAdresseToBienImmobilier(proprietaire2.getAdresse().getIdAdresse(), bien.getIdBienImmobilier());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		try {
			bienService.assignContratToBienImmobilierWithReturn(contrat.getIdContrat(), biens.getIdBienImmobilier());
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		
		

			try {
				if (biens.getStatus().equalsIgnoreCase("A vendre")) 
				bienService.changementStatusBienImmobilier(contrat.getIdContrat(),
						proprietaire.getNumeroTelephonePrivee());
			} catch (Exception e) {
				e.printStackTrace();
			}
		

	

			try {
				if (biens.getStatus().equalsIgnoreCase("A louer"))
				bienService.changementStatusBienImmobilierLocation(contrat.getIdContrat());
			} catch (Exception e) {
				e.printStackTrace();
			}
		

		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("contrats", contratService.findContrats());
			mv.addObject("biens", bienService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
		} catch (ContratsCanNotBeFoundException e) {
			e.printStackTrace();
		}

		mv.setViewName("gestionContrat.html");
		return mv;

	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateContrat(@ModelAttribute Client client, @ModelAttribute Contrat contrat) {
		contrat.setClient(client);
		client.setIdClient(contrat.getClient().getIdClient());
		try {
			clientService.updateClient(client);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			contratService.updateContrat(contrat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("contrats", contratService.findContrats());
		} catch (ContratsCanNotBeFoundException e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionContrat.html");
		return mv;

	}

	@RequestMapping(value = "/getcontrat", method = RequestMethod.GET)
	public ModelAndView findContratById(@RequestParam("idContrat") Long id) {

		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("contrats", contratService.findContratById(id));
			mv.addObject("client", contratService.findContratById(id).getClient());
			mv.addObject("biens", bienService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
		} catch (ContratCanNotBeFoundException | BiensImmobiliersCannotBeFoundException e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionContratUpdate.html");
		return mv;

	}

	@RequestMapping(value = "/deletecontrat", method = RequestMethod.GET)
	public ModelAndView deleteContratById(@RequestParam("idContrat") Long id) {

		try {
			contratService.deleteContratById(id);
		} catch (ContratCanNotBeDeletedException e) {
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("contrats", contratService.findContrats());
			mv.addObject("biens", bienService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
		} catch (ContratsCanNotBeFoundException | BiensImmobiliersCannotBeFoundException e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionContrat.html");
		return mv;

	}

	@RequestMapping(value = "/getcontratbyref", method = RequestMethod.GET)
	public ModelAndView findContratByRef(@RequestParam("referenceContrat") String referenceContrat) {

		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("contrats", contratService.findContratByReferenceContrat(referenceContrat));
			mv.addObject("biens", bienService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
		} catch (ContratCanNotBeFoundException | BiensImmobiliersCannotBeFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		mv.setViewName("gestionContrat.html");
		return mv;

	}

}
