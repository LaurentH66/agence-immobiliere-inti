package com.controlleur;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.management.relation.RoleNotFoundException;
import javax.swing.text.Document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Adresse;
import com.entity.Client;
import com.entity.ConseillersImmobiliers;
import com.entity.Role;
import com.entity.Visite;
import com.exception.CanNotDeleteConseillersImmobiliersByIdException;
import com.exception.CanNotFindAllConseillersImmobiliersException;
import com.exception.CanNotFindConseillersImmobiliersException;
import com.exception.CanNotSaveConseillersImmobiliersException;
import com.exception.CanNotUpdateAdresseException;
import com.exception.CanNotUpdateConseillersImmobiliersException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeUpdatedException;
import com.exception.RoleCannotBeFoundException;
import com.exception.VisitesCanNotBeFoundException;
import com.service.ConseillersImmobiliersService;
import com.service.RoleService;
import com.service.VisiteService;
/**
 * 
 * @author Adeline Scherb
 *
 */
@Controller
@RequestMapping(value = "/gestionConseillerController")
public class GestionConseilllerController {

	@Autowired
	ConseillersImmobiliersService conseillerService;
	
	@Autowired
	VisiteService visiteService;
	
	@Autowired
	RoleService roleService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView findAllConseillers() {
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("conseillers", conseillerService.findAllConseillersImmobilierss().stream().sorted(Comparator.comparing(ConseillersImmobiliers::getNom)).collect(Collectors.toList()));
			} catch (CanNotFindAllConseillersImmobiliersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("gestionConseiller.html");
		return mv;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addConseiller(@ModelAttribute ConseillersImmobiliers conseiller, @RequestParam String selectRole) throws RoleCannotBeFoundException   {
		List<Role> roles=new ArrayList<Role>();
		if (selectRole.equals("admin")) roles.add(roleService.findRoleById(1l));
		if (selectRole.equals("conseiller")) roles.add(roleService.findRoleById(2l)); 
		if (selectRole.equals("admin,conseiller")) {
			roles.add(roleService.findRoleById(1l));
			roles.add(roleService.findRoleById(2l)); 
		}
		conseiller.setRoles(roles);
			try {
				conseillerService.saveConseillersImmobiliers(conseiller);
			} catch (CanNotSaveConseillersImmobiliersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("conseillers", conseillerService.findAllConseillersImmobilierss().stream().sorted(Comparator.comparing(ConseillersImmobiliers::getNom)).collect(Collectors.toList()));
			} catch (CanNotFindAllConseillersImmobiliersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mv.setViewName("gestionConseiller.html");
		return mv;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateConseiller(@ModelAttribute ConseillersImmobiliers conseiller) {


			try {
				conseillerService.updateConseillersImmobiliers(conseiller);
			} catch (CanNotUpdateConseillersImmobiliersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		ModelAndView mv = new ModelAndView();

			try {
				mv.addObject("clients", conseillerService.findAllConseillersImmobilierss().stream().sorted(Comparator.comparing(ConseillersImmobiliers::getNom)).collect(Collectors.toList()));
			} catch (CanNotFindAllConseillersImmobiliersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("gestionConseiller.html");
		return mv;
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteConseillerById(@RequestParam("idConseiller") Long id) {
		ModelAndView mv = new ModelAndView();
		try {
			if (visiteService.findVisiteIdConseiller(id).isEmpty()) {
				conseillerService.deleteConseillersImmobiliersById(id);
				mv.addObject("conseillers", conseillerService.findAllConseillersImmobilierss().stream().sorted(Comparator.comparing(ConseillersImmobiliers::getNom)).collect(Collectors.toList()));
				mv.setViewName("gestionConseiller.html");
				
			}
			
			else {
				mv.addObject("conseillers", conseillerService.findAllConseillersImmobilierss().stream().sorted(Comparator.comparing(ConseillersImmobiliers::getNom)).collect(Collectors.toList()));
				mv.setViewName("erreurConseiller.html");
			}
		} catch (VisitesCanNotBeFoundException | CanNotDeleteConseillersImmobiliersByIdException
				| CanNotFindAllConseillersImmobiliersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mv;
	}
	
	@RequestMapping(value = "/getconseiller", method = RequestMethod.GET)
	public ModelAndView findConseillerById(@RequestParam("idConseiller") Long id) {
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("conseiller", conseillerService.findConseillersImmobiliersById(id));
			} catch (CanNotFindConseillersImmobiliersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mv.setViewName("gestionConseillerUpdate.html");
		return mv;
	}
	
	@RequestMapping(value = "/getconseillerbynom", method = RequestMethod.GET)
	public ModelAndView findConseillerByNom(@RequestParam("nom") String nom) {
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("conseillers", conseillerService.findConseillersImmobiliersByNom(nom).stream().sorted(Comparator.comparing(ConseillersImmobiliers::getNom)).collect(Collectors.toList()));
			} catch (CanNotFindAllConseillersImmobiliersException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mv.setViewName("gestionConseiller.html");
		return mv;
	}
}
