package com.controlleur;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Adresse;
import com.entity.ClasseStandard;
import com.entity.Client;
import com.entity.Visite;
import com.exception.CanNotDeleteAdresseByIdException;
import com.exception.CanNotFindAdresseByIdException;
import com.exception.CanNotSaveAdresseException;
import com.exception.CanNotUpdateAdresseException;
import com.exception.ClasseStandardCannotBeFoundException;
import com.exception.ClientCannotBeDeletetException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;

import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeFoundException;

import com.exception.ClientCannotBeUpdatedException;

import com.exception.VisiteCanNotBeDeletedException;
import com.exception.VisitesCanNotBeFoundException;
import com.service.AdresseService;
import com.service.ClasseStandardService;
import com.service.ClientService;
import com.service.ProprietairesService;
import com.service.VisiteService;
/**
 * 
 * @author Adeline Scherb
 *
 */
@Controller
@RequestMapping(value = "/gestionClientController")
public class GestionClientController {

	@Autowired
	ClientService clientService;

	@Autowired
	VisiteService visiteService;

	@Autowired
	AdresseService adresseService;
	
	@Autowired
	ClasseStandardService classeService;

	@Autowired
	ProprietairesService proprietaireService;


	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView findAllClients() {
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("clients", clientService.findClients().stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			
			//Tri affichage classe Standard
			List<ClasseStandard> classestriees = classeService.findClasseStandards().stream()
					.sorted(Comparator.comparingInt(ClasseStandard::getSuperficieMaximale)
							.thenComparingDouble(ClasseStandard::getPrixMaximum))
					.collect(Collectors.toList());			
			classestriees =classestriees.stream()
					.sorted((x,y)->x.getTypeBienImmobilierId().getTypeBien().compareTo(y.getTypeBienImmobilierId().getTypeBien()))
					.collect(Collectors.toList());			
			classestriees = classestriees.stream()
					.sorted((x,y)->x.getTypeOffreId().getModeOffre().compareTo(y.getTypeOffreId().getModeOffre()))
					.collect(Collectors.toList());
			mv.addObject("classes", classestriees);
		} catch (ClientCannotBeFoundException | ClasseStandardCannotBeFoundException  e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionClient.html");
		return mv;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addClient(@ModelAttribute Adresse adresse, @ModelAttribute Client client, @RequestParam List<Long> idClasseStandard) {
		List<ClasseStandard> classes=new ArrayList<ClasseStandard>();
		for (Long idClasse:idClasseStandard) {
			try {
				classes.add(classeService.findClasseStandardById(idClasse));
			} catch (ClasseStandardCannotBeFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}

		client.setClassesStandards(classes);
		
		try {
			adresseService.saveAdresse(adresse);
		} catch (CanNotSaveAdresseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		client.setAdresse(adresse);

		try {
			clientService.saveClient(client);
		} catch (ClientCannotBeSavedException e) {
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView();
		try {
			//Tri affichage classe Standard
			List<ClasseStandard> classestriees = classeService.findClasseStandards().stream()
					.sorted(Comparator.comparingInt(ClasseStandard::getSuperficieMaximale)
							.thenComparingDouble(ClasseStandard::getPrixMaximum))
					.collect(Collectors.toList());			
			classestriees =classestriees.stream()
					.sorted((x,y)->x.getTypeBienImmobilierId().getTypeBien().compareTo(y.getTypeBienImmobilierId().getTypeBien()))
					.collect(Collectors.toList());			
			classestriees = classestriees.stream()
					.sorted((x,y)->x.getTypeOffreId().getModeOffre().compareTo(y.getTypeOffreId().getModeOffre()))
					.collect(Collectors.toList());
			mv.addObject("classes", classestriees);
			mv.addObject("clients", clientService.findClients().stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
		} catch (ClientCannotBeFoundException | ClasseStandardCannotBeFoundException e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionClient.html");
		return mv;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateClient(@ModelAttribute Adresse adresse, @ModelAttribute Client client, @RequestParam List<Long> idClasseStandard) {
		List<ClasseStandard> classes=new ArrayList<ClasseStandard>();
		for (Long idClasse:idClasseStandard) {
			try {
				classes.add(classeService.findClasseStandardById(idClasse));
			} catch (ClasseStandardCannotBeFoundException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}

		client.setClassesStandards(classes);
		client.setAdresse(adresse);
		adresse.setIdAdresse(client.getAdresse().getIdAdresse());
		try {
			adresseService.updateAdresse(adresse);
		} catch (CanNotUpdateAdresseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try { 
			clientService.updateClient(client);
		} catch (ClientCannotBeUpdatedException e) {
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("clients", clientService.findClients().stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			//Tri affichage classe Standard
			List<ClasseStandard> classestriees = classeService.findClasseStandards().stream()
					.sorted(Comparator.comparingInt(ClasseStandard::getSuperficieMaximale)
							.thenComparingDouble(ClasseStandard::getPrixMaximum))
					.collect(Collectors.toList());			
			classestriees =classestriees.stream()
					.sorted((x,y)->x.getTypeBienImmobilierId().getTypeBien().compareTo(y.getTypeBienImmobilierId().getTypeBien()))
					.collect(Collectors.toList());			
			classestriees = classestriees.stream()
					.sorted((x,y)->x.getTypeOffreId().getModeOffre().compareTo(y.getTypeOffreId().getModeOffre()))
					.collect(Collectors.toList());
			mv.addObject("classes", classestriees);
		} catch (ClientCannotBeFoundException | ClasseStandardCannotBeFoundException e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionClient.html");
		return mv;
	}

	@RequestMapping(value = "/getclient", method = RequestMethod.GET)
	public ModelAndView findClientById(@RequestParam("idClient") Long id) {
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("client", clientService.findClientById(id));
			mv.addObject("adresse", clientService.findClientById(id).getAdresse());
			mv.addObject("classes", classeService.findClasseStandards());
		} catch (ClientCannotBeFoundException | ClasseStandardCannotBeFoundException e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionClientUpdate.html");
		return mv;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteClientById(@RequestParam("idClient") Long id) {
		try {
			if (visiteService.findVisiteIdClient(id) != null) {
				for (Visite vis : visiteService.findVisiteIdClient(id)) {
					visiteService.deleteVisite(vis);
				}
			}
		} catch (VisiteCanNotBeDeletedException | VisitesCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			clientService.deleteClientById(id);
		} catch (ClientCannotBeDeletetException e) {
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("clients", clientService.findClients().stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			//Tri affichage classe Standard
			List<ClasseStandard> classestriees = classeService.findClasseStandards().stream()
					.sorted(Comparator.comparingInt(ClasseStandard::getSuperficieMaximale)
							.thenComparingDouble(ClasseStandard::getPrixMaximum))
					.collect(Collectors.toList());			
			classestriees =classestriees.stream()
					.sorted((x,y)->x.getTypeBienImmobilierId().getTypeBien().compareTo(y.getTypeBienImmobilierId().getTypeBien()))
					.collect(Collectors.toList());			
			classestriees = classestriees.stream()
					.sorted((x,y)->x.getTypeOffreId().getModeOffre().compareTo(y.getTypeOffreId().getModeOffre()))
					.collect(Collectors.toList());
			mv.addObject("classes", classestriees);
		} catch (ClientCannotBeFoundException | ClasseStandardCannotBeFoundException e) {
			e.printStackTrace();
		}
		mv.setViewName("gestionClient.html");
		return mv;
	}

	@RequestMapping(value = "/getclientbynom", method = RequestMethod.GET)
	public ModelAndView findclientbyname(@RequestParam("nom") String nom) {
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("clients", clientService.findClientByNom(nom).stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			//Tri affichage classe Standard
			List<ClasseStandard> classestriees = classeService.findClasseStandards().stream()
					.sorted(Comparator.comparingInt(ClasseStandard::getSuperficieMaximale)
							.thenComparingDouble(ClasseStandard::getPrixMaximum))
					.collect(Collectors.toList());			
			classestriees =classestriees.stream()
					.sorted((x,y)->x.getTypeBienImmobilierId().getTypeBien().compareTo(y.getTypeBienImmobilierId().getTypeBien()))
					.collect(Collectors.toList());			
			classestriees = classestriees.stream()
					.sorted((x,y)->x.getTypeOffreId().getModeOffre().compareTo(y.getTypeOffreId().getModeOffre()))
					.collect(Collectors.toList());
			mv.addObject("classes", classestriees);
		} catch (ClientCannotBeFoundException | ClasseStandardCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mv.setViewName("gestionClient.html");
		return mv;
	}
	


}
