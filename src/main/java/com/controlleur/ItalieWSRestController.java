package com.controlleur;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Adresse;
import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.entity.Contrat;
import com.entity.Proprietaires;
import com.exception.BiensImmobiliersCannotBeDeletetException;
import com.exception.BiensImmobiliersCannotBeSavedException;
import com.exception.BiensImmobiliersCannotBeUpdatedException;
import com.exception.ClientCannotBeDeletetException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;
import com.exception.ClientCannotBeUpdatedException;
import com.exception.ClientDoesntExistException;
import com.exception.ContratCanNotBeAddedException;
import com.exception.ContratCanNotBeDeletedException;
import com.exception.ContratCanNotBeUpdatedException;
import com.exception.ContratsCanNotBeFoundException;
import com.exception.ProprietairesCannotBeDeletetException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
import com.exception.ProprietairesCannotBeUpdatedException;
import com.service.AdresseService;
import com.service.BiensImmobiliersService;
import com.service.ClientService;
import com.service.ContratService;
import com.service.ProprietairesService;

/**
 * 
 * @author Charles
 *
 */
@RestController
@RequestMapping(value = "/italieController")
public class ItalieWSRestController {

	@Autowired
	ProprietairesService propService;

	@Autowired
	AdresseService adresseService;

	@Autowired
	BiensImmobiliersService bienService;

	@Autowired
	ClientService clientService;

	@Autowired
	ContratService contratService;

	/** listes de biens **/

	@GetMapping(value = "/biensdispo")
	public List<BiensImmobiliers> findBiensDispo() {
		return bienService.findBiensImmobiliersItaliensByStatus("dispo");
	}

	@GetMapping(value = "/biensvendus")
	public List<BiensImmobiliers> findBiensVendus() {
		return bienService.findBiensImmobiliersItaliensByStatus("vendu");
	}

	@GetMapping(value = "/biensloués")
	public List<BiensImmobiliers> findBiensLoués() {
		return bienService.findBiensImmobiliersItaliensByStatus("loués");
	}

	@GetMapping(value = "/biensAvendre")
	public List<BiensImmobiliers> findBiensAVendre() {
		return bienService.findBiensImmobiliersItaliensByStatus("a vendre");
	}

	@GetMapping(value = "/biensAlouer")
	public List<BiensImmobiliers> findBiensALouer() {
		return bienService.findBiensImmobiliersItaliensByStatus("a louer");
	}

	/** listes de clients **/

	@GetMapping(value = "/clientsByClasseStandard/{ref}")
	public List<Client> findClientByClasseStandard(@PathVariable("ref") Long idClasseStandard) {
		return clientService.findClientByClasseStandard(idClasseStandard);
	}

	@GetMapping(value = "/clientsByClasseStandardBienImmobilier/{ref}")
	public List<Client> findClientByClasseStandardBienImmobilier(@PathVariable("ref") Long idBienImmobilier) {
		return clientService.findClientByClasseStandardBienImmobilier(idBienImmobilier);
	}
	
	@GetMapping(value = "/locatairesInItalie")
	public List<Client> findLocatairesInItalie() {
		return clientService.findLocataireInItaly();
	}

	/** proprio **/

	@GetMapping(value = "/proprietairesItalie")
	public List<Proprietaires> findProprietairesItalie() {
		return propService.findProprietairesItalie();
	}

	@GetMapping(value = "/proprietaireItalieById/{ref}")
	public Optional<Proprietaires> findProprietaireItalieById(@PathVariable("ref") Long idProprietaireItalie)
			throws ProprietairesCannotBeFoundException {
		return propService.findProprietaireItalieById(idProprietaireItalie);
	}

	@DeleteMapping(value = "/proprietaireItalie")
	public void deleteProprietairesItalieById(@RequestParam("id") Long idProprietaireItalie)
			throws ProprietairesCannotBeDeletetException, ProprietairesCannotBeFoundException {

		if (propService.findProprietaireItalieById(idProprietaireItalie).isPresent()) {

			propService.deleteProprietairesById(idProprietaireItalie);
		}
	}

	@PostMapping(value = "/proprietaireItalie")
	public void saveProprietaireItalie(@RequestBody Proprietaires propritaireItalie)
			throws ProprietairesCannotBeSavedException, ClientCannotBeSavedException, ClientCannotBeFoundException,
			ProprietairesCannotBeFoundException, ClientDoesntExistException {

		propService.saveProprietaires(propritaireItalie);

	}

	@PutMapping(value = "/proprietaireItalie")
	public void updateProprietairesItalie(@RequestBody Proprietaires propritaireItalie)
			throws ProprietairesCannotBeUpdatedException, ProprietairesCannotBeFoundException {

		if (propService.findProprietaireItalieById(propritaireItalie.getIdProprietaire()).isPresent()) {

			propService.updateProprietaires(propritaireItalie);
		}
	}

	/** biensImmo **/

	@GetMapping(value = "/biensImmobiliersItalie")
	public List<BiensImmobiliers> biensImmobiliersItalie() {
		return bienService.findBiensImmobiliersItalie();
	}

	@GetMapping(value = "/bienImmobilierItalieById/{ref}")
	public Optional<BiensImmobiliers> bienImmobilierItalieById(@PathVariable("ref") Long idBienImmobilier) {
		return bienService.findBienImmobilierItalieById(idBienImmobilier);
	}

	@DeleteMapping(value = "/bienImmobilierItalie")
	public void deleteBienImmobilierItalieById(@RequestParam("id") Long idBienImmobilier)
			throws BiensImmobiliersCannotBeDeletetException {

		if (bienService.findBienImmobilierItalieById(idBienImmobilier).isPresent()) {

			bienService.deleteBiensImmobiliersById(idBienImmobilier);
		}
	}

	@PostMapping(value = "/bienImmobilierItalie")
	public void saveBienImmobilierItalie(@RequestBody BiensImmobiliers bienImmo)
			throws BiensImmobiliersCannotBeSavedException {

		Adresse adresse = bienImmo.getAdresse();
		adresse.setPays("Italie");

		bienImmo.setAdresse(adresse);

		bienService.saveBiensImmobiliers(bienImmo);

	}

	@PutMapping(value = "/biensImmobiliersItalie")
	public void updateBiensImmobiliersItalie(@RequestBody BiensImmobiliers bienImmo)
			throws BiensImmobiliersCannotBeUpdatedException {

		if (bienService.findBienImmobilierItalieById(bienImmo.getIdBienImmobilier()).isPresent()) {
			Adresse adresse = bienImmo.getAdresse();
			adresse.setPays("Italie");
			bienImmo.setAdresse(adresse);

			bienService.updateBiensImmobiliers(bienImmo);
		}
	}

	/**
	 * clients Remarque : On peut choisir de laisse ou d'enlever les fonctions
	 * delete et update si on veut restreindre la filiale italienne
	 **/

	@GetMapping(value = "/clients")
	public List<Client> clients() throws ClientCannotBeFoundException {
		return clientService.findClients();
	}

	@GetMapping(value = "/client/{ref}")
	public Client clientById(@PathVariable("ref") Long idClient) throws ClientCannotBeFoundException {
		return clientService.findClientById(idClient);
	}

	@DeleteMapping(value = "/client")
	public void deleteClientById(@RequestParam("id") Long idClient) throws ClientCannotBeDeletetException {

		clientService.deleteClientById(idClient);
	}

	@PostMapping(value = "/client")
	public void saveClient(@RequestBody Client client) throws ClientCannotBeSavedException {

		clientService.saveClient(client);

	}

	@PutMapping(value = "/client")
	public void updateClient(@RequestBody Client client) throws ClientCannotBeUpdatedException {

		clientService.updateClient(client);
	}

	/**
	 * Contrat
	 * 
	 * @throws ContratsCanNotBeFoundException
	 **/

	@GetMapping(value = "/contratsItalie")
	public List<Contrat> contratsItalie() {
		return contratService.findContratsItalie();
	}

	@GetMapping(value = "/contratItalie/{ref}")
	public Optional<Contrat> contratItalieById(@PathVariable("ref") Long idContrat) {
		return contratService.findContratsItalieById(idContrat);
	}

	@DeleteMapping(value = "/contratItalie")
	public void deleteContratById(@RequestParam("id") Long idContrat) throws ContratCanNotBeDeletedException {

		if (contratService.findContratsItalieById(idContrat).isPresent()) {

			contratService.deleteContratById(idContrat);
		}
	}

	@PostMapping(value = "/contratItalie")
	public void saveContrat(@RequestBody Contrat contrat) throws ContratCanNotBeAddedException {

		contratService.saveContrat(contrat);

	}

	@PutMapping(value = "/contratItalie")
	public void updateContrat(@RequestBody Contrat contrat) throws ContratCanNotBeUpdatedException {

		if (contratService.findContratsItalieById(contrat.getIdContrat()).isPresent()) {

			contratService.updateContrat(contrat);
		}
	}

}
