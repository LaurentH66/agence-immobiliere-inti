package com.controlleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Visite;
import com.exception.VisitesCanNotBeFoundException;
import com.service.VisiteService;
/**
 * 
 * @author Damien
 *
 */
@RestController
@RequestMapping(value="/VisiteWebService")
public class VisiteWebServController {

	@Autowired
	VisiteService visiteService;
	@RequestMapping(value="/events", method=RequestMethod.GET)
	public List<Visite> events() throws VisitesCanNotBeFoundException {
		return visiteService.findVisites();
	}
}
