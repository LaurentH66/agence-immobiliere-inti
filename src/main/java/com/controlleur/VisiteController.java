package com.controlleur;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.entity.BiensImmobiliers;
import com.entity.ClasseStandard;
import com.entity.Client;
import com.entity.ConseillersImmobiliers;
import com.entity.Visite;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.CanNotFindAllConseillersImmobiliersException;
import com.exception.CanNotFindConseillersImmobiliersException;
import com.exception.ClientCannotBeFoundException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.VisiteCanNotBeDeletedException;
import com.exception.VisiteCanNotBeFoundException;
import com.exception.VisiteCanNotBeUpdatedException;
import com.exception.VisiteCannotBeSavedException;
import com.exception.VisiteIDsAlreadyOccupiedAtThisMomentException;
import com.exception.VisitesCanNotBeFoundException;
import com.service.BiensImmobiliersService;
import com.service.ClientService;
import com.service.ConseillersImmobiliersService;
import com.service.VisiteService;

import lombok.extern.java.Log;

/**
 * 
 * @author Damien
 *
 */
@Controller
@RequestMapping(value = "/visiteController")
public class VisiteController {
	
	@Autowired
	VisiteService visiteService;
	
	@Autowired
	ConseillersImmobiliersService conseillersImmobiliersService;
	

	@Autowired
	ClientService clientService;
	
	@Autowired
	BiensImmobiliersService biensImmobiliersService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView findAllVisits() throws BiensImmobiliersCannotBeFoundException, CanNotFindAllConseillersImmobiliersException, ClientCannotBeFoundException {
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("visites", visiteService.findVisites().stream().sorted(Comparator.comparing(Visite::getDateVisite)
					.thenComparing(Visite::getHeureVisite)).collect(Collectors.toList()));
			mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
					.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
			mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
			mv.addObject("clients", clientService.findClients()
					.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
		} catch (VisitesCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			e.getMessage();
		}
	 catch (CanNotFindAllConseillersImmobiliersException  e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
		mv.setViewName("visite.html");
		return mv;
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelAndView addVisite(@ModelAttribute Visite visite, @RequestParam Long savedConseillers, @RequestParam Long savedBiens, @RequestParam Long savedClients) throws CanNotFindAllConseillersImmobiliersException, CanNotFindConseillersImmobiliersException, BiensImmobiliersCannotBeFoundException, ClientCannotBeFoundException {
		ModelAndView mv = new ModelAndView();
		BiensImmobiliers biensImmobiliers = biensImmobiliersService.findBiensImmobiliersById(savedBiens);
		ConseillersImmobiliers conseillersImmobiliers = conseillersImmobiliersService.findConseillersImmobiliersById(savedConseillers);
		Client client= clientService.findClientById(savedClients);
		
		try {
			visite.setBiensImmobiliers(biensImmobiliers);
			visite.setConseillerImmobilier(conseillersImmobiliers);
			visite.setClient(client);
			visite.setStart(LocalDateTime.parse(visite.getDateVisite()+" "+visite.getHeureVisite(), DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm")));
		    visite.setTitle("  "+visite.getClient().getNom()+"   "+visite.getConseillerImmobilier().getLogin());
			
		    switch(Math.toIntExact(visite.getConseillerImmobilier().getIdConseillersImmobiliers())) {
		    case 1:
		    	visite.setColor("blue");
		    	break;
		    case 2:
		    	visite.setColor("darkGray");
		    	break;
		    case 3:
		    	visite.setColor("green");
		    	break;
		    case 4:
		    	visite.setColor("magenta");
		    	break;
		    case 5:
		    	visite.setColor("orange");
		    	break;
		    case 6:
		    	visite.setColor("gray");
		    	break;
		    case 7:
		    	visite.setColor("cyan");
		    	break;
		    	
			}
			visiteService.saveVisite(visite);
		} catch (VisiteCannotBeSavedException | VisiteIDsAlreadyOccupiedAtThisMomentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			mv.setViewName("erreurVisite.html");
			return mv;
		}
		
		try {
			mv.addObject("visites", visiteService.findVisites().stream().sorted(Comparator.comparing(Visite::getDateVisite)
					.thenComparing(Visite::getHeureVisite)).collect(Collectors.toList()));
			mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
					.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
			mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
			mv.addObject("clients", clientService.findClients()
					.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
		} catch (VisitesCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch ( CanNotFindAllConseillersImmobiliersException e2) {
			// TODO Auto-generated catch block
			
			e2.printStackTrace();
		}
		
		
	
		mv.setViewName("visite.html");
		return mv;
		}
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView updateVisite(@ModelAttribute Visite visite, @RequestParam Long savedConseillers, @RequestParam Long savedBiens, @RequestParam Long savedClients) throws CanNotFindAllConseillersImmobiliersException, CanNotFindConseillersImmobiliersException, BiensImmobiliersCannotBeFoundException, ClientCannotBeFoundException, VisiteCanNotBeUpdatedException {
		
		BiensImmobiliers biensImmobiliers = biensImmobiliersService.findBiensImmobiliersById(savedBiens);
		ConseillersImmobiliers conseillersImmobiliers = conseillersImmobiliersService.findConseillersImmobiliersById(savedConseillers);
		Client client= clientService.findClientById(savedClients);
		
		try {
			visite.setBiensImmobiliers(biensImmobiliers);
			visite.setConseillerImmobilier(conseillersImmobiliers);
			visite.setClient(client);
			visite.setStart(LocalDateTime.parse(visite.getDateVisite()+" "+visite.getHeureVisite(), DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm")));
		    visite.setTitle("  "+visite.getClient().getNom()+"   "+visite.getConseillerImmobilier().getLogin());
			
			visiteService.updateVisite(visite);
		} catch (VisiteCanNotBeUpdatedException | VisiteIDsAlreadyOccupiedAtThisMomentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("visites", visiteService.findVisites().stream().sorted(Comparator.comparing(Visite::getDateVisite)
					.thenComparing(Visite::getHeureVisite)).collect(Collectors.toList()));
			mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
					.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
			mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
			mv.addObject("clients", clientService.findClients()
					.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
		} catch (VisitesCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch ( CanNotFindAllConseillersImmobiliersException e2) {
			// TODO Auto-generated catch block
			
			e2.printStackTrace();
		}
		mv.setViewName("visite.html");
		return mv;
	}
	
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteProprietaireById(@RequestParam("idVisite") Long id) throws CanNotFindAllConseillersImmobiliersException, BiensImmobiliersCannotBeFoundException, ClientCannotBeFoundException {

		try {
			visiteService.deleteVisiteById(id);
		} catch (VisiteCanNotBeDeletedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		ModelAndView mv = new ModelAndView();

		try {
			mv.addObject("visites", visiteService.findVisites().stream().sorted(Comparator.comparing(Visite::getDateVisite)
					.thenComparing(Visite::getHeureVisite)).collect(Collectors.toList()));
			mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
					.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
			mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
					.collect(Collectors.toList()));
			mv.addObject("clients", clientService.findClients()
					.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
		} catch (VisitesCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mv.setViewName("visite.html");
		return mv;
	}
	
	@RequestMapping(value = "/getvisite", method = RequestMethod.GET)
	public ModelAndView findVisiteById(@RequestParam("idVisite") Long id) throws VisiteCanNotBeFoundException, CanNotFindAllConseillersImmobiliersException, BiensImmobiliersCannotBeFoundException, ClientCannotBeFoundException {
		ModelAndView mv = new ModelAndView();

			try {
				mv.addObject("visite", visiteService.findVisiteById(id));
				mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
						.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
				mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
						.collect(Collectors.toList()));
				mv.addObject("clients", clientService.findClients()
						.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			} catch ( VisiteCanNotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("visiteUpdate.html");
		return mv;
	}
	
	@RequestMapping(value = "/getVisitebyIdBienImmobilier", method = RequestMethod.GET)
	public ModelAndView findVisiteIdBienImmobilier(@RequestParam("biensImmobiliers") Long id) throws VisitesCanNotBeFoundException, CanNotFindAllConseillersImmobiliersException, BiensImmobiliersCannotBeFoundException, ClientCannotBeFoundException {
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("visites", visiteService.findVisiteIdBienImmobilier(id).stream().sorted(Comparator.comparing(Visite::getDateVisite)
						.thenComparing(Visite::getHeureVisite)).collect(Collectors.toList()));
				mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
						.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
				mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
						.collect(Collectors.toList()));
				mv.addObject("clients", clientService.findClients()
						.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			} catch (VisitesCanNotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("visite.html");
		return mv;
	}
	@RequestMapping(value = "/getVisitebyLoginConseillerImmobiliers", method = RequestMethod.GET)
	public ModelAndView findVisiteIdConseillerImmobilier(@RequestParam("conseillersImmobiliers") String login) throws VisitesCanNotBeFoundException, CanNotFindAllConseillersImmobiliersException, BiensImmobiliersCannotBeFoundException, ClientCannotBeFoundException {
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("visites", visiteService.findVisiteByLoginConseiller(login).stream().sorted(Comparator.comparing(Visite::getDateVisite)
						.thenComparing(Visite::getHeureVisite)).collect(Collectors.toList()));
				mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
						.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
				mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
						.collect(Collectors.toList()));
				mv.addObject("clients", clientService.findClients()
						.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			} catch (VisitesCanNotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("visite.html");
		return mv;
	}
	@RequestMapping(value = "/getVisitebyNomClient", method = RequestMethod.GET)
	public ModelAndView findVisiteIdClient(@RequestParam("client") String nom) throws VisitesCanNotBeFoundException, CanNotFindAllConseillersImmobiliersException, BiensImmobiliersCannotBeFoundException, ClientCannotBeFoundException {
		ModelAndView mv = new ModelAndView();
			try {
				mv.addObject("visites", visiteService.findVisiteByNomClient(nom).stream().sorted(Comparator.comparing(Visite::getDateVisite)
						.thenComparing(Visite::getHeureVisite)).collect(Collectors.toList()));
				mv.addObject("conseillers", conseillersImmobiliersService.findAllConseillersImmobilierss()
						.stream().sorted(Comparator.comparing(ConseillersImmobiliers::getLogin)).collect(Collectors.toList()));
				mv.addObject("biens", biensImmobiliersService.findBiensImmobiliers().stream().sorted((x,y)->x.getAdresse().getVille().compareTo(y.getAdresse().getVille()))
						.collect(Collectors.toList()));
				mv.addObject("clients", clientService.findClients()
						.stream().sorted(Comparator.comparing(Client::getNom)).collect(Collectors.toList()));
			} catch (VisitesCanNotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		mv.setViewName("visite.html");
		return mv;
	}
}
