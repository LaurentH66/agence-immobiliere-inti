package com.controlleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.entity.BiensImmobiliers;
import com.entity.ConseillersImmobiliers;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.ClasseStandardCannotBeFoundException;
import com.security.MyUserDetailService;
import com.service.BiensImmobiliersService;
import com.service.ClasseStandardService;
/**
 * 
 * @author Quentin PERINE
 *
 */

@Controller
public class LoginControlleur {

	@Autowired
	MyUserDetailService udetailservice;
	
	@Autowired
	BiensImmobiliersService bservice;
	
	@Autowired
	ClasseStandardService classeService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(){ 
		ModelAndView mv = new ModelAndView();
		mv.setViewName("login.html");
		return mv; 	
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(){ 
		udetailservice.setUserConnecte(null);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("logout.html");
		return mv; 	
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView homePage(){ 
		List<BiensImmobiliers> bienImmos = null;
		ModelAndView mv = new ModelAndView();
		try {
			bienImmos = bservice.findBiensImmobiliers();
		} catch(BiensImmobiliersCannotBeFoundException e) {
			e.printStackTrace();
		}
		try {
			mv.addObject("classes", classeService.findClasseStandards());
		} catch (ClasseStandardCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mv.addObject("biens", bienImmos);
		mv.addObject("user", udetailservice.getUserConnecte());
		mv.setViewName("home.html");
		return mv; 	
	}
	
	@RequestMapping(value = "/connexion", method = RequestMethod.GET)
	public ModelAndView connexion() {

	ConseillersImmobiliers consCo = udetailservice.getUserConnecte();
	ModelAndView mv = new ModelAndView();
	
	if(consCo != null) {
		if (consCo.getRoles().stream().anyMatch((r) -> r.getRoleName().equals("admin"))) {
			mv.addObject("user", consCo);
			mv.setViewName("index.html");
		}
		else if (consCo.getRoles().stream().anyMatch((r) -> r.getRoleName().equals("conseiller"))) {
			mv.setViewName("index.html");
		}
	}	
	else {
		mv.addObject("user", udetailservice.getUserConnecte());
		mv.setViewName("home.html");
	}
	
	return mv; 
}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView accueil() {
		ConseillersImmobiliers consCo = udetailservice.getUserConnecte();
		ModelAndView mv = new ModelAndView();
		if (consCo.getRoles().stream().anyMatch((r) -> r.getRoleName().equals("admin"))) {
			mv.addObject("user", consCo);
			mv.setViewName("index.html");
		}
		else if (consCo.getRoles().stream().anyMatch((r) -> r.getRoleName().equals("conseiller"))) {
			mv.setViewName("index.html");
		}
		mv.setViewName("index.html"); 
		return mv;
	} 	
}
