package com.controlleur;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.entity.Adresse;
import com.entity.BiensImmobiliers;
import com.entity.ClasseStandard;
import com.entity.Client;
import com.entity.Proprietaires;
import com.exception.BiensImmobiliersCannotBeDeletetException;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.BiensImmobiliersCannotBeSavedException;
import com.exception.BiensImmobiliersCannotBeUpdatedException;
import com.exception.CanNotFindAdresseByIdException;
import com.exception.CanNotFindAllAdresseException;
import com.exception.CanNotSaveAdresseException;

import com.exception.ClientCannotBeFoundException;

import com.exception.ClasseStandardCannotBeFoundException;

import com.exception.ClientCannotBeSavedException;
import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
import com.service.AdresseService;
import com.service.BiensImmobiliersService;
import com.service.ClasseStandardService;
import com.service.ClientService;
import com.service.ProprietairesService;
import com.service.TypeBienImmobilierService;
import com.service.TypeOffreService;


/**
 * 
 * @author Quentin PERINE
 *
 */

@Controller
@RequestMapping(value = "/gestionBienController")
public class GestionBienController {

	@Autowired
	BiensImmobiliersService bienService;
	
	@Autowired
	ProprietairesService proprioService;
	
	@Autowired
	AdresseService aservice;
	
	@Autowired
	ClasseStandardService csservice;
	
	@Autowired
	TypeOffreService toservice;
	
	@Autowired
	TypeBienImmobilierService tbiservice;
	
	@Autowired
	ClasseStandardService classeService;
	
	@Autowired
	ClientService cservice;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView findAllBien() {
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("classes", classeService.findClasseStandards());
			mv.addObject("proprios", proprioService.findProprietaires());
			mv.addObject("biens", bienService.findBiensImmobiliers());
		} catch (BiensImmobiliersCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProprietairesCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClasseStandardCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mv.setViewName("gestionBien.html");
		return mv;
	}

	@RequestMapping(value = "/addBien", method = RequestMethod.POST)
	public ModelAndView addBien(@ModelAttribute BiensImmobiliers bien, @RequestParam("superficie") int superficie, @ModelAttribute Adresse adresse, 
			@RequestParam("type_offre") String typeOffre, @RequestParam("typeBien") String typeBien) {
		ModelAndView mv = new ModelAndView();
		
		
		
		try {
			if(aservice.findAdresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), adresse.getCodePostal(), adresse.getPays()) != null) {
				bien.setAdresse(aservice.findAdresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), adresse.getCodePostal(), adresse.getPays()));	
			}
			else {
				aservice.saveAdresse(adresse);
				bien.setAdresse(adresse);
			}	
		} catch (CanNotSaveAdresseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (CanNotFindAllAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(typeOffre.equals("Location"))
		csservice.insererBienDansClasseStandard(bien, superficie, (Double) bien.getPrixLocation(), toservice.findTypeOffreByNom("Location"), tbiservice.findTypeBienImmobilierByName(typeBien));
		
		else {
			csservice.insererBienDansClasseStandard(bien, superficie, (Double) bien.getPrixAchatDemande(), toservice.findTypeOffreByNom("Achat"), tbiservice.findTypeBienImmobilierByName(typeBien));
		}
		
		
		try {
			bienService.saveBiensImmobiliers(bien);
		} catch (BiensImmobiliersCannotBeSavedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		
		mv.addObject("bien", bien);
		
		mv.setViewName("gestionBien2.html");
		return mv;
	}
	
	
	
	@RequestMapping(value = "/getpropriobynum", method = RequestMethod.GET)
	public ModelAndView getpropriobynum(@RequestParam("numeroTelephonePrivee") String numTel, @RequestParam("idBienImmobilier") Long idBien) {
		ModelAndView mv = new ModelAndView();
		
		try {
			mv.addObject("proprietaire",proprioService.findProprietaireByNumeroTelephonePrivee(numTel).get());
		} catch (ProprietairesCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			try {
				mv.addObject("bien", bienService.findBiensImmobiliersById(idBien));
			} catch (BiensImmobiliersCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	
	mv.setViewName("gestionBien2.html");
	return mv;
}
		
		
		
	
	@RequestMapping(value = "/addProprio", method = RequestMethod.POST)
	public ModelAndView addProprio(@ModelAttribute Proprietaires proprietaire, @ModelAttribute Adresse adresse, @RequestParam("idBienImmobilier") Long idBien) throws ClientCannotBeSavedException, ClientCannotBeFoundException, ProprietairesCannotBeFoundException, ClientDoesntExistException {
		System.out.println(idBien);
		ModelAndView mv = new ModelAndView();
		BiensImmobiliers bien = null;
		Proprietaires prop = null;

		try {
		if(aservice.findAdresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), adresse.getCodePostal(), adresse.getPays()) != null) {
			proprietaire.setAdresse(aservice.findAdresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), adresse.getCodePostal(), adresse.getPays()));	
		}
		
		else {
			aservice.saveAdresse(adresse);
			proprietaire.setAdresse(adresse);
		}
		
		} catch (CanNotFindAllAdresseException e) {
			e.printStackTrace();
		} catch (CanNotSaveAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		
		try {
			prop = proprioService.saveProprietaires(proprietaire);
		} catch (ProprietairesCannotBeSavedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			bien = bienService.findBiensImmobiliersById(idBien);
		} catch (BiensImmobiliersCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		bien.setProprietaire(prop);

		
		try {
			bienService.updateBiensImmobiliers(bien);
		} catch (BiensImmobiliersCannotBeUpdatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		mv.setViewName("gestionBien.html");
		return mv;
	}
	
	
	
	
	@RequestMapping(value = "/addProprio2", method = RequestMethod.POST)
	public ModelAndView addProprio2(@RequestParam("idBienImmobilier") Long idBien, @RequestParam("idProprietaire") Long idProprio) {
		System.out.println(idBien);
		ModelAndView mv = new ModelAndView();
		BiensImmobiliers bien = null;
		Proprietaires prop = null;

		
		try {
			prop = proprioService.findProprietairesById(idProprio);
		} catch (ProprietairesCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			bien = bienService.findBiensImmobiliersById(idBien);
		} catch (BiensImmobiliersCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		bien.setProprietaire(prop);
		
		try {
			bienService.updateBiensImmobiliers(bien);
		} catch (BiensImmobiliersCannotBeUpdatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		mv.setViewName("gestionBien.html");
		return mv;
	}
	
	

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView deleteBienById(@RequestParam("idBien") Long id) {

		try {
			bienService.deleteBiensImmobiliersById(id);
		} catch (BiensImmobiliersCannotBeDeletetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ModelAndView mv = new ModelAndView();

		mv.setViewName("gestionBien.html");
		return mv;
	}
	
	@RequestMapping(value = "/getbien", method = RequestMethod.GET)
	public ModelAndView findBienById(@RequestParam("idBien") Long id) {
		ModelAndView mv = new ModelAndView();
		
			try {
				mv.addObject("bien", bienService.findBiensImmobiliersById(id));
			} catch (BiensImmobiliersCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (bienService.findBiensImmobiliersById(id).getClasseStandard().getTypeOffreId().getModeOffre().equals("Location")) mv.addObject("type", 0);
				if (bienService.findBiensImmobiliersById(id).getClasseStandard().getTypeOffreId().getModeOffre().equals("Achat")) mv.addObject("type", 1);
			} catch (BiensImmobiliersCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				mv.addObject("typeBien", bienService.findBiensImmobiliersById(id).getClasseStandard().getTypeBienImmobilierId().getTypeBien());
			} catch (BiensImmobiliersCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mv.setViewName("gestionBienUpdate.html");
		return mv;
	}
	
	
	@RequestMapping(value = "/getbiensbytypeoffte", method = RequestMethod.GET)
	public ModelAndView findBienByTypeOffre(@RequestParam("type") String type) {
		ModelAndView mv = new ModelAndView();
		
		mv.addObject("biensSelonType", bienService.findBiensImmobiliersByTypeOffre(type));
		try {
			mv.addObject("classes", classeService.findClasseStandards());
		} catch (ClasseStandardCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (type.equals("Location")) mv.addObject("type", 0);
		if (type.equals("Achat")) mv.addObject("type", 1);
		System.out.println("*****************" +bienService.findBiensImmobiliersByTypeOffre(type));
		mv.setViewName("home.html");
		return mv;
	}
	
	@RequestMapping(value = "/getFormulaireByTypeOffre", method = RequestMethod.GET)
	public ModelAndView findFormByTypeOffre(@RequestParam("type") String type) {
		ModelAndView mv = new ModelAndView();
		if (type.equals("Location")) mv.addObject("type", 0);
		if (type.equals("Achat")) mv.addObject("type", 1);
		mv.setViewName("gestionBien.html");
		return mv;
		
	}
	
	@RequestMapping(value = "/getbienbyid", method = RequestMethod.GET)
	public ModelAndView getBienById(@RequestParam("idBien") Long id) {
		ModelAndView mv = new ModelAndView();
		
			try {
				if(bienService.findBiensImmobiliersById(id).getClasseStandard().getTypeOffreId().getModeOffre().equals("Location"))  {
				mv.addObject("bien", bienService.findBiensImmobiliersById(id));
				mv.addObject("type", 3);
				mv.addObject("classes", classeService.findClasseStandards());
				}
				else if (bienService.findBiensImmobiliersById(id).getClasseStandard().getTypeOffreId().getModeOffre().equals("Achat")) {
				mv.addObject("bien", bienService.findBiensImmobiliersById(id));
				mv.addObject("type", 4);
				mv.addObject("classes", classeService.findClasseStandards());
				}
				
			} catch (BiensImmobiliersCannotBeFoundException | ClasseStandardCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mv.addObject("clientsInteresses", cservice.findClientByVisiteOnBienImmobilier(id));
		mv.setViewName("gestionBien.html");
		return mv;
	}
	
//	@RequestMapping(value = "/getbienbyidClasseStandard", method = RequestMethod.GET)
//	public ModelAndView getBienByidClasseStandard(@RequestParam("idClasseStandard") Long id) {
//		ModelAndView mv = new ModelAndView();
//		ClasseStandard classeStd;		
//		
//		try {
//			classeStd = classeService.findClasseStandardById(id);
//			System.out.println(classeStd);
//			List<ClasseStandard> CS= classeService.findClasseStandardBySuperficieMaxPrixMax(classeStd.getSuperficieMaximale(), classeStd.getPrixMaximum(), classeStd.getTypeOffreId(), classeStd.getTypeBienImmobilierId()); 
//			List<BiensImmobiliers> biensIm =new ArrayList<BiensImmobiliers>();
//
//			for (ClasseStandard i:CS) {
//				for (BiensImmobiliers j:bienService.findBiensImmobiliersByClasseStandardId(i.getIdClasseStandard())) {
//					biensIm.add(j);
//				}
//			}
//
//			//mv.addObject("type", 5);
//			mv.addObject("classes", classeService.findClasseStandards());
//			mv.addObject("biensSelonType", biensIm);
//			//mv.addObject("biens", biensIm);
//			if (classeStd.getTypeOffreId().getModeOffre().equals("Location")) {
//				mv.addObject("type", 0);
//			}
//			else if (classeStd.getTypeOffreId().getModeOffre().equals("Achat")){
//				mv.addObject("type", 1);
//			}
//		} catch (ClasseStandardCannotBeFoundException | BiensImmobiliersCannotBeFoundException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		mv.setViewName("home.html");
//		return mv;
//	}
	
	
	@RequestMapping(value = "/getbienbyidClasseStandard", method = RequestMethod.GET)
	public ModelAndView getBienByidClasseStandard(@RequestParam("typeOffre") String typeOffre, @RequestParam("typeBien") String typeBien,
			@RequestParam("prixMin") int prixMin,@RequestParam("prixMax") int prixMax,
			@RequestParam("supMin") int supMin,@RequestParam("supMax") int supMax, @RequestParam("ville") String ville) {
		ModelAndView mv = new ModelAndView();
		
		
		mv.addObject("biens", bienService.findBiensImmobiliersByAllCriteriaOfClasseStandard(typeOffre, typeBien, prixMax, supMax, prixMin, supMin, ville));
		if(typeOffre.equals("Location")) {
			mv.addObject("type", 0);
		}
		else if (typeOffre.equals("Achat")) {
			mv.addObject("type", 1);
		}
		
		mv.setViewName("home.html");
		return mv;
	}
	
	@RequestMapping(value = "/getbienbyidClasseStandardGestionBien", method = RequestMethod.GET)
	public ModelAndView getBienByidClasseStandard2(@RequestParam("typeOffre") String typeOffre, @RequestParam("typeBien") String typeBien,
			@RequestParam("prixMin") int prixMin,@RequestParam("prixMax") int prixMax,
			@RequestParam("supMin") int supMin,@RequestParam("supMax") int supMax, @RequestParam("ville") String ville) {
		ModelAndView mv = new ModelAndView();
		
		
		
		mv.addObject("biens", bienService.findBiensImmobiliersByAllCriteriaOfClasseStandard(typeOffre, typeBien, prixMax, supMax, prixMin, supMin, ville));
		if(typeOffre.equals("Location")) {
			mv.addObject("type", 5);
		}
		else if (typeOffre.equals("Achat")) {
			mv.addObject("type", 6);
		}

		mv.setViewName("gestionBien.html");
		return mv;
	}
	
	
	
	@RequestMapping(value = "/updateBien", method = RequestMethod.POST)
	public ModelAndView updateBien(@ModelAttribute BiensImmobiliers bien, @RequestParam("superficie") int superficie, @ModelAttribute Adresse adresse,
			@RequestParam("type_offre") String typeOffre, @RequestParam("typeBien") String typeBien) {
		ModelAndView mv = new ModelAndView();
		
		
		System.out.println(bien);
		try {
			if(aservice.findAdresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), adresse.getCodePostal(), adresse.getPays()) != null) {
				bien.setAdresse(aservice.findAdresse(adresse.getNumero(), adresse.getRue(), adresse.getVille(), adresse.getCodePostal(), adresse.getPays()));	
			}
			else {
				aservice.saveAdresse(adresse);
				bien.setAdresse(adresse);
			}	
		} catch (CanNotSaveAdresseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (CanNotFindAllAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(typeOffre.equals("Location"))
		csservice.insererBienDansClasseStandard(bien, superficie, (Double) bien.getPrixLocation(), toservice.findTypeOffreByNom("Location"), tbiservice.findTypeBienImmobilierByName(typeBien));
		
		else {
			csservice.insererBienDansClasseStandard(bien, superficie, (Double) bien.getPrixAchatDemande(), toservice.findTypeOffreByNom("Achat"), tbiservice.findTypeBienImmobilierByName(typeBien));
		}
		
		try {
			bienService.updateBiensImmobiliers(bien);
		} catch (BiensImmobiliersCannotBeUpdatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		
		mv.addObject("bien", bien);
		
		mv.setViewName("gestionBien.html");
		return mv;
	}
	
	

	@RequestMapping(value = "/findClientsInteresses", method = RequestMethod.GET)
	public ModelAndView findClientsInteresses(@RequestParam("idBien") Long id) {
		ModelAndView mv = new ModelAndView();
		
			try {
				if(bienService.findBiensImmobiliersById(id).getClasseStandard().getTypeOffreId().getModeOffre().equals("Location"))  {
				mv.addObject("bien", bienService.findBiensImmobiliersById(id));
				mv.addObject("type", 3);
				mv.addObject("classes", classeService.findClasseStandards());
				}
				else if (bienService.findBiensImmobiliersById(id).getClasseStandard().getTypeOffreId().getModeOffre().equals("Achat")) {
				mv.addObject("bien", bienService.findBiensImmobiliersById(id));
				mv.addObject("type", 4);
				mv.addObject("classes", classeService.findClasseStandards());
				}
				
			} catch (BiensImmobiliersCannotBeFoundException | ClasseStandardCannotBeFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		mv.addObject("clientsInteresses", cservice.findClientByVisiteOnBienImmobilier(id));
		mv.setViewName("gestionBienDetails.html");
		return mv;
	}
}
