package com.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.entity.ConseillersImmobiliers;
import com.exception.CanNotFindConseillersImmobiliersException;
import com.repository.ConseillersImmobiliersRepository;

/**
 * 
 * @author Quentin Perine
 *
 */
@Service
public class MyUserDetailService implements UserDetailsService {

	@Autowired
	ConseillersImmobiliersRepository repo; 
	
	private ConseillersImmobiliers consConnecte;
	
	
	@Override
	public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Optional<ConseillersImmobiliers> consImmo = null;
		try {
			consImmo = repo.findConseillersImmobiliersByLogin(mail);
			consConnecte = consImmo.get();
			consImmo.orElseThrow(() -> new UsernameNotFoundException("Not found : " + mail));
		} catch (CanNotFindConseillersImmobiliersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return consImmo.map(MyUserDetails::new).get();
		
	}


	public ConseillersImmobiliers getUserConnecte() {
		return consConnecte;
	}


	public void setUserConnecte(ConseillersImmobiliers consConnecte) {
		this.consConnecte = consConnecte;
	}

	
	
	
//	@Override
//	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
//		// TODO Auto-generated method stub
//		return new MyUserDetails(s); 
//	}
	
	

}
