package com.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.entity.ConseillersImmobiliers;
import com.entity.Role;
/**
 * 
 * @author Quentin Perine
 *
 */
public class MyUserDetails implements UserDetails {

//	@Autowired
//	UserService service;
	
	private Long idConseillersImmobiliers;
	private String login;
	private String nom;
	private String password;
	private String prenom;
	private List<Role> roles;

	public MyUserDetails(ConseillersImmobiliers consImmo) {
		this.idConseillersImmobiliers = consImmo.getIdConseillersImmobiliers();
		this.login = consImmo.getLogin();
		this.nom = consImmo.getNom();
		this.password = consImmo.getPassword();
		this.prenom = consImmo.getPrenom();
		this.roles = consImmo.getRoles();
	}
	
//	public MyUserDetails(String mail) {
//		this.user = service.findUserByLogin(mail); 
//	}
	
	public MyUserDetails() {
	}
	
		
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		if(this.roles.stream().anyMatch((r) -> r.getRoleName().equals("admin")))
			return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
			else return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return passwordEncoder().encode(password);
	}
	

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return login;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	

}
