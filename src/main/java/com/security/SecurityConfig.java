package com.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Quentin Perine
 *
 */
@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	 @Autowired
	 UserDetailsService userDetailService;
	
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailService);
	
	}
	
	
	
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().loginPage("/login").permitAll().and().logout().logoutUrl("/logout").logoutSuccessUrl("/home");

		http.authorizeRequests().antMatchers("/connexion**/**", "/index").authenticated();

		http.authorizeRequests().antMatchers("/gestionClientController**/**").hasAnyRole("USER", "ADMIN");
		http.authorizeRequests().antMatchers("/gestionProprietaireController**/**").hasAnyRole("USER", "ADMIN");
		http.authorizeRequests().antMatchers("/visiteController**/**").hasAnyRole("USER", "ADMIN");
		http.authorizeRequests().antMatchers("/gestionContratController**/**").hasAnyRole("USER", "ADMIN");
		
		http.authorizeRequests().antMatchers("/gestionConseillerController**/**").hasAnyRole("ADMIN");
		
		http.authorizeRequests().antMatchers("/").hasAnyRole("USER", "ADMIN");
		
		http.authorizeRequests().antMatchers("/gestionBienController/getbienbyidClasseStandard**/**", 
				"/gestionBienController/getbiensbytypeoffte**/**").permitAll();
		
		http.authorizeRequests().antMatchers("/gestionBienController/index**/**",
				"/gestionBienController/addBien**/**", "/gestionBienController/addProprio**/**",
				"/gestionBienController/addProprio2**/**", "/gestionBienController/getpropriobynum**/**",
				"/gestionBienController/delete**/**", "/gestionBienController/getbien*/**",
				"/gestionBienController/getFormulaireByTypeOffre**/**","/gestionBienController/getbienbyid**/**",
				"/gestionBienController/updateBien**/**").hasAnyRole("USER", "ADMIN");
		


		http.csrf(); 
		
//		http.sessionManagement().maximumSessions(1).expiredUrl("/login?expired");
//		
//		http.sessionManagement().invalidSessionUrl("/login?expired");
//		http.httpBasic().authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/logout"));
//		http.exceptionHandling().defaultAuthenticationEntryPointFor(new LoginUrlAuthenticationEntryPoint("/login?expired"), 
//                new AntPathRequestMatcher("/**"));

		}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}



}
