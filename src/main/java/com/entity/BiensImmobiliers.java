package com.entity;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author Damien
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class  BiensImmobiliers {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idBienImmobilier;
	private double cautionLocative;
	private String dateAchat;
	private String dateDisposition;
	private String dateSoumission;
	private String etat;
	private String garniture;
	private double loyerMensuel;
	private double prixAchat;
	private double prixAchatDemande;
	private double prixLocation;
	private double revenuCadastral;
	private String status;
	private String typeBail;
	
	@ManyToOne
	@JoinColumn(name="FK_ADRESSE", referencedColumnName = "ID_ADRESSE")
	private Adresse adresse;
	
	@ManyToOne
	@JoinColumn(name="FK_CLASSE_STANDARD", referencedColumnName = "ID_CLASSE_STANDARD")
	private ClasseStandard classeStandard;
	
	@ManyToOne
	@JoinColumn(name="FK_CONTRAT", referencedColumnName = "ID_CONTRAT")
	private Contrat contrat;
	
	@ManyToOne
	@JoinColumn(name="FK_PROPRIETAIRE", referencedColumnName = "IDPROPRIETAIRE")
	private Proprietaires proprietaire;

	
	public BiensImmobiliers(Long idBienImmobilier) {
		super();
		this.idBienImmobilier = idBienImmobilier;
	}
	
	
	
	
}
