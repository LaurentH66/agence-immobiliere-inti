package com.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



/**
 * 
 * @author Quentin PERINE
 *
 */



@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class ConseillersImmobiliers {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CONSEILLERS_IMMOBILIERS")
	private Long idConseillersImmobiliers;
	@Column(name = "LOGIN", unique = true)
	private String login;
	@Column(name = "NOM")
	private String nom;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "PRENOM")
	private String prenom;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="CONS_IMMMO_ROLE", joinColumns = @JoinColumn(name = "FK_CONS_IMMO", referencedColumnName = "ID_CONSEILLERS_IMMOBILIERS"), 
	inverseJoinColumns = @JoinColumn(name = "FK_ROLE", referencedColumnName = "ID_ROLE"))
	private List<Role> roles;
	
	public ConseillersImmobiliers(Long idConseillersImmobiliers) {
		super();
		this.idConseillersImmobiliers = idConseillersImmobiliers;
	} 
	
	
	
}
