package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author Adeline Scherb
 *
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class ClasseStandard {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CLASSE_STANDARD")
	private Long idClasseStandard;
	@Column(name = "PRIX_MAXIMUM")
	private Double prixMaximum;
	@Column(name="SUPERFICIE_MAXIMALE")
	private int superficieMaximale;
	
	@ManyToOne
	@JoinColumn(name="FK_TYPE_BIEN_IMMOBILIER", referencedColumnName = "ID_TYPE_BIEN_IMMOBILIER")
	private TypeBienImmobilier typeBienImmobilierId;
	
	@ManyToOne
	@JoinColumn(name="FK_TYPE_OFFRE", referencedColumnName = "ID_TYPE_OFFRE")
	private TypeOffre typeOffreId;
	
	public String toString() {
		return  typeOffreId.getModeOffre()+" "+ typeBienImmobilierId.getTypeBien() +" : " + prixMaximum + "€ max et " + superficieMaximale +"m² max";
	}
}
