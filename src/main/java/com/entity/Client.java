package com.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author Adeline Scherb
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Client {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CLIENT")
	private Long idClient;
	@Column(name = "NOM")
	private String nom;
	@Column(name="NUMERO_TELEPHONE",unique = true)
	private String numeroTelephone;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name="FK_ADRESSE_CLIENT", referencedColumnName = "ID_ADRESSE")
	private Adresse adresse;

	@ManyToMany
	@JoinTable(name="Ligne_Client_Classe_Standard", joinColumns=@JoinColumn(name="FK_CLIENT",referencedColumnName = "ID_CLIENT"),
				inverseJoinColumns = @JoinColumn(name="FK_CLASSE_STANDARD", referencedColumnName = "ID_CLASSE_STANDARD"))
	private List<ClasseStandard> classesStandards;

	public Client(Long idClient) {
		super();
		this.idClient = idClient;
	}

	@Override
	public String toString() {
		return nom + " " + numeroTelephone;
	}
	
	
	
	
	
	
	
}
