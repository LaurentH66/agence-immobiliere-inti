package com.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Laurent Hurtado
 *
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Contrat {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_CONTRAT")
	private Long idContrat;
	@Column(name = "DATE_EFFECTIVE")
	private String dateEffective;
	@Column(name = "PRIX_EFFECTIF")
	private double prixEffectif;
	@Column(name = "REFERENCE_CONTRAT",unique = true)
	private String referenceContrat;
	
	@ManyToOne
	@JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID_CLIENT")
	private Client client;
	
	public Contrat(String dateEffective, Double prixEffectif, String referenceContrat) {
		super();
		this.dateEffective = dateEffective;
		this.prixEffectif = prixEffectif;
		this.referenceContrat = referenceContrat;
	}

	public Contrat(Long idContrat, String dateEffective, Double prixEffectif, String referenceContrat) {
		super();
		this.idContrat = idContrat;
		this.dateEffective = dateEffective;
		this.prixEffectif = prixEffectif;
		this.referenceContrat = referenceContrat;
	}


	
	
	
}
