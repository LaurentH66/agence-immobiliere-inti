package com.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Laurent Hurtado
 *
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TypeOffre {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TYPE_OFFRE")
	private Long idTypeOffre;
	@Column(name = "MODE_OFFRE")
	private String modeOffre;
	
}
