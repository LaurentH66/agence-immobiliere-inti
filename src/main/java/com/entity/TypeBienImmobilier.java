package com.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/** 
 * @author Charles
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class TypeBienImmobilier {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_TYPE_BIEN_IMMOBILIER")
	private Long idTypeBienImmobilier;
	@Column(name = "TYPE_BIEN")
	private String typeBien;
	
	public TypeBienImmobilier(String typeBien) {
		super();
		this.typeBien = typeBien;
	}
	
	
	
	

}
