package com.entity;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * 
 * @author Damien
 *
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Proprietaires{
	@Id
	private Long idProprietaire;
	private String nom;
	@Column(unique = true)
	private String numeroTelephonePrivee;
	private String numeroTelephoneTravail;
	private String prenom;
	
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name="FK_ADRESSE", referencedColumnName = "ID_ADRESSE")
	private Adresse adresse;
	
	
}
