package com.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** 
 * @author Charles
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Visite {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_VISITE")
	private Long idVisite;
	@Column(name = "DATE_VISITE")
	private String dateVisite;
	@Column(name = "HEURE_VISITE")
	private String heureVisite;
	
	@ManyToOne
	@JoinColumn (name="ID_BIEN_IMMOBILIER")
	private BiensImmobiliers biensImmobiliers;
	
	@ManyToOne
	@JoinColumn (name="ID_CLIENT")
	private Client client;
	
	@ManyToOne
	@JoinColumn (name="ID_CONSEILLER_IMMOBILIER")
	private ConseillersImmobiliers conseillerImmobilier;

	@Column(name = "START")
	private  LocalDateTime start;
	
	@Column(name = "TITLE")
	private  String title;
	
	@Column(name = "COLOR")
	private  String color;

	public Visite(String dateVisite, String heureVisite, BiensImmobiliers biensImmobiliers, Client client,
			ConseillersImmobiliers conseillerImmobilier) {
		super();
		this.dateVisite = dateVisite;
		this.heureVisite = heureVisite;
		this.biensImmobiliers = biensImmobiliers;
		this.client = client;
		this.conseillerImmobilier = conseillerImmobilier;
	}

   
	
	
	
	
	
	

}
