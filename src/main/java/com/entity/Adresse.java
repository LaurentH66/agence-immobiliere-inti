package com.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author Quentin PERINE
 *
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "ADRESSE")
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_ADRESSE")
	private Long idAdresse;
	@Column(name = "CODE_POSTAL")
	private String codePostal; 
	@Column(name = "NUMERO")
	private int numero;
	@Column(name = "PAYS")
	private String pays;
	@Column(name = "RUE")
	private String rue;
	@Column(name = "VILLE")
	private String ville;
	@Override
	public String toString() {
		return numero +" " +rue + ", "+ codePostal + " " + ville +", "+ pays ;
	}
	
	
}
