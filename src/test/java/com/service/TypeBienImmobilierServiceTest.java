package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify; 
import static org.mockito.Mockito.timeout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.entity.Adresse;
import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.entity.ConseillersImmobiliers;
import com.entity.TypeBienImmobilier;
import com.entity.Visite;
import com.exception.CanNotSaveAdresseException;
import com.exception.TypeBienImmobilierCanNotBeUpdatedException;
import com.exception.TypeBienImmobilierCannotBeSavedException;
import com.exception.TypeBienImmobiliersCanNotBeFoundException;
import com.exception.VisiteCanNotBeUpdatedException;
import com.exception.VisiteCannotBeSavedException;
import com.repository.TypeBienImmobilierRepository;
import com.repository.VisiteRepository;
/** 
 * @author Charles
 */
@SpringBootTest
public class TypeBienImmobilierServiceTest {
	
	@Autowired
	TypeBienImmobilierService typeBienService;
	
	@MockBean
	TypeBienImmobilierRepository typeBienRepository;

	@Test
	public void addTypeBienImmobilier_shouldReturnTypeBienImmobilier() {
		
		
		TypeBienImmobilier typeBien = new TypeBienImmobilier("Appartement");	
		TypeBienImmobilier typeBien2 = new TypeBienImmobilier(1l,"Appartement");
		
		when(typeBienRepository.save(typeBien)).thenReturn(typeBien2);

		try {
			typeBien=typeBienService.saveTypeBienImmobilier(typeBien);
		} catch (TypeBienImmobilierCannotBeSavedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertNotNull(typeBien);
		assertEquals(typeBien.getIdTypeBienImmobilier(),1l);
	}
	
	@Test
	public void getTypeBienImmobilierTest_shouldBeReturnData() {

		when(typeBienRepository.findAll()).thenReturn(Stream.of(
				new TypeBienImmobilier("Appartement"),
				new TypeBienImmobilier("Maison"),
				new TypeBienImmobilier("Garage")
				).collect(Collectors.toList()));
		
		try {
			assertEquals(typeBienService.findTypeBienImmobiliers().size(), 3);
		} catch (TypeBienImmobiliersCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	 }
	
	@Test
	public void updateTypeBienImmobilierWithoutSpecifiedId_shouldBeReturnTypeBienImmobilierCanNotBeUpdatedException() throws TypeBienImmobilierCanNotBeUpdatedException {
		
		TypeBienImmobilier typeBien = new TypeBienImmobilier("Appartement");
		
		when(typeBienRepository.save(typeBien)).thenReturn(new TypeBienImmobilier("Appartement"));
				
		TypeBienImmobilierCanNotBeUpdatedException exception =assertThrows(TypeBienImmobilierCanNotBeUpdatedException.class, ()->{typeBienService.updateTypeBienImmobilier(typeBien);
	});
		
		assertEquals("typeBien can not be updated : "+typeBien,exception.getMessage());
		
	}
	
	@Test
	public void deleteTypeBienImmobilierByIdTest() {
		typeBienRepository.deleteById(1l);
		verify(typeBienRepository,timeout(1)).deleteById((long)1);
	}
	
	
	

}



