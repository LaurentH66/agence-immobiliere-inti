package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;


import com.entity.Contrat;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ContratsCanNotBeFoundException;
import com.repository.ContratRepository;

@SpringBootTest
public class ContratServiceTest {

	@Autowired
	ContratService contratService;
	
	@MockBean
	ContratRepository contratRepository;
	
	@Test
	public void addContrat_shouldReturnContrat() {

		Contrat contrat = new Contrat("20/12/2021", 120000d, "4uZE");
		Contrat contratResult = new Contrat(1l, "20/12/2021", 120000d, "4uZE");

		when(contratRepository.save(contrat)).thenReturn(contratResult);

		contrat = contratRepository.save(contrat);

		assertNotNull(contrat);
		assertEquals(1l, contrat.getIdContrat());
		assertEquals(contratResult, contrat);

	}
	
	@Test
	public void deleteContratByIdTest() {
		
		contratRepository.deleteById(1l);
		verify(contratRepository, timeout(1)).deleteById((long) 1);
		
	}
	
	@Test
	public void getContratsTest_shouldBeReturnData() throws ContratsCanNotBeFoundException {
		
		when(contratRepository.findAll()).thenReturn(Stream.of(
				new Contrat("20/12/2021", 120000d, "4uZE"),
				new Contrat("20/12/2021", 120000d, "4uZX"),
				new Contrat("20/12/2021", 120000d, "4uZA")
				).collect(Collectors.toList()));
		assertEquals(3, contratService.findContrats().size());
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ContratsCanNotBeFoundException
	 */
	@Test
	public void getContratsByDateTest_shouldBeReturnData() throws ContratsCanNotBeFoundException, ContratCanNotBeFoundException {
		
		when(contratRepository.findContratByDateEffective("24/12/2018")).thenReturn(Stream.of(
				new Contrat("24/12/2018", 120000d, "4uZE"),
				new Contrat("20/12/2021", 120000d, "4uZX"),
				new Contrat("18/12/2004", 120000d, "4uZA")
				).filter(s -> s.getDateEffective().equals("24/12/2018")).collect(Collectors.toList()));
		assertEquals(1, contratService.findContratByDateEffective("24/12/2018").size());
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ContratsCanNotBeFoundException
	 */
	@Test
	public void getContratsByPrixTest_shouldBeReturnData() throws ContratsCanNotBeFoundException {
		
		when(contratRepository.findContratByPrixEffectif(120000d)).thenReturn(Stream.of(
				new Contrat("20/12/2021", 90000d, "4uZE"),
				new Contrat("20/12/2021", 120000d, "4uZX"),
				new Contrat("20/12/2021", 80000d, "4uZA")
				).filter(s -> s.getReferenceContrat().equals("4uZA")).collect(Collectors.toList()));
		assertEquals(1, contratService.findContratByPrixEffectif(120000d).size());
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ContratCanNotBeFoundException
	 */
//	@Test
//	public void getContratByReferenceContrat_shoudlReturnData() throws ContratCanNotBeFoundException {
//		
//		Optional<Contrat> contrat = Optional.ofNullable(Contrat.builder().dateEffective("09/05/1999").prixEffectif(99999d).referenceContrat("4uZE").build());
//		
//		when(contratRepository.findContratByReferenceContrat("4uZE")).thenReturn(contrat);
//	
//		try {
//			assertEquals(99999d, contratService.findContratByReferenceContrat("4uZE").get().getPrixEffectif());
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//	
//	}
}
