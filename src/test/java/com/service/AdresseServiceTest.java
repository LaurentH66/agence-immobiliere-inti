package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.entity.Adresse;
import com.exception.CanNotFindAllAdresseException;
import com.exception.CanNotSaveAdresseException;
import com.exception.CanNotUpdateAdresseException;
import com.repository.AdresseRepository;
import com.repository.AdresseRepositoryTest;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class AdresseServiceTest {

	@Autowired
	AdresseService aservice;
	
	@MockBean
	AdresseRepository arepo;
	
	@Test
	public void addAdresseTest_shouldReturnAdresse() { 
		
		Adresse adresse = Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build();

		Adresse adresseR = Adresse.builder().idAdresse(1l).codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build();
		
		when(arepo.save(adresse)).thenReturn(adresseR);
		
		try {
			adresse = aservice.saveAdresse(adresse);
		} catch (CanNotSaveAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		assertNotNull(adresse);
		assertEquals(adresse, adresseR);
		
	}
	
	
	
	@Test
	public void getAdressesTest_shouldReturnData() { 
		
		when(arepo.findAll()).thenReturn(Stream.of(
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build()
				).collect(Collectors.toList()));

		try {
			assertEquals(aservice.findAllAdresses().size(), 3);
		} catch (CanNotFindAllAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateAdresseWithoutSpecificId_shouldReturnCanNotUpdateAdresseException() throws CanNotUpdateAdresseException { 
		Adresse adresse = Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build();
		
		when(arepo.save(adresse)).thenReturn(Adresse.builder().idAdresse(2l).codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build());
		
		CanNotUpdateAdresseException exception = assertThrows(CanNotUpdateAdresseException.class, 
				() -> {aservice.updateAdresse(adresse);}
				);
		
		assertEquals("Adresse can not be updated", exception.getMessage());
		
			}
	
	
	@Test
	public void deleteAdresseByIdTest() { 

		arepo.deleteById(1l);
		
		verify(arepo,timeout(1)).deleteById(1l);
		
		
		/**
		 * @author Laurent Hurtado
		 * @throws CanNotFindAllAdresseException
		 */
	}
	
	@Test
	public void getAdressesWithSameCodePostalTest_shouldReturnData() throws CanNotFindAllAdresseException { 
		
		when(arepo.findAdresseByCodePostal("54000")).thenReturn(Stream.of(
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54010").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build()
				).filter(s -> s.getCodePostal().equals("54000")).collect(Collectors.toList()));

		try {
			assertEquals(2, aservice.findAdresseByCodePostal("54000").size());
		} catch (CanNotFindAllAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllAdresseException
	 */
	@Test
	public void getAdressesWithSamePaysTest_shouldReturnData() throws CanNotFindAllAdresseException { 
		
		when(arepo.findAdresseByPays("MEXIQUE")).thenReturn(Stream.of(
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54010").ville("Nancy").rue("Rue des Loutres").numero(4).pays("MEXIQUE").build(),
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build()
				).filter(s -> s.getPays().equals("MEXIQUE")).collect(Collectors.toList()));

		try {
			assertEquals(1, aservice.findAdresseByPays("MEXIQUE").size());
		} catch (CanNotFindAllAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllAdresseException
	 */
	@Test
	public void getAdressesWithSameRueTest_shouldReturnData() throws CanNotFindAllAdresseException { 
		
		when(arepo.findAdresseByRue("Rue des Loutres")).thenReturn(Stream.of(
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54010").ville("Nancy").rue("Rue des Pissenlits").numero(4).pays("MEXIQUE").build(),
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build()
				).filter(s -> s.getRue().equals("Rue des Loutres")).collect(Collectors.toList()));

		try {
			assertEquals(2, aservice.findAdresseByRue("Rue des Loutres").size());
		} catch (CanNotFindAllAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllAdresseException
	 */
	@Test
	public void getAdressesWithSameVilleTest_shouldReturnData() throws CanNotFindAllAdresseException { 
		
		when(arepo.findAdresseByVille("Nancy")).thenReturn(Stream.of(
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54010").ville("Nancy").rue("Rue des Pissenlits").numero(4).pays("MEXIQUE").build(),
				Adresse.builder().codePostal("66000").ville("Perpignan").rue("Rue des Loutres").numero(4).pays("FRANCE").build(),
				Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4).pays("FRANCE").build()
				).filter(s -> s.getVille().equals("Nancy")).collect(Collectors.toList()));

		try {
			assertEquals(3, aservice.findAdresseByVille("Nancy").size());
		} catch (CanNotFindAllAdresseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
