package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.entity.ClasseStandard;
import com.exception.ClasseStandardCannotBeFoundException;
import com.exception.ClasseStandardCannotBeSavedException;
import com.repository.ClasseStandardRepository;

@SpringBootTest
public class ClasseStandardServiceTest {

	@Autowired
	ClasseStandardService classeStandardService;
	
	@MockBean
	ClasseStandardRepository classeStandardRepository; 
	
	@Test
	public void addClasseStandardShouldReturnClasseStandard() {
		ClasseStandard classeStandard =ClasseStandard.builder().prixMaximum(20000d).superficieMaximale(25).build();
		
		ClasseStandard classeStandardResult =ClasseStandard.builder().idClasseStandard(1l).prixMaximum(20000d).superficieMaximale(25).build();
		
		when(classeStandardRepository.save(classeStandard)).thenReturn(classeStandardResult);
		
		try {
			classeStandard=classeStandardService.saveClasseStandard(classeStandard);
		} catch (ClasseStandardCannotBeSavedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertNotNull(classeStandard);
		assertEquals(classeStandard.getIdClasseStandard(),1l);
	}
	
	@Test
	public void getClasseStandardsTestShouldReturnData() {
		when(classeStandardRepository.findAll()).thenReturn(Stream.of(ClasseStandard.builder().prixMaximum(20000d).superficieMaximale(25).build(),
				ClasseStandard.builder().prixMaximum(30000d).superficieMaximale(25).build(),
				ClasseStandard.builder().prixMaximum(40000d).superficieMaximale(25).build())
				.collect(Collectors.toList()));		
		try {
			assertEquals(classeStandardService.findClasseStandards().size(),3 );
		} catch (ClasseStandardCannotBeFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void deleteClasseStandardById() {
		classeStandardRepository.deleteById(1l);
		verify(classeStandardRepository, timeout(1)).deleteById((long) 1);
	}
}
