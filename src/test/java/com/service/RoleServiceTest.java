package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.entity.Role;
import com.repository.RoleRepository;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class RoleServiceTest {

	@Autowired
	RoleService rservice;
	
	@MockBean                   // A chaque fois que rolevice a besoin d'utiliser urepo, on "prepare" le resultat, Mock Bean 
	RoleRepository rrepo;       // affecte données preparées à notre service

	
	@Test
	public void addRoleTest_shouldReturnRole() { 
		
		Role role = Role.builder().roleName("Supeer").description("bla").build();
		
		Role roleResult = Role.builder().idRole(1l).roleName("Supeer").description("bla").build();
		
		when(rrepo.save(role)).thenReturn(roleResult);
		
		role = rservice.saveRole(role); 
		
		assertNotNull(role);
		assertEquals(role, roleResult);
		
	}
	
	
	@Test
	public void getRolesTest_shouldReturnData() { 
		
		when(rrepo.findAll()).thenReturn(Stream.of(
				Role.builder().roleName("Supeer").description("bla").build(),
				Role.builder().roleName("Supeer").description("bla").build(),
				Role.builder().roleName("Supeer").description("bla").build()
				).collect(Collectors.toList()));

		assertEquals(rservice.findRoles().size(), 3);
	}
	

	
	@Test
	public void deleteRoleByIdTest() { 

		rrepo.deleteById(1l);
		
		verify(rrepo,timeout(1)).deleteById(1l);
		
		
	}

}
