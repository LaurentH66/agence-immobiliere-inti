package com.service;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.timeout;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.entity.ConseillersImmobiliers;
import com.entity.Visite;
import com.exception.VisiteCanNotBeUpdatedException;
import com.exception.VisiteCannotBeSavedException;
import com.exception.VisiteIDsAlreadyOccupiedAtThisMomentException;
import com.exception.VisitesCanNotBeFoundException;
import com.repository.VisiteRepository;

/**
 * @author Charles
 */
@SpringBootTest
public class VisiteServiceTest {

	@Autowired
	VisiteService visiteService;

	@MockBean
	VisiteRepository visiteRepository;

	@Test
	public void addVisite_shouldReturnVisite() {

		BiensImmobiliers bi = new BiensImmobiliers(3l);
		Client cl = new Client(3l);

		ConseillersImmobiliers ci = new ConseillersImmobiliers(3l);

		Visite visite = new Visite("17/07/2020", "15h30", bi, cl, ci);

		Visite visiteResult = new Visite(1l, "17/07/2020", "15h30", bi, cl, ci, null, null, null);

		when(visiteRepository.save(visite)).thenReturn(visiteResult);

		try {
			visite = visiteService.saveVisite(visite);
		} catch (VisiteCannotBeSavedException | VisiteIDsAlreadyOccupiedAtThisMomentException e) {

			e.printStackTrace();
		}
		assertNotNull(visite);
		assertEquals(visite.getIdVisite(), 1l);
	}

	@Test
	public void getVisiteTest_shouldBeReturnData() {

		BiensImmobiliers bi = new BiensImmobiliers(1l);
		Client cl = new Client(1l);
		ConseillersImmobiliers ci = new ConseillersImmobiliers(1l);

		when(visiteRepository.findAll()).thenReturn(
				Stream.of(new Visite("17/07/2020", "15h30", bi, cl, ci), new Visite("17/08/2020", "16h30", bi, cl, ci),
						new Visite("17/09/2020", "18h00", bi, cl, ci)).collect(Collectors.toList()));

		try {
			assertEquals(visiteService.findVisites().size(), 3);
		} catch (VisitesCanNotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void updateVisiteWithoutSpecifiedId_shouldBeReturnVisiteCanNotBeUpdatedException()
			throws VisiteCanNotBeUpdatedException {

		BiensImmobiliers bi = new BiensImmobiliers(1l);
		Client cl = new Client(1l);
		ConseillersImmobiliers ci = new ConseillersImmobiliers(1l);

		Visite visite = new Visite("17/07/2020", "15h30", bi, cl, ci);
		when(visiteRepository.save(visite)).thenReturn(new Visite("17/07/2020", "15h30", bi, cl, ci));

		VisiteCanNotBeUpdatedException exception = assertThrows(VisiteCanNotBeUpdatedException.class, () -> {
			visiteService.updateVisite(visite);
		});

		assertEquals("visite can not be updated : " + visite, exception.getMessage());

	}

	@Test
	public void deleteVisiteByIdTest() {
		visiteRepository.deleteById(1l);
		verify(visiteRepository, timeout(1)).deleteById((long) 1);
	}

	@Test
	public void addImpossibleVisite_shouldReturnIdsVisiteException()
			throws VisiteIDsAlreadyOccupiedAtThisMomentException, VisiteCannotBeSavedException {


		Client cl = new Client(3l);

		BiensImmobiliers bi2 = new BiensImmobiliers(32l);
		ConseillersImmobiliers ci2 = new ConseillersImmobiliers(32l);
		
		Visite visite2 = new Visite("17/07/2020", "15h30", bi2, cl, ci2);
		Visite visiteResult2 = new Visite(2l, "17/07/2020", "15h30", bi2, cl, ci2, null, null, null);

		when(visiteRepository.save(visite2)).thenReturn(visiteResult2);
		/** On suppose que le client a déjà une visite à cette date/heure là**/
		when(visiteRepository.existsByDateHeureIdClient(visite2.getDateVisite(), visite2.getHeureVisite(), visite2.getClient().getIdClient())).thenReturn(true);

		assertThrows(VisiteIDsAlreadyOccupiedAtThisMomentException.class, () -> {
			visiteService.saveVisite(visite2);	});	
		}
	
	@Test
	public void updateImpossibleVisite_shouldReturnIdsVisiteException()
			throws VisiteCanNotBeUpdatedException, VisiteIDsAlreadyOccupiedAtThisMomentException {

		BiensImmobiliers bi = new BiensImmobiliers(1l);
		Client cl = new Client(1l);
		ConseillersImmobiliers ci = new ConseillersImmobiliers(1l);

		Visite visite = new Visite(1l,"17/07/2020", "15h30", bi, cl, ci, null, null, null);
		when(visiteRepository.save(visite)).thenReturn(new Visite(1l,"17/07/2020", "15h30", bi, cl, ci, null, null, null));
		/** On suppose que le client a déjà une visite à cette date/heure là**/
		when(visiteRepository.existsByDateHeureIdClient(visite.getDateVisite(), visite.getHeureVisite(), visite.getClient().getIdClient())).thenReturn(true);


		assertThrows(VisiteIDsAlreadyOccupiedAtThisMomentException.class, () -> {
			visiteService.updateVisite(visite);
		});

	}
	
	
}
