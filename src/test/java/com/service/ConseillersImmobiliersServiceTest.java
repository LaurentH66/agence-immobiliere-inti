package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.stereotype.Service;

import com.entity.ConseillersImmobiliers;
import com.exception.CanNotFindAllConseillersImmobiliersException;
import com.exception.CanNotFindConseillersImmobiliersException;
import com.exception.CanNotSaveConseillersImmobiliersException;
import com.exception.CanNotUpdateConseillersImmobiliersException;
import com.repository.ConseillersImmobiliersRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Quentin PERINE
 *
 */

@SpringBootTest
@Slf4j
public class ConseillersImmobiliersServiceTest {

	@Autowired
	ConseillersImmobiliersService ciservice;
	
	@MockBean
	ConseillersImmobiliersRepository cirepo;
	
	@Test
	public void addConseillersImmobiliersTest_shouldReturnConseillersImmobiliers() { 
		
		ConseillersImmobiliers conseillersImmobiliers = ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build();

		ConseillersImmobiliers conseillersImmobiliersR = ConseillersImmobiliers.builder().idConseillersImmobiliers(1l).nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build();
		
		when(cirepo.save(conseillersImmobiliers)).thenReturn(conseillersImmobiliersR);
		
		try {
			conseillersImmobiliers = ciservice.saveConseillersImmobiliers(conseillersImmobiliers);
		} catch (CanNotSaveConseillersImmobiliersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		assertNotNull(conseillersImmobiliers);
		assertEquals(conseillersImmobiliers, conseillersImmobiliersR);
		
	}
	
	
	
	@Test
	public void getConseillersImmobilierssTest_shouldReturnData() { 
		
		when(cirepo.findAll()).thenReturn(Stream.of(
				ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build(),
				ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build(),
				ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build()
				).collect(Collectors.toList()));

		try {
			assertEquals(ciservice.findAllConseillersImmobilierss().size(), 3);
		} catch (CanNotFindAllConseillersImmobiliersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void updateConseillersImmobiliersWithoutSpecificId_shouldReturnCanNotUpdateConseillersImmobiliersException() throws CanNotUpdateConseillersImmobiliersException { 
		ConseillersImmobiliers conseillersImmobiliers = ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build();
		
		when(cirepo.save(conseillersImmobiliers)).thenReturn(ConseillersImmobiliers.builder().idConseillersImmobiliers(2l).nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build());
		
		CanNotUpdateConseillersImmobiliersException exception = assertThrows(CanNotUpdateConseillersImmobiliersException.class, 
				() -> {ciservice.updateConseillersImmobiliers(conseillersImmobiliers);}
				);
		
		assertEquals("ConseillersImmobiliers can not be updated", exception.getMessage());
		
			}
	
	
	@Test
	public void deleteConseillersImmobiliersByIdTest() { 

		cirepo.deleteById(1l);
		
		verify(cirepo,timeout(1)).deleteById(1l);
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindConseillersImmobiliersException
	 */
	@Test
	public void getConseillerImmobilierByLoginTest_shouldReturnConseillerImmobilier() throws CanNotFindConseillersImmobiliersException {
		
		Optional<ConseillersImmobiliers> conseillerImmobilier = Optional.ofNullable(ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build());
		
		when(cirepo.findConseillersImmobiliersByLogin("sjohn")).thenReturn(conseillerImmobilier);
		
		try {
			assertEquals("Steeve", ciservice.findConseillersImmobiliersByLogin("sjohn").get().getNom());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllConseillersImmobiliersException
	 */
	@Test
	public void getAllConseillersImmobilierByNomTest_shouldReturnListOfConseillers() throws CanNotFindAllConseillersImmobiliersException {
		
		when(cirepo.findConseillersImmobiliersByNom("Steeve")).thenReturn(Stream.of(
				ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build(),
				ConseillersImmobiliers.builder().nom("Stov").prenom("Johnson").login("sjohn").password("toto").build(),
				ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build()
				).filter(s -> s.getNom().equals("Steeve")).collect(Collectors.toList()));
		
		try {
			assertEquals(2, ciservice.findConseillersImmobiliersByNom("Steeve").size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllConseillersImmobiliersException
	 */
	@Test
	public void getAllConseillersImmobilierByPrenomTest_shouldReturnListOfConseillers() throws CanNotFindAllConseillersImmobiliersException {
		
		when(cirepo.findConseillersImmobiliersByPrenom("Pablo")).thenReturn(Stream.of(
				ConseillersImmobiliers.builder().nom("Steeve").prenom("Pablo").login("sjohn").password("toto").build(),
				ConseillersImmobiliers.builder().nom("Stov").prenom("Johnson").login("sjohn").password("toto").build(),
				ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build()
				).filter(s -> s.getPrenom().equals("Pablo")).collect(Collectors.toList()));
		
		try {
			assertEquals(1, ciservice.findConseillersImmobiliersByPrenom("Pablo").size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
