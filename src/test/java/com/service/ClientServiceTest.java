package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.entity.Client;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientCannotBeSavedException;
import com.repository.ClientRepository;

@SpringBootTest
public class ClientServiceTest {

	@Autowired
	ClientService clientService;

	@MockBean
	ClientRepository clientRepository;

	@Test
	public void addClient_shouldReturnClient() {
		Client client = Client.builder().nom("Scherb").numeroTelephone("0602020104").build();

		Client clientResult = Client.builder().idClient(1l).nom("Scherb").numeroTelephone("0602020104").build();

		when(clientRepository.save(client)).thenReturn(clientResult);

		try {
			client = clientService.saveClient(client);
		} catch (ClientCannotBeSavedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertNotNull(client);
		assertEquals(client.getIdClient(), 1l);
	}

	@Test
	public void getClientsTest_ShouldReturnData() {
		when(clientRepository.findAll()).thenReturn(Stream
				.of(Client.builder().nom("Scherb1").numeroTelephone("0602020104").build(),
						Client.builder().nom("Scherb2").numeroTelephone("0602020105").build(),
						Client.builder().nom("Scherb3").numeroTelephone("0602020106").build())
				.collect(Collectors.toList()));

		try {
			assertEquals(clientService.findClients().size(), 3);
		} catch (ClientCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deleteClientById() {
		clientRepository.deleteById(1l);
		verify(clientRepository, timeout(1)).deleteById((long) 1);
	}

	/**
	 * @author LaurentHurtado
	 * @throws ClientCannotBeFoundException
	 */
	@Test
	public void getClientsByNomTest_ShouldReturnData() throws ClientCannotBeFoundException {
		when(clientRepository.findClientByNom("Scherb1")).thenReturn(Stream
				.of(Client.builder().nom("Scherb1").numeroTelephone("0602020104").build(),
						Client.builder().nom("Scherb2").numeroTelephone("0602020105").build(),
						Client.builder().nom("Scherb3").numeroTelephone("0602020106").build())
				.filter(s -> s.getNom().equals("Scherb1")).collect(Collectors.toList()));

		try {
			assertEquals(1, clientService.findClientByNom("Scherb1").size());
		} catch (ClientCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @author LaurentHurtado
	 * @throws ClientCannotBeFoundException
	 */
	@Test
	public void getClientByNumeroTelephoneTest_ShouldReturnData() throws ClientCannotBeFoundException {
		
		Optional<Client> client =Optional.ofNullable(Client.builder().nom("Scherb").numeroTelephone("0602020104").build());
		
		when(clientRepository.findClientByNumeroTelephone("0602020104")).thenReturn(client);
		
	try { 
		assertEquals("Scherb", clientService.findClientByNumeroTelephone("0602020104").get().getNom());
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
		
	}
}
