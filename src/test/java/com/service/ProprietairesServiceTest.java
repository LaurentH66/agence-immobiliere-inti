package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.entity.Client;
import com.entity.Proprietaires;
import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
//import com.exception.ProprietairesCannotBeFoundException;
//import com.exception.ProprietairesCannotBeSavedException;
import com.repository.ProprietairesRepository;

@SpringBootTest
public class ProprietairesServiceTest {

	@Autowired
	ProprietairesService proprietairesService;
	
	@MockBean
	ProprietairesRepository proprietairesRepository; 
	
	/**
	 * 
	 * @author Damien
	 *
	 */
//	@Test
//	public void addProprietaires_shouldReturnProprietaires() {
//		Proprietaires proprietaire =Proprietaires.builder().nom("Croche").numeroTelephonePrivee("0602020104").numeroTelephoneTravail("0602020105").prenom("Sarah").build();
//		
//		Proprietaires proprietaireResult =Proprietaires.builder().idProprietaire(1l).nom("Croche").numeroTelephonePrivee("0602020104").numeroTelephoneTravail("0602020105").prenom("Sarah").build();
//		
//		when(proprietairesRepository.save(proprietaire)).thenReturn(proprietaireResult);
//		
//		try {
//			proprietaire=proprietairesService.saveProprietaires(proprietaire);
//		} catch (ProprietairesCannotBeSavedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		assertNotNull(proprietaire);
//		assertEquals(proprietaire.getIdProprietaire(),1l);
//	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	public void getProprietairessTest_ShouldReturnData() {
		when(proprietairesRepository.findAll()).thenReturn(Stream.of(Proprietaires.builder().nom("Croche").numeroTelephonePrivee("0602020124").numeroTelephoneTravail("0602020107").prenom("Sarah").build(),
				Proprietaires.builder().nom("Croche2").numeroTelephonePrivee("0602020114").numeroTelephoneTravail("0602020105").prenom("Sarah").build(),
				Proprietaires.builder().nom("Croche3").numeroTelephonePrivee("0602020115").numeroTelephoneTravail("0602020106").prenom("Sarah").build())
				.collect(Collectors.toList()));
		
		try {
			assertEquals(proprietairesService.findProprietaires().size(),3 );
		} catch (ProprietairesCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	public void deleteProprietairesById() {
		proprietairesRepository.deleteById(1l);
		verify(proprietairesRepository, timeout(1)).deleteById((long) 1);
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	public void getProprietairesWithSameNomTest_shouldReturnData() throws ProprietairesCannotBeFoundException {
		when(proprietairesRepository.findProprietaireByNom("Croche")).thenReturn(Stream.of(
				Proprietaires.builder().nom("Croche").numeroTelephonePrivee("0602020104").numeroTelephoneTravail("0602020105").prenom("Sarah").build(),
				Proprietaires.builder().nom("Croche2").numeroTelephonePrivee("0702020104").numeroTelephoneTravail("0702020105").prenom("Sarah").build(),
				Proprietaires.builder().nom("Croche3").numeroTelephonePrivee("0802020104").numeroTelephoneTravail("0802020105").prenom("Sarah").build())
		.filter(s -> s.getNom().equals("Croche")).collect(Collectors.toList()));
	
		try {
			assertEquals(1, proprietairesService.findProprietaireByNom("Croche").size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	public void getProprietaireByNumeroTelephonePriveeTest_shouldReturnProprietaire() throws ProprietairesCannotBeFoundException {
		
		Optional<Proprietaires> proprietaire = Optional.ofNullable(Proprietaires.builder().nom("Jhonson").numeroTelephonePrivee("0612345678").numeroTelephoneTravail("0611111111").prenom("Steev").build());
		
		when(proprietairesRepository.findProprietaireByNumeroTelephonePrivee("0612345678")).thenReturn(proprietaire);
		
		try {
			assertEquals("Jhonson", proprietairesService.findProprietaireByNumeroTelephonePrivee("0612345678").get().getNom());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	public void getProprietaireByNumeroTelephoneTravailTest_shouldReturnProprietaire() throws ProprietairesCannotBeFoundException {
		
		Optional<Proprietaires> proprietaire = Optional.ofNullable(Proprietaires.builder().nom("Jhonson").numeroTelephonePrivee("0612345678").numeroTelephoneTravail("0611111111").prenom("Steev").build());
		
		when(proprietairesRepository.findProprietaireByNumeroTelephoneTravail("0611111111")).thenReturn(proprietaire);
		
		try {
			assertEquals("Steev", proprietairesService.findProprietaireByNumeroTelephoneTravail("0611111111").get().getPrenom());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	public void getProprietairesWithSamePrenomTest_shouldReturnData() throws ProprietairesCannotBeFoundException {
		when(proprietairesRepository.findProprietaireByPrenom("Sarah")).thenReturn(Stream.of(
				Proprietaires.builder().nom("Croche").numeroTelephonePrivee("0602020104").numeroTelephoneTravail("0602020105").prenom("Sarah").build(),
				Proprietaires.builder().nom("Croche2").numeroTelephonePrivee("0702020104").numeroTelephoneTravail("0702020105").prenom("Sarah").build(),
				Proprietaires.builder().nom("Croche3").numeroTelephonePrivee("0802020104").numeroTelephoneTravail("0802020105").prenom("Sarah").build())
		.filter(s -> s.getPrenom().equals("Sarah")).collect(Collectors.toList()));
	
		try {
			assertEquals(3, proprietairesService.findProprietaireByPrenom("Sarah").size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	
}
