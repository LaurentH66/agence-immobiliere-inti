package com.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.entity.BiensImmobiliers;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.BiensImmobiliersCannotBeSavedException;
import com.exception.ProprietairesCannotBeFoundException;
//import com.exception.BiensImmobiliersCannotBeFoundException;
//import com.exception.BiensImmobiliersCannotBeSavedException;
import com.repository.BiensImmobiliersRepository;

@SpringBootTest
public class BiensImmobiliersServiceTest {

	@Autowired
	BiensImmobiliersService BiensImmobiliersService;
	
	@MockBean
	BiensImmobiliersRepository BiensImmobiliersRepository; 
	 
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	public void addBiensImmobiliersShouldReturnBiensImmobiliers() {
		BiensImmobiliers bienImmobilier =BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build();
		
		BiensImmobiliers bienImmobilierResult =BiensImmobiliers.builder().idBienImmobilier(1l).cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build();
		
		when(BiensImmobiliersRepository.save(bienImmobilier)).thenReturn(bienImmobilierResult);
		
		try {
			bienImmobilier=BiensImmobiliersService.saveBiensImmobiliers(bienImmobilier);
		} catch (BiensImmobiliersCannotBeSavedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertNotNull(bienImmobilier);
		assertEquals(bienImmobilier.getIdBienImmobilier(),1l);
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	public void getBiensImmobilierssTestShouldReturnData() {
		when(BiensImmobiliersRepository.findAll()).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build())
				.collect(Collectors.toList()));
		
		try {
			assertEquals(BiensImmobiliersService.findBiensImmobiliers().size(),3 );
		} catch (BiensImmobiliersCannotBeFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	public void deleteBiensImmobiliersById() {
		BiensImmobiliersRepository.deleteById(1l);
		verify(BiensImmobiliersRepository, timeout(1)).deleteById((long) 1);
	}
	/**
	 * @author Damien
	 */
	@Test
	public void findByCautionMaxBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMax(14)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getCautionLocative() < 14 ).collect(Collectors.toList()));
	try {
		assertEquals(2, BiensImmobiliersService.findBiensImmobiliersByCautionlocativeMax(14).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}	
	/**
	 * @author Damien
	 */
	@Test
	public void findByCautionMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMin(14)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getCautionLocative() > 14 ).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByCautionlocativeMin(14).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	/**
	 * @author Damien
	 */
	@Test
	public void findByCautionMaxMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMaxMin(14,15)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getCautionLocative() < 14 && s.getCautionLocative() > 13).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByCautionlocativeMaxMin(14,15).size());
	} catch (Exception e) {
		e.printStackTrace();
	}

}
	/**
	 * @author Damien
	 */
	@Test
	public void findByLoyerMensMaxBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMax(602.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getLoyerMensuel() < 602.0).collect(Collectors.toList()));
	try {
		assertEquals(2, BiensImmobiliersService.findBiensImmobiliersByLoyermensuelMax(602.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	/**
	 * @author Damien
	 */
	@Test
	public void findByLoyerMensMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMin(602.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getLoyerMensuel() > 602.0 ).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByLoyermensuelMin(602.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	/**
	 * @author Damien
	 */
	@Test
	public void findByLoyerMensMaxMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMaxMin(601.0,602.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getLoyerMensuel() > 601.0 && s.getLoyerMensuel() < 602.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByLoyermensuelMaxMin(601.0,602.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}

}	
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixlocationMaxBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMax(605.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getPrixLocation() < 605.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixlocationMax(605.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixlocationMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMin(606.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getPrixLocation() > 606.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixlocationMin(606.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixlocationMaxMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMaxMin(604.0,605)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
		    .filter(s -> s.getLoyerMensuel() > 601.0 && s.getLoyerMensuel() < 602.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixlocationMaxMin(604.0,605).size());
	} catch (Exception e) {
		e.printStackTrace();
	}

}
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixachatMaxBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMax(1201.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getPrixAchat() < 1201.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixachatMax(1201.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixachatMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMin(1202.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getPrixAchat() > 1202.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixachatMin(1202.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixachatMaxMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMaxMin(1201.0,1202.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
		    .filter(s -> s.getPrixAchat() > 1201.0 && s.getPrixAchat() < 1202.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixachatMaxMin(1201.0,1202.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}

}	
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixachatdemandeMaxBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMax(1206.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getPrixAchatDemande() < 1206.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixachatdemandeMax(1206.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixachatMindemandeBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMin(1207.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getPrixAchatDemande() > 1207.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixachatdemandeMin(1207.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	/**
	 * @author Damien
	 */
	@Test
	public void findByPrixachatdemandeMaxMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMaxMin(1206.0,1207.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
		    .filter(s -> s.getPrixAchatDemande() > 1206.0 && s.getPrixAchatDemande() < 1207.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByPrixachatdemandeMaxMin(1206.0,1207.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}

}
	/**
	 * @author Damien
	 */
	@Test
	public void findByRevenucadastralMaxBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMax(201.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getRevenuCadastral() < 201.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByRevenucadastralMax(201.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	/**
	 * @author Damien
	 */
	@Test
	public void findByRevenucadastralMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
	when(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMin(202.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
			BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
			.filter(s -> s.getRevenuCadastral() > 202.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByRevenucadastralMin(202.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	/**
	 * @author Damien
	 */
	@Test
	public void findByRevenucadastralMaxMinBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMaxMin(201.0,202.0)).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vendu").typeBail("apart").build())
		    .filter(s -> s.getRevenuCadastral() > 201.0 && s.getRevenuCadastral() < 202.0).collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByRevenucadastralMaxMin(201.0,202.0).size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	/**
	 * @author Damien
	 */
	@Test
	public void findByTypeBailBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByTypeBail("apart")).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gaod").garniture("gcod").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vandu").typeBail("bpart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gbod").garniture("gdod").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vbndu").typeBail("cpart").build())
		    .filter(s -> s.getTypeBail() == "apart").collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByTypeBail("apart").size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}	
	/**
	 * @author Damien
	 */
	@Test
	public void findByGarnitureBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByGarniture("gcod")).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gaod").garniture("gcod").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vandu").typeBail("bpart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gbod").garniture("gdod").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vbndu").typeBail("cpart").build())
		    .filter(s -> s.getGarniture() == "gcod").collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByGarniture("gcod").size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}	
	/**
	 * @author Damien
	 */
	@Test
	public void findByEtatBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByEtat("gaod")).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gaod").garniture("gcod").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vandu").typeBail("bpart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gbod").garniture("gdod").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vbndu").typeBail("cpart").build())
		    .filter(s -> s.getEtat() == "gaod").collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByEtat("gaod").size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}	
	/**
	 * @author Damien
	 */
	@Test
	public void findByStatusBiensImmobiliers() throws ProprietairesCannotBeFoundException{
		when(BiensImmobiliersRepository.findBiensImmobiliersByStatus("vendu")).thenReturn(Stream.of(BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(604.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build(),
				BiensImmobiliers.builder().cautionLocative(13.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gaod").garniture("gcod").loyerMensuel(601.5).prixAchat(1201.5).prixAchatDemande(1206.5).prixLocation(605.5).revenuCadastral(201.5).status("vandu").typeBail("bpart").build(),
				BiensImmobiliers.builder().cautionLocative(14.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("gbod").garniture("gdod").loyerMensuel(602.5).prixAchat(1202.5).prixAchatDemande(1207.5).prixLocation(606.5).revenuCadastral(202.5).status("vbndu").typeBail("cpart").build())
		    .filter(s -> s.getStatus() == "vendu").collect(Collectors.toList()));
	try {
		assertEquals(1, BiensImmobiliersService.findBiensImmobiliersByStatus("vendu").size());
	} catch (Exception e) {
		e.printStackTrace();
	}
}
}
