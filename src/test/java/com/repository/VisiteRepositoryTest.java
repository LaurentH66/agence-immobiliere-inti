package com.repository;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.BiensImmobiliers;
import com.entity.Client;
import com.entity.ConseillersImmobiliers;
import com.entity.Visite;
import com.exception.VisiteCannotBeSavedException;
import com.exception.VisiteIDsAlreadyOccupiedAtThisMomentException;
import com.exception.VisitesCanNotBeFoundException;

import lombok.extern.slf4j.Slf4j;
/** 
 * @author Charles
 */
@SpringBootTest
@Slf4j
public class VisiteRepositoryTest {

	
	@Autowired
	private VisiteRepository repo; 
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "delete from VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void addVisite_shouldReturnVisite() {
				
		BiensImmobiliers bi=new BiensImmobiliers(1l);
		Client cl=new Client(1l);
		ConseillersImmobiliers ci=new ConseillersImmobiliers(1l);
		
		
		Visite visite= new Visite("17/07/2020","15h30", bi,cl,ci);
		
		log.info("Start addVisite_shouldReturnVisite");

		visite = repo.save(visite);
		
		assertNotNull(visite);
		
		log.info("End addVisite_shouldReturnVisite");

		
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)


	public void updateVisite_shouldReturnVisite() {
		
		
		BiensImmobiliers bi=new BiensImmobiliers(1l);
		Client cl=new Client(1l);
		ConseillersImmobiliers ci=new ConseillersImmobiliers(1l);
		

		
		Visite visite= new Visite("17/07/2020","15h30", bi,cl,ci);
		
		log.info("Start updateVisite_shouldReturnVisite");
		visite = repo.save(visite); 
		
		assertNotNull(visite);
		assertEquals("17/07/2020", visite.getDateVisite());
		
		log.info("End updateVisite_shouldReturnVisite");
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void deleteVisiteById() {
		
		log.info("Start deleteVisiteById");
		repo.deleteById(1l);
		
		assertNull(repo.findById(1l).orElse(null));
		
		log.info("End deleteVisiteById");
	}
	
	@Test

	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findVisiteById_shouldReturnVisite() {
		
		Optional<Visite> visite = repo.findById(1l); 
		assertNotNull(visite);
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve2','toto2', 'Johnson2')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(2,'Scherb','0666666667')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(2,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(2, '17/07/2020','15h30',2,2,2)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	
	
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findAllVisite_shouldReturnListOfVisite() {
		
		assertNotNull(repo.findAll());
		assertEquals(repo.findAll().size(), 2);
	}
	
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findVisiteByDateHeureIdClient_shouldReturnVisite() throws VisitesCanNotBeFoundException {
		
		Optional<Visite> visite = repo.findVisiteByDateHeureIdClient("17/07/2020", "15h30", 1l); 
		assertEquals(visite.get().getIdVisite(),1);
		assertEquals(visite.get().getHeureVisite(),"15h30");

	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findVisiteByDateHeureIdBienImmobilier_shouldReturnVisite() throws VisitesCanNotBeFoundException {
		
		Optional<Visite> visite = repo.findVisiteByDateHeureIdBienImmobilier("17/07/2020", "15h30", 1l); 
		assertEquals(visite.get().getIdVisite(),1);
		assertEquals(visite.get().getHeureVisite(),"15h30");
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findVisiteByDateHeureIdConseiller_shouldReturnVisite() throws VisitesCanNotBeFoundException {
		
		Optional<Visite> visite = repo.findVisiteByDateHeureIdConseiller("17/07/2020", "15h30", 1l); 
		assertEquals(visite.get().getIdVisite(),1);
		assertEquals(visite.get().getHeureVisite(),"15h30");

	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve2','toto2', 'Johnson2')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(2,'Scherb','0666666667')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(2,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(2, '17/07/2020','17h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(3, '18/07/2020','17h30',2,2,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findVisitesByIdClient_shouldReturnListOfVisite() throws VisitesCanNotBeFoundException {
		
		assertNotNull(repo.findVisiteIdClient(1l));
		assertEquals(repo.findVisiteIdClient(1l).size(), 2);
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve2','toto2', 'Johnson2')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(2,'Scherb','0666666667')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(2,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(2, '17/07/2020','17h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(3, '18/07/2020','17h30',2,2,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findVisitesByIdConseiller_shouldReturnListOfVisite() throws VisitesCanNotBeFoundException {
		
		assertNotNull(repo.findVisiteIdConseiller(1l));
		assertEquals(repo.findVisiteIdConseiller(1l).size(), 3);
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve2','toto2', 'Johnson2')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(2,'Scherb','0666666667')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(2,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(2, '17/07/2020','17h30',2,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(3, '18/07/2020','17h30',2,2,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)

	public void findVisitesByIdBienImmobilier_shouldReturnListOfVisite() throws VisitesCanNotBeFoundException {
		
		assertNotNull(repo.findVisiteIdBienImmobilier(1l));
		assertEquals(repo.findVisiteIdBienImmobilier(2l).size(), 2);
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve2','toto2', 'Johnson2')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(2,'Scherb','0666666667')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(2,0.5,'01/01/2001','01/01/2001', '01/01/2001','bad','bad', 0.5,0.5,0.5,0.5,0.5,'indef', 'indef')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	
	@Sql(statements = "INSERT INTO VISITE VALUES(1, '17/07/2020','15h30',1,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(2, '17/07/2020','17h30',2,1,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO VISITE VALUES(3, '18/07/2020','17h30',2,2,1)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)

	@Sql(statements = "DELETE FROM VISITE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void booleansTest_shouldReturnBooleans() {
		assertTrue(repo.existsByDateHeureIdBienImmobilier("17/07/2020", "15h30", 1l));
		assertTrue(repo.existsByDateHeureIdClient("17/07/2020", "15h30", 1l));
		assertTrue(repo.existsByDateHeureIdConseiller("17/07/2020", "15h30", 1l));
		assertFalse(repo.existsByDateHeureIdBienImmobilier("17/08/2020", "15h30", 1l));
		assertFalse(repo.existsByDateHeureIdClient("17/07/2020", "19h30", 1l));
		assertFalse(repo.existsByDateHeureIdConseiller("17/07/2020", "15h30", 2l));
	}

	
}
