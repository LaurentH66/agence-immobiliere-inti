package com.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.Adresse;
import com.exception.CanNotFindAllAdresseException;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class AdresseRepositoryTest {

	@Autowired
	private AdresseRepository repo;

	@Test
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addAdresse_shouldReturnAdresse() {

		Adresse adresse = Adresse.builder().codePostal("54000").ville("Nancy").rue("Rue des Loutres").numero(4)
				.pays("FRANCE").build();

		log.info("Start addAdresse_shouldReturnAdresse");
		adresse = repo.save(adresse);

		assertNotNull(adresse);

		log.info("End addAdresse_shouldReturnAdresse");
	}

	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateAdresse_shouldReturnAdresse() {

		Adresse adresse = Adresse.builder().idAdresse(1l).codePostal("54000").ville("NANCY").rue("Rue des Loutres")
				.numero(4).pays("FRANCE").build();

		log.info("Start updateAdresse_shouldReturnAdresse");
		adresse = repo.save(adresse);

		assertNotNull(adresse);
		assertEquals("NANCY", adresse.getVille());

		log.info("End updateAdresse_shouldReturnAdresse");
	}

	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteAdresseById() {

		log.info("Start deleteAdresseById");
		repo.deleteById(1l);

		assertNull(repo.findById(1l).orElse(null));

		log.info("End deleteAdresseById");
	}

	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAdresseById_shouldReturnAdresse() {

		Optional<Adresse> adresse = repo.findById(1l);
		assertNotNull(adresse);
	}
	
	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(2, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAllAdresses_shouldReturnListOfAdresses() {

		assertNotNull(repo.findAll());
		assertEquals(repo.findAll().size(), 2);
	}

	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllAdresseException
	 */
	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(2, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(3, '54010', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAdressesByCodePostal_shouldReturnListOfAdressesWithSameCodePostal()
			throws CanNotFindAllAdresseException {

		assertFalse(repo.findAdresseByCodePostal("54000").isEmpty());
		assertEquals(2, repo.findAdresseByCodePostal("54000").size());
	}

	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllAdresseException
	 */
	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(2, '66000', 4,'FRANCE', 'RUE DES LOUTRES', 'PERPIGNAN')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(3, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAdressesByVille_shouldReturnListOfAdressesWithSameVille() throws CanNotFindAllAdresseException {
		
		assertFalse(repo.findAdresseByVille("NANCY").isEmpty());
		assertEquals(2, repo.findAdresseByVille("NANCY").size());
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllAdresseException
	 */
	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'MEXIQUE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(2, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(3, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAdressesByPays_shouldReturnListOfAdressesWithSamePays() throws CanNotFindAllAdresseException {
		
		assertFalse(repo.findAdresseByPays("MEXIQUE").isEmpty());
		assertEquals(1, repo.findAdresseByPays("MEXIQUE").size());
		
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllAdresseException
	 */
	@Test
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(2, '54000', 4,'FRANCE', 'RUE DES PISSENLITS', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(3, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAdressesByRue_shouldReturnListOfAdressesWithSameRue()
		throws CanNotFindAllAdresseException {
		
		assertFalse(repo.findAdresseByRue("RUE DES LOUTRES").isEmpty());
		assertEquals(2, repo.findAdresseByRue("RUE DES LOUTRES").size());
		
	}
	
	
}
