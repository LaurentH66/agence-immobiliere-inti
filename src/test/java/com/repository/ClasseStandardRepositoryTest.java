package com.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.ClasseStandard;
import com.exception.ClientCannotBeSavedException;

import lombok.extern.slf4j.Slf4j;
/**
 * 
 * @author Adeline Scherb
 *
 */
@SpringBootTest
@Slf4j
public class ClasseStandardRepositoryTest {

	@Autowired
	ClasseStandardRepository classeStandardRepository;
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "DELETE FROM classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addClasseStandard_shouldReturnClasseStandard(){
		ClasseStandard classeStandard=ClasseStandard.builder().prixMaximum(20000d).superficieMaximale(25).build();
		classeStandard=classeStandardRepository.save(classeStandard);		
		assertNotNull(classeStandard);
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(1,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateClasseStandard_shouldReturnClasseStandardUpdate() {
		ClasseStandard classeStandard=ClasseStandard.builder().idClasseStandard(1l).prixMaximum(20000d).superficieMaximale(25).build();
		classeStandard=classeStandardRepository.save(classeStandard);		
		assertNotNull(classeStandard);
		assertNotEquals(classeStandard.getPrixMaximum(), 30000);
		assertEquals(classeStandard.getPrixMaximum(), 20000);
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(1,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteClasseStandard_shouldReturnDeletedClasseStandard() {
		ClasseStandard classeStandard=ClasseStandard.builder().idClasseStandard(1l).prixMaximum(20000d).superficieMaximale(25).build();
		classeStandardRepository.delete(classeStandard);
		assertNull(classeStandardRepository.findById(1l).orElse(null));
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(1,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getClasseStandardById_ShouldReturnClasseStandard() {
		Optional<ClasseStandard> classeStandard=classeStandardRepository.findById(1l);
		assertNotNull(classeStandard);	
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(1,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getClasseStandards_shouldReturnClasseStandards() {
		assertNotNull(classeStandardRepository.findAll());
	}

}
