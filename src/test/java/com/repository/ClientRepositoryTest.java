package com.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.Client;
import com.exception.ClientCannotBeFoundException;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class ClientRepositoryTest {

	@Autowired
	ClientRepository clientRepository;
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "DELETE FROM client", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addClient_shouldReturnClient(){
		Client client=Client.builder().nom("Scherb").numeroTelephone("0602020104").build();
		client=clientRepository.save(client);		
		assertNotNull(client);
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM client", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateClient_shouldReturnClientUpdate() {
		Client client=Client.builder().idClient(1l).nom("Scherb update").numeroTelephone("0602020104").build();
		client=clientRepository.save(client);		
		assertNotNull(client);
		assertNotEquals(client.getNom(), "Scherb");
		assertEquals(client.getNom(), "Scherb update");
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM client", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteClient_shouldReturnDeletedClient() {
		Client client=Client.builder().idClient(1l).nom("Scherb update").numeroTelephone("0602020104").build();
		clientRepository.delete(client);
		assertNull(clientRepository.findById(1l).orElse(null));
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM client", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getClientById_ShouldReturnClient() {
		Optional<Client> client=clientRepository.findById(1l);
		assertNotNull(client);	
	}
	
	/**
	 * @author Adeline Scherb
	 */
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM client", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getClients_shouldReturnClients() {
		assertNotNull(clientRepository.findAll());
	}
	
	/**
	 * @author LaurentHurtado
	 * @throws ClientCannotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(2,'Hurtado','0666666667')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM client", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getClientsByNom_shouldReturnListOfClientsWithSameNom() throws ClientCannotBeFoundException {
		
		assertFalse(clientRepository.findClientByNom("Sche").isEmpty()); // test pour voir si like % nom % fonctionne en sql
		assertFalse(clientRepository.findClientByNom("Scherb").isEmpty());
		assertEquals(1, clientRepository.findClientByNom("Sche").size());
	}
	
	/**
	 * @author LaurentHurtado
	 * @throws ClientCannotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(2,'Scherb2','0777777777')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM client", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getClientByNumeroTelephone_shouldReturnClientWithNumeroTelephone() throws ClientCannotBeFoundException {
		
		assertTrue(clientRepository.findClientByNumeroTelephone("0666666666").isPresent()); 
		assertEquals("Scherb", clientRepository.findClientByNumeroTelephone("0666666666").get().getNom());
	}
	
	

}
