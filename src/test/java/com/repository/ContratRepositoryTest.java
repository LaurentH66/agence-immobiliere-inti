package com.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.Contrat;
import com.exception.ClientCannotBeFoundException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ContratsCanNotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Laurent Hurtado
 *
 */

@SpringBootTest
@Slf4j
public class ContratRepositoryTest {

	@Autowired
	private ContratRepository contratRepository;
	
	@Test
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addContrat_shouldReturnContrat() {
		
		Contrat contrat = new Contrat("21/12/2021", 100000d, "4uZE");
		contrat = contratRepository.save(contrat);
		
		assertNotNull(contrat);
		
	}
	
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateContrat_shouldBeReturnContratUpdated() {
		
		Contrat contrat = new Contrat(1l, "21/12/2021", 100000d, "4uZE");
		contrat = contratRepository.save(contrat);
		
		assertNotNull(contrat);
		assertEquals(100000d, contrat.getPrixEffectif());
	}
	
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/19999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteContrat_shouldBeReturnContratDeleted() {
		
		Contrat contrat = new Contrat(1l, "21/12/2021", 100000d, "4uZE");
		
		contratRepository.delete(contrat);
		
		assertNull(contratRepository.findById(1l).orElse(null));
		
	}
	
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (2, '21/12/2021', '100000', '4uZE')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getContratById_shouldBeReturnContrat() {
		
		Optional<Contrat> contrat = contratRepository.findById(1l);
		
		assertNotNull(contrat);
		assertEquals("09/05/1999", contrat.get().getDateEffective());

	}

	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (2, '21/12/2021', '100000', '4uZE')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getContrats_shouldReturnContrats() {
		
		assertNotNull(contratRepository.findAll());
		assertEquals(2, contratRepository.findAll().size());

	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ContratsCanNotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (2, '21/12/2021', '100000', '4uZE')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getContratByDate_shouldReturnContrats() throws ContratsCanNotBeFoundException {
		
		assertFalse(contratRepository.findContratByDateEffective("09/05/1999").isEmpty());
		assertEquals(1, contratRepository.findContratByDateEffective("09/05/1999").size());

	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ContratsCanNotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (2, '21/12/2021', '100000', '4uZE')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getContratByPrix_shouldReturnContrats() throws  ContratsCanNotBeFoundException {
		
		assertFalse(contratRepository.findContratByPrixEffectif(25000d).isEmpty());
		assertEquals(1, contratRepository.findContratByPrixEffectif(25000).size());

	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ContratCanNotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (2, '21/12/2021', '100000', '4uZE')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getContratByReference_shouldReturnContrats() throws ContratCanNotBeFoundException {
		
		assertEquals(1, contratRepository.findContratByReferenceContrat("4uZE").size());
		assertEquals(100000d, contratRepository.findContratByReferenceContrat("4uZE").get(0).getPrixEffectif());

	}
	
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (1, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "delete from CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "delete from CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void assignContratToClientTest_shouldReturnContratWithClient() throws ContratCanNotBeFoundException, ClientCannotBeFoundException, ContratsCanNotBeFoundException, ProprietairesCannotBeSavedException {
		
		contratRepository.assignContratToClient(1l, 1l);
		
		assertEquals(1l, contratRepository.findContratByDateEffective("09/05/1999").get(0).getClient().getIdClient());
		
	}
}
