package com.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.Proprietaires;
import com.exception.ClientCannotBeFoundException;
import com.exception.ClientDoesntExistException;
import com.exception.ProprietairesCannotBeFoundException;
import com.exception.ProprietairesCannotBeSavedException;
import com.exception.VisiteIDsAlreadyOccupiedAtThisMomentException;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class ProprietairesRepositoryTest {

	@Autowired
	ProprietairesRepository proprietairesRepository;
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addProprietaires_shouldReturnProprietaires(){
		Proprietaires proprietaires=Proprietaires.builder().nom("Croche").numeroTelephonePrivee("0602020104").numeroTelephoneTravail("0602020105").prenom("Sarah").build();
		proprietaires=proprietairesRepository.save(proprietaires);		
		assertNotNull(proprietaires);
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIREs(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateProprietaires_shouldReturnProprietairesUpdate() {
		Proprietaires proprietaires=Proprietaires.builder().idProprietaire(1l).nom("Croche").numeroTelephonePrivee("0602020104").numeroTelephoneTravail("0602020105").prenom("Sarah").build();
		proprietaires=proprietairesRepository.save(proprietaires);	
		
		assertNotNull(proprietaires);
		assertEquals(proprietaires.getNom(), "Croche");
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteProprietaires_shouldReturnDeletedProprietaires() {
		Proprietaires proprietaires=Proprietaires.builder().idProprietaire(1l).nom("Croche").numeroTelephonePrivee("0602020104").numeroTelephoneTravail("0602020105").prenom("Sarah").build();
		proprietairesRepository.delete(proprietaires);
		assertNull(proprietairesRepository.findById(1l).orElse(null));
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getProprietairesById_ShouldReturnProprietaires() {
		Optional<Proprietaires> proprietaires=proprietairesRepository.findById(1l);
		assertNotNull(proprietaires);	
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getProprietaires_shouldReturnProprietaires() {
		assertNotNull(proprietairesRepository.findAll());
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(2,'Ppelle','0666666667','0602020106','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findProprietairesByNom_shouldReturnProprietaires() throws ProprietairesCannotBeFoundException {
		
		assertFalse(proprietairesRepository.findProprietaireByNom("Ppelle").isEmpty());
		assertEquals(1, proprietairesRepository.findProprietaireByNom("Ppelle").size());
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0611111111','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(2,'Ppelle','0666666666','0602020106','Farah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findProprietaireByNumTelephonePrivee_shouldReturnProprietaire() throws ProprietairesCannotBeFoundException {
		
		assertTrue(proprietairesRepository.findProprietaireByNumeroTelephonePrivee("0611111111").isPresent());
		assertEquals("Farah", proprietairesRepository.findProprietaireByNumeroTelephonePrivee("0666666666").get().getPrenom());
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(2,'Ppelle','0666666667','0699999998','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findProprietaireByNumTelephoneTravail_shouldReturnProprietaire() throws ProprietairesCannotBeFoundException {
		
		assertNotNull(proprietairesRepository.findProprietaireByNumeroTelephoneTravail("0699999998"));
		assertEquals(2, proprietairesRepository.findProprietaireByNumeroTelephoneTravail("0699999998").get().getIdProprietaire());

	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws ProprietairesCannotBeFoundException
	 */
	@Test
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(1,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES(ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE,NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(2,'Ppelle','0666666667','0602020106','Thara')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findProprietairesByPrenom_shouldReturnProprietaires() throws ProprietairesCannotBeFoundException {
		
		assertFalse(proprietairesRepository.findProprietaireByPrenom("Thara").isEmpty());
		assertEquals(1, proprietairesRepository.findProprietaireByPrenom("Thara").size());
	}
	
	
	@Test
	@Sql(statements = "insert into client(ID_CLIENT, NOM, NUMERO_TELEPHONE) values(1,'Scherb','0666666666')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE FROM CLIENT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addProprietaireFromClientTest_shouldReturnProprietaire() throws ClientDoesntExistException, ProprietairesCannotBeFoundException, ProprietairesCannotBeSavedException, ClientCannotBeFoundException {

		
		proprietairesRepository.addProprietaireFromClient(1l);
		
		assertEquals("Scherb", proprietairesRepository.findProprietaireByNumeroTelephonePrivee("0666666666").get().getNom());

	}
}
