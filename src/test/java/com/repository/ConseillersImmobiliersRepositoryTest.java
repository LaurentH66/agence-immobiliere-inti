package com.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.ConseillersImmobiliers;
import com.exception.CanNotFindAllConseillersImmobiliersException;
import com.exception.CanNotFindConseillersImmobiliersException;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class ConseillersImmobiliersRepositoryTest {

	@Autowired
	private ConseillersImmobiliersRepository repo; 
	
	@Test
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addConseillersImmobiliers_shouldReturnConseillersImmobiliers() {
		
		ConseillersImmobiliers conseillersImmobiliers = ConseillersImmobiliers.builder().nom("Steeve").prenom("Johnson").login("sjohn").password("toto").build();
		
		log.info("Start addConseillersImmobiliers_shouldReturnConseillersImmobiliers");
		conseillersImmobiliers = repo.save(conseillersImmobiliers); 
		
		assertNotNull(conseillersImmobiliers);
		
		log.info("End addConseillersImmobiliers_shouldReturnConseillersImmobiliers");
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateConseillersImmobiliers_shouldReturnConseillersImmobiliers() {
		
		ConseillersImmobiliers conseillersImmobiliers = ConseillersImmobiliers.builder().idConseillersImmobiliers(1l).nom("Steevo").prenom("Johnson").login("sjohn").password("toto").build();
		
		log.info("Start updateConseillersImmobiliers_shouldReturnConseillersImmobiliers");
		conseillersImmobiliers = repo.save(conseillersImmobiliers); 
		
		assertNotNull(conseillersImmobiliers);
		assertEquals("Steevo", conseillersImmobiliers.getNom());
		
		log.info("End updateConseillersImmobiliers_shouldReturnConseillersImmobiliers");
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteConseillersImmobiliersById() {
		
		log.info("Start deleteConseillersImmobiliersById");
		repo.deleteById(1l);
		
		assertNull(repo.findById(1l).orElse(null));
		
		log.info("End deleteConseillersImmobiliersById");
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findConseillersImmobiliersById_shouldReturnConseillersImmobiliers() {
		
		Optional<ConseillersImmobiliers> conseillersImmobiliers = repo.findById(1l); 
		assertNotNull(conseillersImmobiliers);
	}
	
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAllConseillersImmobilierss_shouldReturnListOfConseillersImmobilierss() {
		
		assertNotNull(repo.findAll());
		assertEquals(repo.findAll().size(), 2);
	}
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindConseillersImmobiliersException
	 */
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findConseillerImmobilierByLogin_shouldReturnConseillerImmobilier() throws CanNotFindConseillersImmobiliersException {
	
		assertTrue(repo.findConseillersImmobiliersByLogin("sjohn").isPresent());
		assertEquals("Steeve", repo.findConseillersImmobiliersByLogin("sjohn").get().getNom());
		
	}	
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllConseillersImmobiliersException
	 */
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(3, 'sjohn3', 'Pablo','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findConseillersImmobiliersWithSameNom_shouldReturnListOfConseillerImmobilierWithSameNom() throws CanNotFindAllConseillersImmobiliersException {
	
		assertFalse(repo.findConseillersImmobiliersByNom("Steeve").isEmpty());
		assertEquals(2, repo.findConseillersImmobiliersByNom("Steeve").size());
		
	}	
	
	/**
	 * @author Laurent Hurtado
	 * @throws CanNotFindAllConseillersImmobiliersException
	 */
	@Test
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(1, 'sjohn', 'Steeve','toto', 'Johnson')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO CONSEILLERS_IMMOBILIERS VALUES(2, 'sjohn2', 'Steeve','toto', 'Pablo')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM CONSEILLERS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findConseillerImmobilierByPrenom_shouldReturnConseillerImmobilier() throws CanNotFindAllConseillersImmobiliersException {
	
		assertFalse(repo.findConseillersImmobiliersByPrenom("Pablo").isEmpty());
		assertEquals(1, repo.findConseillersImmobiliersByPrenom("Pablo").size());
		
	}	
}
