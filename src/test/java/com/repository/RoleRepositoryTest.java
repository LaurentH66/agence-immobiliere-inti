package com.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.Role;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class RoleRepositoryTest {

	
	@Autowired
	private RoleRepository rrepo;

	@Test
	@Sql(statements = "DELETE FROM ROLE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addRoleWithoutUser_shouldReturnRole() { 
		
		Role role = Role.builder().roleName("Supeer").description("bla").build();
		
		log.info("test addRoleWithoutUser_shouldReturnRole begin");
		role = rrepo.save(role); 
		
		assertNotNull(role);
		log.info("test addRoleWithoutUser_shouldReturnRole end");
		
	}
	
	
	
	@Test
	@Sql(statements = "INSERT INTO ROLE VALUES (1,'role','read only')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ROLE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateRole_shouldReturnRoleUpdated() {
		
		
		Role role = Role.builder().idRole(1l).roleName("roleeee").description("read only").build();

		
		log.info("test updateRole_shouldReturnRoleUpdated begin");
		role = rrepo.save(role);
		
		assertNotNull(role);
		assertEquals("roleeee", role.getRoleName());
		
		log.info("test updateRole_shouldReturnRoleUpdated end");
	}
	
	
	
	@Test
	@Sql(statements = "INSERT INTO ROLE VALUES (1,'role','read only')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ROLE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteRole_shouldReturnRoleDeleted() { 
		
		Role role = Role.builder().idRole(1l).roleName("role").description("read only").build();
		
		log.info("test deleteRole_shouldReturnRoleDeleted begin");
		rrepo.delete(role);
		
		assertNull(rrepo.findById(1l).orElse(null));
		log.info("test deleteRole_shouldReturnRoleDeleted end");
	}
	
	
	@Test
	@Sql(statements = "INSERT INTO ROLE VALUES (1,'role','read only')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ROLE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getRoleById_shouldReturnRole() { 
		
		
		
		log.info("test getRoleById_shouldReturnRole begin");
		Optional<Role> role = rrepo.findById(1l); 
		assertNotNull(role);

		log.info("test getRoleById_shouldReturnRole end");
	}
	
	
	
	@Test
	@Sql(statements = "INSERT INTO ROLE VALUES (1,'role','read only')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ROLE VALUES (2,'role2','read only')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM ROLE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getRoles_shouldReturnRoles() { 
		log.info("test getRoles_shouldReturnRoles begin");

		assertNotNull(rrepo.findAll());
		assertEquals(rrepo.findAll().size(), 2);
		
		log.info("test getRoles_shouldReturnRoles end");

		
	}
}
