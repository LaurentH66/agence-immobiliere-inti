package com.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.Adresse;
import com.entity.Contrat;
import com.entity.TypeBienImmobilier;

import lombok.extern.slf4j.Slf4j;
/** 
 * @author Charles
 */
@SpringBootTest
@Slf4j
public class TypeBienImmobilierRepositoryTest {
	
	@Autowired
	private TypeBienImmobilierRepository repo; 
	
	@Test
	@Sql(statements = "delete from TYPE_BIEN_IMMOBILIER", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addTypeBienImmobilier_shouldReturnTypeBienImmobilier() {
		
		TypeBienImmobilier typeBien = new TypeBienImmobilier("Maison");
		
		log.info("Start addTypeBienImmobilier_shouldReturnTypeBienImmobilier");

		typeBien = repo.save(typeBien);
		
		assertNotNull(typeBien);
		
		log.info("End addTypeBienImmobilier_shouldReturnTypeBienImmobilier");
		
	}
	
	@Test
	@Sql(statements = "INSERT INTO TYPE_BIEN_IMMOBILIER VALUES(1, 'MAISON')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM TYPE_BIEN_IMMOBILIER", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateTypeBienImmobilier_shouldReturnTypeBienImmobilier() {
		
		TypeBienImmobilier typeBien = new TypeBienImmobilier(1l,"MAISON");
		
		log.info("Start updateTypeBienImmobilier_shouldReturnTypeBienImmobilier");
		typeBien = repo.save(typeBien); 
		
		assertNotNull(typeBien);
		assertEquals("MAISON", typeBien.getTypeBien());
		
		log.info("End updateTypeBienImmobilier_shouldReturnTypeBienImmobilier");
	}
	
	@Test
	@Sql(statements = "INSERT INTO TYPE_BIEN_IMMOBILIER VALUES(1, 'MAISON')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM TYPE_BIEN_IMMOBILIER", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteTypeBienImmobilierById() {
		
		log.info("Start deleteTypeBienImmobilierById");
		repo.deleteById(1l);
		
		assertNull(repo.findById(1l).orElse(null));
		
		log.info("End deleteTypeBienImmobilierById");
	}
	
	@Test
	@Sql(statements = "INSERT INTO TYPE_BIEN_IMMOBILIER VALUES(1, 'MAISON')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM TYPE_BIEN_IMMOBILIER", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findTypeBienImmobilierById_shouldReturnTypeBienImmobilier() {
		
		Optional<TypeBienImmobilier> typeBien = repo.findById(1l); 
		assertNotNull(typeBien);
	}
	
	@Test
	@Sql(statements = "INSERT INTO TYPE_BIEN_IMMOBILIER VALUES(1, 'MAISON')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO TYPE_BIEN_IMMOBILIER VALUES(2, 'MAISON')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM TYPE_BIEN_IMMOBILIER", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findAllTypeBienImmobilier_shouldReturnListOfTypeBienImmobilier() {
		
		assertNotNull(repo.findAll());
		assertEquals(repo.findAll().size(), 2);
	}

}
