package com.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import com.entity.BiensImmobiliers;
import com.exception.BiensImmobiliersCannotBeFoundException;
import com.exception.BiensImmobiliersCannotBeUpdatedException;
import com.exception.ContratCanNotBeFoundException;
import com.exception.ProprietairesCannotBeFoundException;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
public class BiensImmobiliersRepositoryTest {

	@Autowired
	BiensImmobiliersRepository BiensImmobiliersRepository;
	
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test 
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void addBiensImmobiliers_shouldReturnBiensImmobiliers(){
		BiensImmobiliers biensImmobiliers=BiensImmobiliers.builder().cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build();
		biensImmobiliers=BiensImmobiliersRepository.save(biensImmobiliers);		
		assertNotNull(biensImmobiliers);
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void updateBiensImmobiliers_shouldReturnBiensImmobiliersUpdate() {
		BiensImmobiliers biensImmobiliers=BiensImmobiliers.builder().idBienImmobilier(1l).cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build();
		biensImmobiliers=BiensImmobiliersRepository.save(biensImmobiliers);	
		
		assertNotNull(biensImmobiliers);
		assertEquals(biensImmobiliers.getCautionLocative(), 12.5);
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteBiensImmobiliers_shouldReturnDeletedBiensImmobiliers() {
		BiensImmobiliers biensImmobiliers=BiensImmobiliers.builder().idBienImmobilier(1l).cautionLocative(12.5).dateAchat("01/02/2022").dateDisposition("02/02/2022").dateSoumission("03/02/2022").etat("good").garniture("good").loyerMensuel(600.5).prixAchat(1200.5).prixAchatDemande(1205.5).prixLocation(602.5).revenuCadastral(200.5).status("vendu").typeBail("apart").build();
		BiensImmobiliersRepository.delete(biensImmobiliers);
		assertNull(BiensImmobiliersRepository.findById(1l).orElse(null));
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getBiensImmobiliersById_ShouldReturnBiensImmobiliers() {
		Optional<BiensImmobiliers> biensImmobiliers=BiensImmobiliersRepository.findById(1l);
		assertNotNull(biensImmobiliers);	
	}
	
	/**
	 * 
	 * @author Damien
	 *
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void getBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findAll().isEmpty());
	}
	
	
	
	
	
	
	
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByCautionMaxBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMax(0.6).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMax(0.4).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByCautionMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMin(0.4).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMin(0.6).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByCautionMaxMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMaxMin(0.4, 0.6).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByCautionlocativeMaxMin(0.2,0.4).isEmpty());
	}
	
	
	
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByLoyerMensMaxBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMax(0.8).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMax(0.6).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByLoyerMensMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMin(0.6).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMin(0.8).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByLoyerMensMaxMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMaxMin(0.6, 0.8).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByLoyermensuelMaxMin(0.8,1.0).isEmpty());
	}
	
	
	
	
	
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixlocationMaxBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMax(1.4).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMax(1.2).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixlocationMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMin(1.2).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMin(1.4).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixlocationMaxMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMaxMin(1.2, 1.4).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixlocationMaxMin(1.4 , 1.6).isEmpty());
	}
	
	
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixachatMaxBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMax(1.0).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMax(0.8).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixachatMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMin(0.8).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMin(1.0).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixachatMaxMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMaxMin(0.8, 1.0).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatMaxMin(1.0 , 1.2).isEmpty());
	}
	
	
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixachatdemandeMaxBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMax(1.2).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMax(1.0).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixachatdemandeMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMin(1.0).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMin(1.2).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByPrixachatdemandeMaxMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMaxMin(1.0, 1.2).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByPrixachatdemandeMaxMin(1.2 , 1.4).isEmpty());
	}
	
	
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByRevenucadastralMaxBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMax(1.6).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMax(1.4).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByRevenucadastralMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMin(1.4).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMin(1.6).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByRevenucadastralMaxMinBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMaxMin(1.4, 1.6).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByRevenucadastralMaxMin(1.6 , 1.8).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByTypeBailBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByTypeBail("def").isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByTypeBail("in").isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByGarnitureBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByGarniture("good").isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByGarniture("bad").isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByEtatBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByEtat("bad").isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByEtat("good").isEmpty());
	}
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByStatusBiensImmobiliers_shouldReturnBiensImmobiliers() {
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByStatus("ind").isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByStatus("bad").isEmpty());
	}
	
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (3, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES( ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE, NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(4,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(2,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL, FK_ADRESSE, FK_CLASSE_STANDARD, FK_CONTRAT, FK_PROPRIETAIRE) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def', 1,2,3,4)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByProprietaireIdBiensImmobiliers_shouldReturnBiensImmobiliers() {
		
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByProprietaireId(4l).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByProprietaireId(1l).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (3, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES( ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE, NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(4,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(2,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL, FK_ADRESSE, FK_CLASSE_STANDARD, FK_CONTRAT, FK_PROPRIETAIRE) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def', 1,2,3,4)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByAdresseIdBiensImmobiliers_shouldReturnBiensImmobiliers() {
		
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByAdresseId(1l).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByAdresseId(2l).isEmpty());
	}
	/**
	 * @author Damien
	 */
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (3, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES( ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE, NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(4,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(2,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL, FK_ADRESSE, FK_CLASSE_STANDARD, FK_CONTRAT, FK_PROPRIETAIRE) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def', 1,2,3,4)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void findByContratIdBiensImmobiliers_shouldReturnBiensImmobiliers() {
		
		assertFalse(BiensImmobiliersRepository.findBiensImmobiliersByContratId(3l).isEmpty());
		assertTrue(BiensImmobiliersRepository.findBiensImmobiliersByContratId(2l).isEmpty());
	}
	
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (3, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES( ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE, NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(4,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(2,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL, FK_ADRESSE, FK_CLASSE_STANDARD) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def', 1,2)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void assignContratToBienImmobilierTest() throws ContratCanNotBeFoundException, BiensImmobiliersCannotBeFoundException {
		
		BiensImmobiliersRepository.assignContratToBienImmobilier(3l, 1l);
		
		assertEquals(3l, BiensImmobiliersRepository.findAll().get(0).getContrat().getIdContrat());
		
	}
	
	@Test
	@Sql(statements = "insert into CONTRAT(ID_CONTRAT, DATE_EFFECTIVE, PRIX_EFFECTIF, REFERENCE_CONTRAT) values (3, '09/05/1999', '25000', 'aaaa')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "INSERT INTO ADRESSE VALUES(1, '54000', 4,'FRANCE', 'RUE DES LOUTRES', 'NANCY')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into PROPRIETAIRES( ID_PROPRIETAIRE, NOM, NUMERO_TELEPHONE_PRIVEE, NUMERO_TELEPHONE_TRAVAIL, PRENOM) values(4,'Porte','0666666666','0602020105','Sarah')", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into classe_standard(ID_CLASSE_STANDARD, PRIX_MAXIMUM, SUPERFICIE_MAXIMALE) values(2,30000,25)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "insert into BIENS_IMMOBILIERS(ID_BIEN_IMMOBILIER, CAUTION_LOCATIVE, DATE_ACHAT, DATE_DISPOSITION,DATE_SOUMISSION, ETAT,GARNITURE,LOYER_MENSUEL,PRIX_ACHAT, PRIX_ACHAT_DEMANDE, PRIX_LOCATION, REVENU_CADASTRAL, STATUS, TYPE_BAIL, FK_ADRESSE, FK_CLASSE_STANDARD, FK_CONTRAT) values(1,0.5,'01/02/2001','01/01/2001', '01/03/2001','bad','good', 0.7,0.9,1.1,1.3,1.5,'indef', 'def', 1,2,3)", executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(statements = "DELETE FROM BIENS_IMMOBILIERS", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE CONTRAT", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE ADRESSE", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE PROPRIETAIRES", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	@Sql(statements = "DELETE classe_standard", executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	public void changementStatusBienImmobilierWhenSoldTest() throws BiensImmobiliersCannotBeUpdatedException, ProprietairesCannotBeFoundException {
		
		BiensImmobiliersRepository.changementStatusBienImmobilier(3l, "0666666666");
		
		assertEquals("Vendu", BiensImmobiliersRepository.findAll().get(0).getStatus());
		assertEquals(4l, BiensImmobiliersRepository.findAll().get(0).getProprietaire().getIdProprietaire());
		
	}
	
}
